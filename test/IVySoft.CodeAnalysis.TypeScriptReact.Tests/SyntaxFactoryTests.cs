// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Text;
using static IVySoft.CodeAnalysis.TypeScriptReact.SyntaxFactory;

namespace IVySoft.CodeAnalysis.TypeScriptReact.Tests;

public class ImportTests
{
    [Fact]
    public async Task CommentTest()
    {
        using var timeout = new CancellationTokenSource(TimeSpan.FromSeconds(300));
        var cancellationToken = timeout.Token;

        var sb = new StringBuilder();
        using (var writer = new StringWriter(sb))
        {
            var unit =
                CompilationUnit()
                .WithImpors(
                    SingletonList<ImportDirectiveSyntax>(
                        ImportDirective(
                            Literal("'react'"),
                            Identifier("React"),
                            true)
                        .WithImportKeyword(
                            Token(
                                TriviaList(
                                    SingleLineComment("// Test")),
                                SyntaxKind.ImportKeyword,
                                TriviaList()))
                        .WithSemicolonToken(
                            Token(
                                TriviaList(),
                                SyntaxKind.SemicolonToken,
                                TriviaList(
                                    SingleLineComment("//test 1"))))))
                .NormalizeWhitespace();

            await unit.WriteTo(writer, cancellationToken);
        }
        var result = @"// Test
import React from 'react';//test 1";
        Assert.Equal(result, sb.ToString());
    }
    [Fact]
    public async Task AliasTest()
    {
        using var timeout = new CancellationTokenSource(TimeSpan.FromSeconds(300));
        var cancellationToken = timeout.Token;

        var sb = new StringBuilder();
        using (var writer = new StringWriter(sb))
        {
            var unit =
                CompilationUnit()
                .WithImpors(
                    SingletonList<ImportDirectiveSyntax>(
                        ImportDirective(Literal("'react'"))
                        .WithReferences(
                            ImportReference(Identifier("React"))
                            )
                        .WithNamedReferences(
                            ImportReference(Identifier("test"), Identifier("Test"))
                            )
                        ))
                .NormalizeWhitespace();

            await unit.WriteTo(writer, cancellationToken);
        }
        var result = "import React, { test as Test } from 'react';";
        Assert.Equal(result, sb.ToString());
    }
    [Fact]
    public void ChangeImportTest()
    {
        var syntax =
            CompilationUnit()
                .WithImpors(
                    SingletonList<ImportDirectiveSyntax>(
                        ImportDirective(
                            Literal("'react'"),
                            Identifier("React"),
                            false)));

        Assert.Equal("import React from 'react';", syntax.NormalizeWhitespace().ToString());

        var createRoot = Import(ref syntax, "react-dom/client", "createRoot", true);
        Assert.Equal("createRoot", createRoot);
        Assert.Equal(@"import React from 'react';
import { createRoot } from 'react-dom/client';", syntax.NormalizeWhitespace().ToString());


        var app = Import(ref syntax, "./App", "App", false);
        Assert.Equal("App", app);
        Assert.Equal(@"import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App';", syntax.NormalizeWhitespace().ToString());

        Import(ref syntax, "./index.css");
        Assert.Equal(@"import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App';
import './index.css';", syntax.NormalizeWhitespace().ToString());


        var createRoot1 = Import(ref syntax, "otherSource", "createRoot", true);
        Assert.Equal("createRoot1", createRoot1);
        Assert.Equal(@"import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App';
import './index.css';
import { createRoot as createRoot1 } from 'otherSource';", syntax.NormalizeWhitespace().ToString());

        app = Import(ref syntax, "./App", "App", false);
        Assert.Equal("App", app);
        Assert.Equal(@"import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App';
import './index.css';
import { createRoot as createRoot1 } from 'otherSource';", syntax.NormalizeWhitespace().ToString());

        var dom = Import(ref syntax, "react", "dom", true);
        Assert.Equal("dom", dom);
        Assert.Equal(@"import React, { dom } from 'react';
import { createRoot } from 'react-dom/client';
import App from './App';
import './index.css';
import { createRoot as createRoot1 } from 'otherSource';", syntax.NormalizeWhitespace().ToString());
    }
    private static void Import(ref CompilationUnitSyntax syntax, string path)
    {
        if (!syntax.DescendantsOrSelf().OfType<ImportDirectiveSyntax>().Any(x => x.Source.Token.Text == $"'{path}'"))
        {
            syntax = syntax
                .AddImpors(
                    ImportDirective(
                        Literal($"'{path}'")));
        }
    }
    private static string Import(ref CompilationUnitSyntax syntax, string path, string name, bool isNamed)
    {
        return Import(ref syntax, path, name, new HashSet<string>(), isNamed);
    }
    private static string Import(ref CompilationUnitSyntax syntax, string path, string name, HashSet<string> reservedNames, bool isNamed)
    {
        foreach (var import in syntax.DescendantsOrSelf().OfType<ImportDirectiveSyntax>().Where(x => x.Source.Token.Text == $"'{path}'"))
        {
            foreach (var reference in isNamed ? import.NamedReferences : import.References)
            {
                if (reference.Name.Identifier.Token.Text == name)
                {
                    if (null != reference.Alias)
                    {
                        return reference.Alias.Identifier.Token.Text;
                    }
                    return name;
                }
            }
            var alias = name;
            if (
                reservedNames.Contains(name)
                || syntax.DescendantsOrSelf().OfType<IdentifierSyntax>().Any(x => x.Identifier.Token.Text == name)
                )
            {
                for (int i = 1; ; ++i)
                {
                    alias = name + i.ToString();
                    if (!reservedNames.Contains(alias) && !syntax.DescendantsOrSelf().OfType<IdentifierSyntax>().Any(x => x.Identifier.Token.Text == alias))
                    {
                        break;
                    }
                }
            }
            syntax = (CompilationUnitSyntax)(syntax.Accept(
                new SimpleRewriter(import,
                    isNamed
                    ? import.AddNamedReferences(
                        (name == alias)
                            ? ImportReference(Identifier(name))
                            : ImportReference(Identifier(name), Identifier(alias)))
                    : import.AddReferences(
                        (name == alias)
                            ? ImportReference(Identifier(name))
                            : ImportReference(Identifier(name), Identifier(alias)))
                )) ?? throw new NullReferenceException());
            return alias;
        }
        var alias1 = name;
        if (reservedNames.Contains(alias1)
            || syntax.DescendantsOrSelf().OfType<IdentifierSyntax>().Any(x => x.Identifier.Token.Text == name)
            )
        {
            for (int i = 1; ; ++i)
            {
                alias1 = name + i.ToString();
                if (!reservedNames.Contains(alias1)
                    && !syntax.DescendantsOrSelf().OfType<IdentifierSyntax>().Any(x => x.Identifier.Token.Text == alias1)
                    )
                {
                    break;
                }
            }
        }
        syntax = syntax
            .AddImpors(
                isNamed
                ? ImportDirective(
                    Literal($"'{path}'"))
                  .WithNamedReferences(
                    (name == alias1)
                        ? ImportReference(Identifier(name))
                        : ImportReference(Identifier(name), Identifier(alias1)))
                : ImportDirective(
                    Literal($"'{path}'"))
                  .WithReferences(
                    (name == alias1)
                        ? ImportReference(Identifier(name))
                        : ImportReference(Identifier(name), Identifier(alias1))));
        return alias1;
    }
}
