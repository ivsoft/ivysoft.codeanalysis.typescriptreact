// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using static IVySoft.CodeAnalysis.TypeScriptReact.SyntaxFactory;

namespace IVySoft.CodeAnalysis.TypeScriptReact.Tests;

public class GenerateTests
{
    [Fact]
    public async Task ExportInterface()
    {
        using var timeout = new CancellationTokenSource(TimeSpan.FromSeconds(300));
        var cancellationToken = timeout.Token;

        var sb = new System.Text.StringBuilder();
        using (var writer = new StringWriter(sb))
        {
            var unit =
                CompilationUnit()
                .WithMembers(
                    InterfaceDefinition(Identifier("Parameters"))
                    .WithModifiers(new SyntaxNode(new SyntaxToken(SyntaxKind.ExportKeyword)))
                    .WithMembers(InterfaceProperty(Identifier("data"), TypeName("IVySoft.SiteBuilder.CodeModel.Project")))
            )
            .NormalizeWhitespace();

            await unit.WriteTo(writer, cancellationToken);
        }
        var result = @"export interface Parameters {
  data: IVySoft.SiteBuilder.CodeModel.Project;
}";
        Assert.Equal(result, sb.ToString());
    }
    [Fact]
    public async Task ExportConst()
    {
        using var timeout = new CancellationTokenSource(TimeSpan.FromSeconds(300));
        var cancellationToken = timeout.Token;

        var sb = new System.Text.StringBuilder();
        using (var writer = new StringWriter(sb))
        {
            var unit =
                CompilationUnit()
                .WithMembers(
                    ConstDefinition(Identifier("ProjectEdit"))
                    .WithModifiers(new SyntaxNode(new SyntaxToken(SyntaxKind.ExportKeyword)))
                    .WithInitializer(
                        EqualsValueClause(
                        LambdaExpression()
                        .WithBody(
                            ExpressionBlock(
                                ReturnStatement(
                                    ParenthesizedExpression(
                                        JsxElement()
                                        .WithBody(
                                            JsxElement(Identifier("IntEdit"))
                                                .WithAttributes(
                                                    JsxAttribute(
                                                        Identifier("data"),
                                                        MemberAccessExpression(
                                                            SyntaxKind.SimpleMemberAccessExpression,
                                                            IdentifierName("value"),
                                                            Identifier("Id"))))
            ))))))))
            .NormalizeWhitespace();

            await unit.WriteTo(writer, cancellationToken);
        }
        var result = @"export const ProjectEdit = () => {
  return (
    <>
      <IntEdit data={ value.Id }/>
    </>
  );
}";
        Assert.Equal(result, sb.ToString());
    }
    [Fact]
    public async Task GenerateJsonObjectExpression()
    {
        using var timeout = new CancellationTokenSource(TimeSpan.FromSeconds(300));
        var cancellationToken = timeout.Token;

        var sb = new System.Text.StringBuilder();
        using (var writer = new StringWriter(sb))
        {
            var unit =
                CompilationUnit()
                .WithMembers(
                    GlobalStatement(
                        ExpressionStatement(
                            InvocationExpression(
                                MemberAccessExpression(
                                    SyntaxKind.SimpleMemberAccessExpression,
                                    IdentifierName("tinymce"),
                                    Identifier("init")))
                            .WithArgumentList(
                                Argument(
                                    JsonObjectExpression(
                                        JsonObjectMemberDeclarator(
                                            Identifier("selector"),
                                            LiteralExpression(
                                                Literal("'textarea'"))),
                                        JsonObjectMemberDeclarator(
                                            Identifier("height"),
                                            LiteralExpression(
                                                Literal(500))),
                                        JsonObjectMemberDeclarator(
                                            Identifier("theme"),
                                            LiteralExpression(
                                                Literal("'silver'"))),
                                        JsonObjectMemberDeclarator(
                                            Identifier("plugins"),
                                            LiteralExpression(
                                                Literal(
                                                    "'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help'"
                                                    ))),
                                        JsonObjectMemberDeclarator(
                                            Identifier("toolbar1"),
                                            LiteralExpression(
                                                Literal(
                                                    "'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat'"
                                                    ))),
                                        JsonObjectMemberDeclarator(
                                            Identifier("image_advtab"),
                                            LiteralExpression(
                                                SyntaxKind.TrueKeyword))))))))
            .NormalizeWhitespace();

            await unit.WriteTo(writer, cancellationToken);
        }
        var result = @"tinymce.init({
  selector: 'textarea'
  , height: 500
  , theme: 'silver'
  , plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help'
  , toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat'
  , image_advtab: true
  }
);";
        Assert.Equal(result, sb.ToString());
    }
    [Fact]
    public async Task EmptyJsxElement()
    {
        using var timeout = new CancellationTokenSource(TimeSpan.FromSeconds(300));
        var cancellationToken = timeout.Token;

        var sb = new System.Text.StringBuilder();
        using (var writer = new StringWriter(sb))
        {
            var element = JsxElement();
            var context = new NormalizeWhitespaceContext(null);
            context.Visit(element, element);
            await element.WriteTo(writer, cancellationToken);
        }
        var result = @"
  </>";
        Assert.Equal(result, sb.ToString());
    }
    [Fact]
    public async Task EmptyJsxElementBody()
    {
        using var timeout = new CancellationTokenSource(TimeSpan.FromSeconds(300));
        var cancellationToken = timeout.Token;

        var sb = new System.Text.StringBuilder();
        using (var writer = new StringWriter(sb))
        {
            var element = JsxElement().WithBody(JsxElement());
            var context = new NormalizeWhitespaceContext(null);
            context.Visit(element, element);
            await element.WriteTo(writer, cancellationToken);
        }
        var result = @"
  <>
    </>
  </>";
        Assert.Equal(result, sb.ToString());
    }
    [Fact]
    public async Task GenerateNamespace()
    {
        using var timeout = new CancellationTokenSource(TimeSpan.FromSeconds(300));
        var cancellationToken = timeout.Token;

        var sb = new System.Text.StringBuilder();
        using (var writer = new StringWriter(sb))
        {
            var element = NamespaceDeclaration(
                Identifier("System"))
                .WithModifiers(
                    new SyntaxNode(
                        new SyntaxToken(
                            SyntaxKind.ExportKeyword)))
                .WithMembers(
                    TypeAliasDefinition(Identifier("Int32"), Identifier("number")).WithModifiers(new SyntaxNode(new SyntaxToken(SyntaxKind.ExportKeyword))),
                    TypeAliasDefinition(Identifier("String"), Identifier("string")).WithModifiers(new SyntaxNode(new SyntaxToken(SyntaxKind.ExportKeyword)))
                    );
            var context = new NormalizeWhitespaceContext(null);
            context.Visit(element, element);
            await element.WriteTo(writer, cancellationToken);
        }
        var result = @"export namespace System {
  export type Int32 = number;
  export type String = string;
}";
        Assert.Equal(result, sb.ToString());
    }

    [Fact]
    public async Task SimpleTest()
    {
        using var timeout = new CancellationTokenSource(TimeSpan.FromSeconds(300));
        var cancellationToken = timeout.Token;

        var sb = new System.Text.StringBuilder();
        using (var writer = new StringWriter(sb))
        {
            var unit =
                CompilationUnit()
                .WithMembers(
                    GlobalStatement(
                        ExpressionStatement(
                            AssignmentExpression(
                                SyntaxKind.SimpleAssignmentExpression,
                                MemberAccessExpression(
                                    SyntaxKind.SimpleMemberAccessExpression,
                                    IdentifierName(
                                        Identifier(
                                            TriviaList(
                                                SingleLineComment("// Initialize the app")),
                                            "tinymce",
                                            TriviaList())),
                                    Identifier("baseURL")),
                                LiteralExpression(
                                    Literal("'/tinymce'"))))),
                    GlobalStatement(
                        ExpressionStatement(
                            InvocationExpression(
                                MemberAccessExpression(
                                    SyntaxKind.SimpleMemberAccessExpression,
                                    IdentifierName("tinymce"),
                                    Identifier("init")))
                            .WithArgumentList(
                                Argument(
                                    JsonObjectExpression(
                                        JsonObjectMemberDeclarator(
                                            Identifier("selector"),
                                            LiteralExpression(
                                                Literal("'textarea'"))),
                                        JsonObjectMemberDeclarator(
                                            Identifier("height"),
                                            LiteralExpression(
                                                Literal(500))),
                                        JsonObjectMemberDeclarator(
                                            Identifier("theme"),
                                            LiteralExpression(
                                                Literal("'silver'"))),
                                        JsonObjectMemberDeclarator(
                                            Identifier("plugins"),
                                            LiteralExpression(
                                                Literal(
                                                    "'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help'"
                                                    ))),
                                        JsonObjectMemberDeclarator(
                                            Identifier("toolbar1"),
                                            LiteralExpression(
                                                Literal(
                                                    "'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat'"
                                                    ))),
                                        JsonObjectMemberDeclarator(
                                            Identifier("image_advtab"),
                                            LiteralExpression(
                                                SyntaxKind.TrueKeyword))))))),
                    GlobalStatement(
                        ConstDefinition(
                            Identifier("container"))
                        .WithInitializer(
                            EqualsValueClause(
                                PostfixUnaryExpression(
                                    SyntaxKind.SuppressNullableWarningExpression,
                                    InvocationExpression(
                                        MemberAccessExpression(
                                            SyntaxKind.SimpleMemberAccessExpression,
                                            IdentifierName("document"),
                                            Identifier("getElementById")))
                                    .WithArgumentList(
                                        Argument(
                                            LiteralExpression(
                                                Literal("'root'")))))))),
                    GlobalStatement(
                        ConstDefinition(
                            Identifier("root"))
                        .WithInitializer(
                            EqualsValueClause(
                                InvocationExpression(
                                    IdentifierName("createRoot"))
                                .WithArgumentList(
                                    Argument(
                                        IdentifierName("container")))))),
                    GlobalStatement(
                        ExpressionStatement(
                            InvocationExpression(
                                MemberAccessExpression(
                                    SyntaxKind.SimpleMemberAccessExpression,
                                    IdentifierName(
                                        Identifier(
                                            TriviaList(new SyntaxToken(SyntaxKind.NewLineTrivia)),
                                            "root",
                                            TriviaList())),
                                    Identifier("render")))
                            .WithArgumentList(
                                Argument(
                                    JsxElement(Identifier("React.StrictMode"))
                                    .WithBody(
                                        JsxElement(Identifier("Provider"))
                                        .WithAttributes(
                                            JsxAttribute(Identifier("store"), IdentifierName("store")))
                                        .WithBody(
                                            JsxElement(Identifier("BrowserRouter"))
                                            .WithBody(
                                                JsxElement(Identifier("App"))
            ))))))))
            .NormalizeWhitespace();

            await unit.WriteTo(writer, cancellationToken);
        }
        var result = @"// Initialize the app
tinymce.baseURL = '/tinymce';
tinymce.init({
  selector: 'textarea'
  , height: 500
  , theme: 'silver'
  , plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help'
  , toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat'
  , image_advtab: true
  }
);
const container = document.getElementById('root')!
const root = createRoot(container)
root.render(
  <React.StrictMode>
    <Provider store={ store }>
      <BrowserRouter>
        <App/>
      </BrowserRouter>
    </Provider>
  </React.StrictMode>
);";
        Assert.Equal(result, sb.ToString());
    }
}
