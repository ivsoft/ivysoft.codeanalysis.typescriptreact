﻿// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct JsxElementTextSyntax : JsxElementBody
{
    public SyntaxNode Body { get; }

    public JsxElementTextSyntax(SyntaxNode body)
    {
        Body = body;
    }
    public JsxElementTextSyntax(string text)
        : this(new SyntaxNode(new SyntaxToken(SyntaxKind.LiteralType, text)))
    {
    }

    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { Body };
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitJsxElementText(this);
    }
}
