// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct InstanceOfExpressionSyntax : ExpressionSyntax
{
    public ExpressionSyntax Expression { get; }
    public SyntaxNode InstanceOfKeyword { get; }
    public ITypeSyntax Type { get; }
    public InstanceOfExpressionSyntax(
        ExpressionSyntax expression,
        SyntaxNode instanceOfKeyword,
        ITypeSyntax type)
    {
        Expression = expression;
        InstanceOfKeyword = instanceOfKeyword;
        Type = type;
    }
    public InstanceOfExpressionSyntax(
        ExpressionSyntax expression,
        ITypeSyntax type)
        : this(
              expression,
              new SyntaxNode(new SyntaxToken(SyntaxKind.InstanceOfKeyword)),
              type
              )
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { Expression, InstanceOfKeyword, Type };
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitInstanceOfExpressionSyntax(this);
    }
}
