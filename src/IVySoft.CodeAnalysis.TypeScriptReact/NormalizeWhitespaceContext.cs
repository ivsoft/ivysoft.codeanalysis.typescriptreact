// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class NormalizeWhitespaceContext
{
    private class WhitespaceState
    {
        private bool _isInline = false;
        private readonly TextWriter? _log;
        private int _indentLevel = 0;
        private WhitespaceRequirements _currentWhitespaceRequirements = WhitespaceRequirements.NoSpaceRequired;

        public WhitespaceState(TextWriter? log)
        {
            _log = log;
        }


        public int IndentLevel
        {
            get => _indentLevel;
            set
            {
                if (_indentLevel < value)
                {
                    _log?.WriteLine($"<++indent {value}>");
                }
                else
                {
                    _log?.WriteLine($"<--indent {value}>");
                }
                _indentLevel = value;
            }
        }

        public WhitespaceRequirements CurrentWhitespaceRequirements
        {
            get => _currentWhitespaceRequirements;
            set
            {
                _log?.WriteLine($"<CurrentWhitespaceRequirements {value}>");
                _currentWhitespaceRequirements = value;
            }
        }

        public bool IsInline
        {
            get => _isInline;
            set
            {
                if (_isInline != value)
                {
                    _isInline = value;
                    _log?.WriteLine($"<IsInline {value}>");
                }
            }
        }

        public void LogWhitespaceRequirements(string prefix, WhitespaceRequirements whitespaceRequirements)
        {
            _log?.WriteLine($"<WhitespaceRequirements prefix={prefix} WhitespaceRequirements={whitespaceRequirements}>");
        }

        internal void LogEnterNode(ISyntaxNode node)
        {
            if (null != _log)
            {
                _log.Write($"<Node {node.GetType().FullName} '");
                node.WriteTo(_log, default).GetAwaiter().GetResult();
                _log.WriteLine("'>");
            }
        }
        internal void LogLeaveNode(ISyntaxNode node)
        {
            if (null != _log)
            {
                _log.Write($"</Node {node.GetType().FullName} '");
                node.WriteTo(_log, default).GetAwaiter().GetResult();
                _log.WriteLine("'>");
            }
        }

        internal void LogTrivia(string text)
        {
            _log?.WriteLine($"<Trivia '{text.Replace("\n","\\n").Replace("\r","\\r")}'>");
        }
    }
    private readonly WhitespaceState _state;

    public NormalizeWhitespaceContext(TextWriter? log)
    {
        _state = new WhitespaceState(log);
    }

    public void Visit(ISyntaxNode node, ISyntaxNode parent)
    {
        var saveInline = _state.IsInline;
        EnterNode(node, parent);

        _state.LogEnterNode(node);
        foreach (var child in node.Children)
        {
            Visit(child, node);
        }
        _state.LogLeaveNode(node);
        LeaveNode(node, parent);
        _state.IsInline = saveInline;
    }

    private void EnterNode(ISyntaxNode node, ISyntaxNode parent)
    {
        if (node is SyntaxNode syntaxNode)
        {
            RewriteLeadingTrivia(syntaxNode, parent);
        }
        else
        {
            switch (node)
            {
            case ImportDirectiveSyntax:
                _state.IsInline = true;
                break;

            case StatementSyntax:
                if(parent is StatementSyntax
                    || parent is InterfaceDefinitionSyntax
                    || parent is LambdaExpressionSyntax
                    || parent is ExpressionBlockSyntax
                    )
                {
                    _state.CurrentWhitespaceRequirements = WhitespaceRequirements.NewLine;
                    _state.IndentLevel += 2;
                }
                break;

            case IInterfaceDefinitionMemberSyntax:
            case INamespaceMemberSyntax:
                if (parent is not CompilationUnitSyntax)
                {
                    _state.IndentLevel += 2;
                }
                break;

            case JsonObjectExpressionSyntax:
                _state.IsInline = false;
                _state.IndentLevel += 2;
                break;

            case JsonObjectMemberDeclaratorSyntax:
                break;

            case JsxElementSyntax:
                _state.IndentLevel += 2;
                _state.CurrentWhitespaceRequirements = WhitespaceRequirements.NewLine;
                _state.IsInline = true;
                break;

            case ExpressionSyntax:
                _state.IsInline = true;
                break;


            default:
                break;
            }
        }
    }
    private void LeaveNode(ISyntaxNode node, ISyntaxNode parent)
    {
        if (node is SyntaxNode syntaxNode)
        {
            UpdateTrailingWhitespaceRequirements(syntaxNode.Token.Kind, parent);
        }
        else
        {
            switch (node)
            {
            case ImportDirectiveSyntax:
                _state.CurrentWhitespaceRequirements = WhitespaceRequirements.NewLine;
                break;

            case StatementSyntax:
                _state.CurrentWhitespaceRequirements = WhitespaceRequirements.NewLine;
                if (parent is StatementSyntax
                    || parent is InterfaceDefinitionSyntax
                    || parent is LambdaExpressionSyntax
                    || parent is ExpressionBlockSyntax
                    )
                {
                    _state.IndentLevel -= 2;
                }
                break;

            case IInterfaceDefinitionMemberSyntax:
            case INamespaceMemberSyntax:
                if (parent is not CompilationUnitSyntax)
                {
                    _state.CurrentWhitespaceRequirements = WhitespaceRequirements.NewLine;
                    _state.IndentLevel -= 2;
                }
                break;

            case JsonObjectExpressionSyntax:
                _state.IndentLevel -= 2;
                break;

            case JsonObjectMemberDeclaratorSyntax:
                _state.CurrentWhitespaceRequirements = WhitespaceRequirements.NewLine;
                break;

            case JsxElementSyntax:
                _state.IndentLevel -= 2;
                _state.CurrentWhitespaceRequirements = WhitespaceRequirements.NewLine;
                break;

            default:
                break;
            }
        }
    }
    private void RewriteLeadingTrivia(SyntaxNode node, ISyntaxNode parent)
    {
#pragma warning disable IDE0010 // Add missing cases
        switch (node.LeadingTrivia.Kind)
        {
        case SyntaxKind.SingleLineCommentTrivia:
            _state.CurrentWhitespaceRequirements = WhitespaceRequirements.NewLine;
            break;

        case SyntaxKind.MultiLineCommentTrivia:
            _state.CurrentWhitespaceRequirements = WhitespaceRequirements.None;
            break;

        case SyntaxKind.WhitespaceTrivia:
            _state.CurrentWhitespaceRequirements = WhitespaceRequirements.SoftSpace;
            break;

        case SyntaxKind.NewLineTrivia:
            node.LeadingTrivia.Text = Environment.NewLine;
            _state.CurrentWhitespaceRequirements = WhitespaceRequirements.NoSpaceRequired;
            break;

        case SyntaxKind.Unknown:
            break;
        }

        var requirements = LeadingWhitespaceRequirements(node.Token.Kind, parent);
        _state.LogWhitespaceRequirements(node.Token.Kind.ToString(), requirements);
        if (_state.CurrentWhitespaceRequirements < requirements)
        {
            _state.CurrentWhitespaceRequirements = requirements;
        }
        switch (_state.CurrentWhitespaceRequirements)
        {
        case WhitespaceRequirements.None:
            node.LeadingTrivia.Text = node.LeadingTrivia.ValueText;
            break;

        case WhitespaceRequirements.SoftSpace:
            node.LeadingTrivia.Text = node.LeadingTrivia.ValueText + " ";
            break;

        case WhitespaceRequirements.NoSpaceRequired:
            node.LeadingTrivia.Text = node.LeadingTrivia.ValueText;
            break;

        case WhitespaceRequirements.HardSpace:
            node.LeadingTrivia.Text = node.LeadingTrivia.ValueText + " ";
            break;

        case WhitespaceRequirements.NewLine:
            node.LeadingTrivia.Text = node.LeadingTrivia.ValueText + Environment.NewLine + new string(' ', _state.IndentLevel);
            break;
        }
#pragma warning restore IDE0010 // Add missing cases
        _state.LogTrivia(node.LeadingTrivia.Text);
    }
    private enum WhitespaceRequirements
    {
        None = 0,
        SoftSpace = 1,
        NoSpaceRequired = 2,
        HardSpace = 3,
        NewLine = 4
    }

    private WhitespaceRequirements LeadingWhitespaceRequirements(SyntaxKind kind, ISyntaxNode parent)
    {
        switch (kind)
        {
        case SyntaxKind.OpenBraceToken:
            return WhitespaceRequirements.SoftSpace;

        case SyntaxKind.ColonToken:
        case SyntaxKind.CommaToken:
        case SyntaxKind.SemicolonToken:
        case SyntaxKind.SimpleMemberAccessExpression:
        case SyntaxKind.SuppressNullableWarningExpression:
        case SyntaxKind.JsxSelfClosingElementClosingBracket:
        case SyntaxKind.JsxElementClosingBracket:
        case SyntaxKind.OpenParenToken:
        case SyntaxKind.CloseParenToken:
        case SyntaxKind.OpenGeneric:
        case SyntaxKind.CloseGeneric:
        case SyntaxKind.JsxElementOpeningBracket:
        case SyntaxKind.JsxClosingElementOpeningBracket:
            return WhitespaceRequirements.NoSpaceRequired;

        case SyntaxKind.EqualsToken:
            if (parent is JsxEqualValueClauseSyntax)
            {
                return WhitespaceRequirements.NoSpaceRequired;
            }
            return WhitespaceRequirements.HardSpace;

        case SyntaxKind.CloseBraceToken:
            return WhitespaceRequirements.SoftSpace;

        case SyntaxKind.SimpleAssignmentExpression:
            return WhitespaceRequirements.SoftSpace;

        case SyntaxKind.LabmdaToken:
            return WhitespaceRequirements.HardSpace;

        case SyntaxKind.Unknown:
        case SyntaxKind.EndOfFileToken:
        case SyntaxKind.SingleLineCommentTrivia:
        case SyntaxKind.MultiLineCommentTrivia:
        case SyntaxKind.NewLineTrivia:
        case SyntaxKind.WhitespaceTrivia:
        case SyntaxKind.ShebangTrivia:
        case SyntaxKind.ConflictMarkerTrivia:
        case SyntaxKind.NumericLiteral:
        case SyntaxKind.StringLiteral:
        case SyntaxKind.JsxText:
        case SyntaxKind.RegularExpressionLiteral:
        case SyntaxKind.NoSubstitutionTemplateLiteral:
        case SyntaxKind.TemplateHead:
        case SyntaxKind.TemplateMiddle:
        case SyntaxKind.TemplateTail:
        case SyntaxKind.OpenBracketToken:
        case SyntaxKind.CloseBracketToken:
        case SyntaxKind.DotDotDotToken:
        case SyntaxKind.LessThanToken:
        case SyntaxKind.LessThanSlashToken:
        case SyntaxKind.GreaterThanToken:
        case SyntaxKind.LessThanEqualsToken:
        case SyntaxKind.GreaterThanEqualsToken:
        case SyntaxKind.EqualsEqualsToken:
        case SyntaxKind.ExclamationEqualsToken:
        case SyntaxKind.EqualsEqualsEqualsToken:
        case SyntaxKind.ExclamationEqualsEqualsToken:
        case SyntaxKind.EqualsGreaterThanToken:
        case SyntaxKind.PlusToken:
        case SyntaxKind.MinusToken:
        case SyntaxKind.AsteriskToken:
        case SyntaxKind.AsteriskAsteriskToken:
        case SyntaxKind.SlashToken:
        case SyntaxKind.PercentToken:
        case SyntaxKind.PlusPlusToken:
        case SyntaxKind.MinusMinusToken:
        case SyntaxKind.LessThanLessThanToken:
        case SyntaxKind.GreaterThanGreaterThanToken:
        case SyntaxKind.GreaterThanGreaterThanGreaterThanToken:
        case SyntaxKind.AmpersandToken:
        case SyntaxKind.BarToken:
        case SyntaxKind.CaretToken:
        case SyntaxKind.ExclamationToken:
        case SyntaxKind.TildeToken:
        case SyntaxKind.AmpersandAmpersandToken:
        case SyntaxKind.BarBarToken:
        case SyntaxKind.QuestionToken:
        case SyntaxKind.AtToken:
        case SyntaxKind.PlusEqualsToken:
        case SyntaxKind.MinusEqualsToken:
        case SyntaxKind.AsteriskEqualsToken:
        case SyntaxKind.AsteriskAsteriskEqualsToken:
        case SyntaxKind.SlashEqualsToken:
        case SyntaxKind.PercentEqualsToken:
        case SyntaxKind.LessThanLessThanEqualsToken:
        case SyntaxKind.GreaterThanGreaterThanEqualsToken:
        case SyntaxKind.GreaterThanGreaterThanGreaterThanEqualsToken:
        case SyntaxKind.AmpersandEqualsToken:
        case SyntaxKind.BarEqualsToken:
        case SyntaxKind.CaretEqualsToken:
        case SyntaxKind.Identifier:
        case SyntaxKind.BreakKeyword:
        case SyntaxKind.CaseKeyword:
        case SyntaxKind.CatchKeyword:
        case SyntaxKind.ClassKeyword:
        case SyntaxKind.ConstKeyword:
        case SyntaxKind.ContinueKeyword:
        case SyntaxKind.DebuggerKeyword:
        case SyntaxKind.DefaultKeyword:
        case SyntaxKind.DeleteKeyword:
        case SyntaxKind.DoKeyword:
        case SyntaxKind.ElseKeyword:
        case SyntaxKind.EnumKeyword:
        case SyntaxKind.ExportKeyword:
        case SyntaxKind.ExtendsKeyword:
        case SyntaxKind.FalseKeyword:
        case SyntaxKind.FinallyKeyword:
        case SyntaxKind.ForKeyword:
        case SyntaxKind.FunctionKeyword:
        case SyntaxKind.IfKeyword:
        case SyntaxKind.ImportKeyword:
        case SyntaxKind.InKeyword:
        case SyntaxKind.InstanceOfKeyword:
        case SyntaxKind.NewKeyword:
        case SyntaxKind.NullKeyword:
        case SyntaxKind.ReturnKeyword:
        case SyntaxKind.SuperKeyword:
        case SyntaxKind.SwitchKeyword:
        case SyntaxKind.ThisKeyword:
        case SyntaxKind.ThrowKeyword:
        case SyntaxKind.TrueKeyword:
        case SyntaxKind.TryKeyword:
        case SyntaxKind.TypeOfKeyword:
        case SyntaxKind.VarKeyword:
        case SyntaxKind.VoidKeyword:
        case SyntaxKind.WhileKeyword:
        case SyntaxKind.WithKeyword:
        case SyntaxKind.ImplementsKeyword:
        case SyntaxKind.InterfaceKeyword:
        case SyntaxKind.LetKeyword:
        case SyntaxKind.PackageKeyword:
        case SyntaxKind.PrivateKeyword:
        case SyntaxKind.ProtectedKeyword:
        case SyntaxKind.PublicKeyword:
        case SyntaxKind.StaticKeyword:
        case SyntaxKind.YieldKeyword:
        case SyntaxKind.AbstractKeyword:
        case SyntaxKind.AsKeyword:
        case SyntaxKind.AnyKeyword:
        case SyntaxKind.AsyncKeyword:
        case SyntaxKind.AwaitKeyword:
        case SyntaxKind.BooleanKeyword:
        case SyntaxKind.ConstructorKeyword:
        case SyntaxKind.DeclareKeyword:
        case SyntaxKind.GetKeyword:
        case SyntaxKind.IsKeyword:
        case SyntaxKind.KeyOfKeyword:
        case SyntaxKind.ModuleKeyword:
        case SyntaxKind.NamespaceKeyword:
        case SyntaxKind.NeverKeyword:
        case SyntaxKind.ReadonlyKeyword:
        case SyntaxKind.RequireKeyword:
        case SyntaxKind.NumberKeyword:
        case SyntaxKind.ObjectKeyword:
        case SyntaxKind.SetKeyword:
        case SyntaxKind.StringKeyword:
        case SyntaxKind.SymbolKeyword:
        case SyntaxKind.TypeKeyword:
        case SyntaxKind.UndefinedKeyword:
        case SyntaxKind.FromKeyword:
        case SyntaxKind.GlobalKeyword:
        case SyntaxKind.OfKeyword:
        case SyntaxKind.QualifiedName:
        case SyntaxKind.ComputedPropertyName:
        case SyntaxKind.TypeParameter:
        case SyntaxKind.Parameter:
        case SyntaxKind.Decorator:
        case SyntaxKind.PropertySignature:
        case SyntaxKind.PropertyDeclaration:
        case SyntaxKind.MethodSignature:
        case SyntaxKind.MethodDeclaration:
        case SyntaxKind.Constructor:
        case SyntaxKind.GetAccessor:
        case SyntaxKind.SetAccessor:
        case SyntaxKind.CallSignature:
        case SyntaxKind.ConstructSignature:
        case SyntaxKind.IndexSignature:
        case SyntaxKind.TypePredicate:
        case SyntaxKind.TypeReference:
        case SyntaxKind.FunctionType:
        case SyntaxKind.ConstructorType:
        case SyntaxKind.TypeQuery:
        case SyntaxKind.TypeLiteral:
        case SyntaxKind.ArrayType:
        case SyntaxKind.TupleType:
        case SyntaxKind.UnionType:
        case SyntaxKind.IntersectionType:
        case SyntaxKind.ParenthesizedType:
        case SyntaxKind.ThisType:
        case SyntaxKind.TypeOperator:
        case SyntaxKind.IndexedAccessType:
        case SyntaxKind.MappedType:
        case SyntaxKind.LiteralType:
        case SyntaxKind.ObjectBindingPattern:
        case SyntaxKind.ArrayBindingPattern:
        case SyntaxKind.BindingElement:
        case SyntaxKind.ArrayLiteralExpression:
        case SyntaxKind.ObjectLiteralExpression:
        case SyntaxKind.PropertyAccessExpression:
        case SyntaxKind.ElementAccessExpression:
        case SyntaxKind.CallExpression:
        case SyntaxKind.NewExpression:
        case SyntaxKind.TaggedTemplateExpression:
        case SyntaxKind.TypeAssertionExpression:
        case SyntaxKind.ParenthesizedExpression:
        case SyntaxKind.FunctionExpression:
        case SyntaxKind.ArrowFunction:
        case SyntaxKind.DeleteExpression:
        case SyntaxKind.TypeOfExpression:
        case SyntaxKind.VoidExpression:
        case SyntaxKind.AwaitExpression:
        case SyntaxKind.PrefixUnaryExpression:
        case SyntaxKind.PostfixUnaryExpression:
        case SyntaxKind.BinaryExpression:
        case SyntaxKind.ConditionalExpression:
        case SyntaxKind.TemplateExpression:
        case SyntaxKind.YieldExpression:
        case SyntaxKind.SpreadElement:
        case SyntaxKind.ClassExpression:
        case SyntaxKind.OmittedExpression:
        case SyntaxKind.ExpressionWithTypeArguments:
        case SyntaxKind.AsExpression:
        case SyntaxKind.NonNullExpression:
        case SyntaxKind.MetaProperty:
        case SyntaxKind.TemplateSpan:
        case SyntaxKind.SemicolonClassElement:
        case SyntaxKind.Block:
        case SyntaxKind.VariableStatement:
        case SyntaxKind.EmptyStatement:
        case SyntaxKind.ExpressionStatement:
        case SyntaxKind.IfStatement:
        case SyntaxKind.DoStatement:
        case SyntaxKind.WhileStatement:
        case SyntaxKind.ForStatement:
        case SyntaxKind.ForInStatement:
        case SyntaxKind.ForOfStatement:
        case SyntaxKind.ContinueStatement:
        case SyntaxKind.BreakStatement:
        case SyntaxKind.ReturnStatement:
        case SyntaxKind.WithStatement:
        case SyntaxKind.SwitchStatement:
        case SyntaxKind.LabeledStatement:
        case SyntaxKind.ThrowStatement:
        case SyntaxKind.TryStatement:
        case SyntaxKind.DebuggerStatement:
        case SyntaxKind.VariableDeclaration:
        case SyntaxKind.VariableDeclarationList:
        case SyntaxKind.FunctionDeclaration:
        case SyntaxKind.ClassDeclaration:
        case SyntaxKind.InterfaceDeclaration:
        case SyntaxKind.TypeAliasDeclaration:
        case SyntaxKind.EnumDeclaration:
        case SyntaxKind.ModuleDeclaration:
        case SyntaxKind.ModuleBlock:
        case SyntaxKind.CaseBlock:
        case SyntaxKind.NamespaceExportDeclaration:
        case SyntaxKind.ImportEqualsDeclaration:
        case SyntaxKind.ImportDeclaration:
        case SyntaxKind.ImportClause:
        case SyntaxKind.NamespaceImport:
        case SyntaxKind.NamedImports:
        case SyntaxKind.ImportSpecifier:
        case SyntaxKind.ExportAssignment:
        case SyntaxKind.ExportDeclaration:
        case SyntaxKind.NamedExports:
        case SyntaxKind.ExportSpecifier:
        case SyntaxKind.MissingDeclaration:
        case SyntaxKind.ExternalModuleReference:
        case SyntaxKind.JsxElement:
        case SyntaxKind.JsxSelfClosingElement:
        case SyntaxKind.JsxOpeningElement:
        case SyntaxKind.JsxClosingElement:
        case SyntaxKind.JsxAttribute:
        case SyntaxKind.JsxAttributes:
        case SyntaxKind.JsxSpreadAttribute:
        case SyntaxKind.JsxExpression:
        case SyntaxKind.CaseClause:
        case SyntaxKind.DefaultClause:
        case SyntaxKind.HeritageClause:
        case SyntaxKind.CatchClause:
        case SyntaxKind.PropertyAssignment:
        case SyntaxKind.ShorthandPropertyAssignment:
        case SyntaxKind.SpreadAssignment:
        case SyntaxKind.EnumMember:
        case SyntaxKind.SourceFile:
        case SyntaxKind.Bundle:
        case SyntaxKind.JsDocTypeExpression:
        case SyntaxKind.JsDocAllType:
        case SyntaxKind.JsDocUnknownType:
        case SyntaxKind.JsDocArrayType:
        case SyntaxKind.JsDocUnionType:
        case SyntaxKind.JsDocTupleType:
        case SyntaxKind.JsDocNullableType:
        case SyntaxKind.JsDocNonNullableType:
        case SyntaxKind.JsDocRecordType:
        case SyntaxKind.JsDocRecordMember:
        case SyntaxKind.JsDocTypeReference:
        case SyntaxKind.JsDocOptionalType:
        case SyntaxKind.JsDocFunctionType:
        case SyntaxKind.JsDocVariadicType:
        case SyntaxKind.JsDocConstructorType:
        case SyntaxKind.JsDocThisType:
        case SyntaxKind.JsDocComment:
        case SyntaxKind.JsDocTag:
        case SyntaxKind.JsDocAugmentsTag:
        case SyntaxKind.JsDocParameterTag:
        case SyntaxKind.JsDocReturnTag:
        case SyntaxKind.JsDocTypeTag:
        case SyntaxKind.JsDocTemplateTag:
        case SyntaxKind.JsDocTypedefTag:
        case SyntaxKind.JsDocPropertyTag:
        case SyntaxKind.JsDocTypeLiteral:
        case SyntaxKind.JsDocLiteralType:
        case SyntaxKind.SyntaxList:
        case SyntaxKind.NotEmittedStatement:
        case SyntaxKind.PartiallyEmittedExpression:
        case SyntaxKind.MergeDeclarationMarker:
        case SyntaxKind.EndOfDeclarationMarker:
        case SyntaxKind.Count:
            return WhitespaceRequirements.SoftSpace;

        default:
            return WhitespaceRequirements.None;
        }
    }
    private void UpdateTrailingWhitespaceRequirements(SyntaxKind kind, ISyntaxNode parent)
    {
        switch (kind)
        {
        case SyntaxKind.SemicolonToken:
        case SyntaxKind.SimpleMemberAccessExpression:
        case SyntaxKind.JsxElementOpeningBracket:
        case SyntaxKind.JsxClosingElementOpeningBracket:
        case SyntaxKind.OpenParenToken:
            _state.CurrentWhitespaceRequirements = WhitespaceRequirements.NoSpaceRequired;
            break;

        case SyntaxKind.OpenBraceToken:
        case SyntaxKind.CloseBraceToken:
            _state.CurrentWhitespaceRequirements =
                _state.IsInline
                ? WhitespaceRequirements.SoftSpace
                : WhitespaceRequirements.NewLine
                ;
            break;

        case SyntaxKind.EqualsToken:
            _state.CurrentWhitespaceRequirements = parent is JsxEqualValueClauseSyntax
                ? WhitespaceRequirements.NoSpaceRequired
                : WhitespaceRequirements.HardSpace;
            break;
        case SyntaxKind.LabmdaToken:
        case SyntaxKind.ReturnKeyword:
            _state.CurrentWhitespaceRequirements = WhitespaceRequirements.HardSpace;
            break;

        case SyntaxKind.ColonToken:
        case SyntaxKind.CommaToken:
        case SyntaxKind.SuppressNullableWarningExpression:
        case SyntaxKind.JsxSelfClosingElementClosingBracket:
        case SyntaxKind.JsxElementClosingBracket:
        case SyntaxKind.CloseParenToken:
        case SyntaxKind.OpenGeneric:
        case SyntaxKind.CloseGeneric:
        case SyntaxKind.SimpleAssignmentExpression:
        case SyntaxKind.Unknown:
        case SyntaxKind.EndOfFileToken:
        case SyntaxKind.SingleLineCommentTrivia:
        case SyntaxKind.MultiLineCommentTrivia:
        case SyntaxKind.NewLineTrivia:
        case SyntaxKind.WhitespaceTrivia:
        case SyntaxKind.ShebangTrivia:
        case SyntaxKind.ConflictMarkerTrivia:
        case SyntaxKind.NumericLiteral:
        case SyntaxKind.StringLiteral:
        case SyntaxKind.JsxText:
        case SyntaxKind.RegularExpressionLiteral:
        case SyntaxKind.NoSubstitutionTemplateLiteral:
        case SyntaxKind.TemplateHead:
        case SyntaxKind.TemplateMiddle:
        case SyntaxKind.TemplateTail:
        case SyntaxKind.OpenBracketToken:
        case SyntaxKind.CloseBracketToken:
        case SyntaxKind.DotDotDotToken:
        case SyntaxKind.LessThanToken:
        case SyntaxKind.LessThanSlashToken:
        case SyntaxKind.GreaterThanToken:
        case SyntaxKind.LessThanEqualsToken:
        case SyntaxKind.GreaterThanEqualsToken:
        case SyntaxKind.EqualsEqualsToken:
        case SyntaxKind.ExclamationEqualsToken:
        case SyntaxKind.EqualsEqualsEqualsToken:
        case SyntaxKind.ExclamationEqualsEqualsToken:
        case SyntaxKind.EqualsGreaterThanToken:
        case SyntaxKind.PlusToken:
        case SyntaxKind.MinusToken:
        case SyntaxKind.AsteriskToken:
        case SyntaxKind.AsteriskAsteriskToken:
        case SyntaxKind.SlashToken:
        case SyntaxKind.PercentToken:
        case SyntaxKind.PlusPlusToken:
        case SyntaxKind.MinusMinusToken:
        case SyntaxKind.LessThanLessThanToken:
        case SyntaxKind.GreaterThanGreaterThanToken:
        case SyntaxKind.GreaterThanGreaterThanGreaterThanToken:
        case SyntaxKind.AmpersandToken:
        case SyntaxKind.BarToken:
        case SyntaxKind.CaretToken:
        case SyntaxKind.ExclamationToken:
        case SyntaxKind.TildeToken:
        case SyntaxKind.AmpersandAmpersandToken:
        case SyntaxKind.BarBarToken:
        case SyntaxKind.QuestionToken:
        case SyntaxKind.AtToken:
        case SyntaxKind.PlusEqualsToken:
        case SyntaxKind.MinusEqualsToken:
        case SyntaxKind.AsteriskEqualsToken:
        case SyntaxKind.AsteriskAsteriskEqualsToken:
        case SyntaxKind.SlashEqualsToken:
        case SyntaxKind.PercentEqualsToken:
        case SyntaxKind.LessThanLessThanEqualsToken:
        case SyntaxKind.GreaterThanGreaterThanEqualsToken:
        case SyntaxKind.GreaterThanGreaterThanGreaterThanEqualsToken:
        case SyntaxKind.AmpersandEqualsToken:
        case SyntaxKind.BarEqualsToken:
        case SyntaxKind.CaretEqualsToken:
        case SyntaxKind.Identifier:
        case SyntaxKind.BreakKeyword:
        case SyntaxKind.CaseKeyword:
        case SyntaxKind.CatchKeyword:
        case SyntaxKind.ClassKeyword:
        case SyntaxKind.ConstKeyword:
        case SyntaxKind.ContinueKeyword:
        case SyntaxKind.DebuggerKeyword:
        case SyntaxKind.DefaultKeyword:
        case SyntaxKind.DeleteKeyword:
        case SyntaxKind.DoKeyword:
        case SyntaxKind.ElseKeyword:
        case SyntaxKind.EnumKeyword:
        case SyntaxKind.ExportKeyword:
        case SyntaxKind.ExtendsKeyword:
        case SyntaxKind.FalseKeyword:
        case SyntaxKind.FinallyKeyword:
        case SyntaxKind.ForKeyword:
        case SyntaxKind.FunctionKeyword:
        case SyntaxKind.IfKeyword:
        case SyntaxKind.ImportKeyword:
        case SyntaxKind.InKeyword:
        case SyntaxKind.InstanceOfKeyword:
        case SyntaxKind.NewKeyword:
        case SyntaxKind.NullKeyword:
        case SyntaxKind.SuperKeyword:
        case SyntaxKind.SwitchKeyword:
        case SyntaxKind.ThisKeyword:
        case SyntaxKind.ThrowKeyword:
        case SyntaxKind.TrueKeyword:
        case SyntaxKind.TryKeyword:
        case SyntaxKind.TypeOfKeyword:
        case SyntaxKind.VarKeyword:
        case SyntaxKind.VoidKeyword:
        case SyntaxKind.WhileKeyword:
        case SyntaxKind.WithKeyword:
        case SyntaxKind.ImplementsKeyword:
        case SyntaxKind.InterfaceKeyword:
        case SyntaxKind.LetKeyword:
        case SyntaxKind.PackageKeyword:
        case SyntaxKind.PrivateKeyword:
        case SyntaxKind.ProtectedKeyword:
        case SyntaxKind.PublicKeyword:
        case SyntaxKind.StaticKeyword:
        case SyntaxKind.YieldKeyword:
        case SyntaxKind.AbstractKeyword:
        case SyntaxKind.AsKeyword:
        case SyntaxKind.AnyKeyword:
        case SyntaxKind.AsyncKeyword:
        case SyntaxKind.AwaitKeyword:
        case SyntaxKind.BooleanKeyword:
        case SyntaxKind.ConstructorKeyword:
        case SyntaxKind.DeclareKeyword:
        case SyntaxKind.GetKeyword:
        case SyntaxKind.IsKeyword:
        case SyntaxKind.KeyOfKeyword:
        case SyntaxKind.ModuleKeyword:
        case SyntaxKind.NamespaceKeyword:
        case SyntaxKind.NeverKeyword:
        case SyntaxKind.ReadonlyKeyword:
        case SyntaxKind.RequireKeyword:
        case SyntaxKind.NumberKeyword:
        case SyntaxKind.ObjectKeyword:
        case SyntaxKind.SetKeyword:
        case SyntaxKind.StringKeyword:
        case SyntaxKind.SymbolKeyword:
        case SyntaxKind.TypeKeyword:
        case SyntaxKind.UndefinedKeyword:
        case SyntaxKind.FromKeyword:
        case SyntaxKind.GlobalKeyword:
        case SyntaxKind.OfKeyword:
            _state.CurrentWhitespaceRequirements = WhitespaceRequirements.SoftSpace;
            break;

        case SyntaxKind.QualifiedName:
        case SyntaxKind.ComputedPropertyName:
        case SyntaxKind.TypeParameter:
        case SyntaxKind.Parameter:
        case SyntaxKind.Decorator:
        case SyntaxKind.PropertySignature:
        case SyntaxKind.PropertyDeclaration:
        case SyntaxKind.MethodSignature:
        case SyntaxKind.MethodDeclaration:
        case SyntaxKind.Constructor:
        case SyntaxKind.GetAccessor:
        case SyntaxKind.SetAccessor:
        case SyntaxKind.CallSignature:
        case SyntaxKind.ConstructSignature:
        case SyntaxKind.IndexSignature:
        case SyntaxKind.TypePredicate:
        case SyntaxKind.TypeReference:
        case SyntaxKind.FunctionType:
        case SyntaxKind.ConstructorType:
        case SyntaxKind.TypeQuery:
        case SyntaxKind.TypeLiteral:
        case SyntaxKind.ArrayType:
        case SyntaxKind.TupleType:
        case SyntaxKind.UnionType:
        case SyntaxKind.IntersectionType:
        case SyntaxKind.ParenthesizedType:
        case SyntaxKind.ThisType:
        case SyntaxKind.TypeOperator:
        case SyntaxKind.IndexedAccessType:
        case SyntaxKind.MappedType:
        case SyntaxKind.LiteralType:
        case SyntaxKind.ObjectBindingPattern:
        case SyntaxKind.ArrayBindingPattern:
        case SyntaxKind.BindingElement:
        case SyntaxKind.ArrayLiteralExpression:
        case SyntaxKind.ObjectLiteralExpression:
        case SyntaxKind.PropertyAccessExpression:
        case SyntaxKind.ElementAccessExpression:
        case SyntaxKind.CallExpression:
        case SyntaxKind.NewExpression:
        case SyntaxKind.TaggedTemplateExpression:
        case SyntaxKind.TypeAssertionExpression:
        case SyntaxKind.ParenthesizedExpression:
        case SyntaxKind.FunctionExpression:
        case SyntaxKind.ArrowFunction:
        case SyntaxKind.DeleteExpression:
        case SyntaxKind.TypeOfExpression:
        case SyntaxKind.VoidExpression:
        case SyntaxKind.AwaitExpression:
        case SyntaxKind.PrefixUnaryExpression:
        case SyntaxKind.PostfixUnaryExpression:
        case SyntaxKind.BinaryExpression:
        case SyntaxKind.ConditionalExpression:
        case SyntaxKind.TemplateExpression:
        case SyntaxKind.YieldExpression:
        case SyntaxKind.SpreadElement:
        case SyntaxKind.ClassExpression:
        case SyntaxKind.OmittedExpression:
        case SyntaxKind.ExpressionWithTypeArguments:
        case SyntaxKind.AsExpression:
        case SyntaxKind.NonNullExpression:
        case SyntaxKind.MetaProperty:
        case SyntaxKind.TemplateSpan:
        case SyntaxKind.SemicolonClassElement:
        case SyntaxKind.Block:
        case SyntaxKind.VariableStatement:
        case SyntaxKind.EmptyStatement:
        case SyntaxKind.ExpressionStatement:
        case SyntaxKind.IfStatement:
        case SyntaxKind.DoStatement:
        case SyntaxKind.WhileStatement:
        case SyntaxKind.ForStatement:
        case SyntaxKind.ForInStatement:
        case SyntaxKind.ForOfStatement:
        case SyntaxKind.ContinueStatement:
        case SyntaxKind.BreakStatement:
        case SyntaxKind.ReturnStatement:
        case SyntaxKind.WithStatement:
        case SyntaxKind.SwitchStatement:
        case SyntaxKind.LabeledStatement:
        case SyntaxKind.ThrowStatement:
        case SyntaxKind.TryStatement:
        case SyntaxKind.DebuggerStatement:
        case SyntaxKind.VariableDeclaration:
        case SyntaxKind.VariableDeclarationList:
        case SyntaxKind.FunctionDeclaration:
        case SyntaxKind.ClassDeclaration:
        case SyntaxKind.InterfaceDeclaration:
        case SyntaxKind.TypeAliasDeclaration:
        case SyntaxKind.EnumDeclaration:
        case SyntaxKind.ModuleDeclaration:
        case SyntaxKind.ModuleBlock:
        case SyntaxKind.CaseBlock:
        case SyntaxKind.NamespaceExportDeclaration:
        case SyntaxKind.ImportEqualsDeclaration:
        case SyntaxKind.ImportDeclaration:
        case SyntaxKind.ImportClause:
        case SyntaxKind.NamespaceImport:
        case SyntaxKind.NamedImports:
        case SyntaxKind.ImportSpecifier:
        case SyntaxKind.ExportAssignment:
        case SyntaxKind.ExportDeclaration:
        case SyntaxKind.NamedExports:
        case SyntaxKind.ExportSpecifier:
        case SyntaxKind.MissingDeclaration:
        case SyntaxKind.ExternalModuleReference:
        case SyntaxKind.JsxElement:
        case SyntaxKind.JsxSelfClosingElement:
        case SyntaxKind.JsxOpeningElement:
        case SyntaxKind.JsxClosingElement:
        case SyntaxKind.JsxAttribute:
        case SyntaxKind.JsxAttributes:
        case SyntaxKind.JsxSpreadAttribute:
        case SyntaxKind.JsxExpression:
        case SyntaxKind.CaseClause:
        case SyntaxKind.DefaultClause:
        case SyntaxKind.HeritageClause:
        case SyntaxKind.CatchClause:
        case SyntaxKind.PropertyAssignment:
        case SyntaxKind.ShorthandPropertyAssignment:
        case SyntaxKind.SpreadAssignment:
        case SyntaxKind.EnumMember:
        case SyntaxKind.SourceFile:
        case SyntaxKind.Bundle:
        case SyntaxKind.JsDocTypeExpression:
        case SyntaxKind.JsDocAllType:
        case SyntaxKind.JsDocUnknownType:
        case SyntaxKind.JsDocArrayType:
        case SyntaxKind.JsDocUnionType:
        case SyntaxKind.JsDocTupleType:
        case SyntaxKind.JsDocNullableType:
        case SyntaxKind.JsDocNonNullableType:
        case SyntaxKind.JsDocRecordType:
        case SyntaxKind.JsDocRecordMember:
        case SyntaxKind.JsDocTypeReference:
        case SyntaxKind.JsDocOptionalType:
        case SyntaxKind.JsDocFunctionType:
        case SyntaxKind.JsDocVariadicType:
        case SyntaxKind.JsDocConstructorType:
        case SyntaxKind.JsDocThisType:
        case SyntaxKind.JsDocComment:
        case SyntaxKind.JsDocTag:
        case SyntaxKind.JsDocAugmentsTag:
        case SyntaxKind.JsDocParameterTag:
        case SyntaxKind.JsDocReturnTag:
        case SyntaxKind.JsDocTypeTag:
        case SyntaxKind.JsDocTemplateTag:
        case SyntaxKind.JsDocTypedefTag:
        case SyntaxKind.JsDocPropertyTag:
        case SyntaxKind.JsDocTypeLiteral:
        case SyntaxKind.JsDocLiteralType:
        case SyntaxKind.SyntaxList:
        case SyntaxKind.NotEmittedStatement:
        case SyntaxKind.PartiallyEmittedExpression:
        case SyntaxKind.MergeDeclarationMarker:
        case SyntaxKind.EndOfDeclarationMarker:
        case SyntaxKind.Count:
        default:
            break;
        }
    }
}
