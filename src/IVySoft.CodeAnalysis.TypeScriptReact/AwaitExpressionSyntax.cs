// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct AwaitExpressionSyntax : ExpressionSyntax
{
    public SyntaxNode AwaitKeyword { get; }
    public ExpressionSyntax Expression { get; }
    public AwaitExpressionSyntax(SyntaxNode awaitKeyword, ExpressionSyntax expression)
    {
        AwaitKeyword = awaitKeyword;
        Expression = expression;
    }
    public AwaitExpressionSyntax(ExpressionSyntax expression)
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.AwaitKeyword)),
              expression
              )
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { AwaitKeyword, Expression };
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitAwaitExpressionSyntax(this);
    }
}
