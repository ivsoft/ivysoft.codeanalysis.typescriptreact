// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class ParameterSyntax : ISyntaxNode
{
    public IdentifierSyntax Identifier { get; }
    public SyntaxNode? Colon { get; }
    public ITypeSyntax? Type { get; }

    public ParameterSyntax(IdentifierSyntax identifier, SyntaxNode? colon, ITypeSyntax? type)
    {
        Identifier = identifier;
        Colon = colon;
        Type = type;
    }
    public ParameterSyntax(IdentifierSyntax identifier)
       : this(identifier, null, null)
    {
    }
    public ParameterSyntax(IdentifierSyntax identifier, ITypeSyntax type)
        : this(identifier, new SyntaxNode(new SyntaxToken(SyntaxKind.ColonToken)), type)
    {
    }

    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            yield return Identifier;
            if(null != Colon)
            {
                yield return Colon;
            }
            if(null != Type)
            {
                yield return Type;
            }
        }
    }

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitFunctionParameterSyntax(this);
    }

    public ParameterSyntax WithType(ITypeSyntax type)
    {
        return new ParameterSyntax(
            Identifier,
            Colon ?? new SyntaxNode(new SyntaxToken(SyntaxKind.ColonToken)),
            type);
    }
}
