// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct BinaryExpressionSyntax : ExpressionSyntax
{
    public ExpressionSyntax Left { get; }
    public SyntaxNode Operator { get; }
    public ExpressionSyntax Right { get; }

    public BinaryExpressionSyntax(
        ExpressionSyntax left,
        SyntaxNode @operator,
        ExpressionSyntax right)
    {
        Left = left;
        Operator = @operator;
        Right = right;
    }

    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { Left, Operator, Right };
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitBinaryExpressionSyntax(this);
    }
}
