// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct GenericTypeSyntax : ITypeSyntax
{
    public IdentifierSyntax Identifier { get; }
    public SyntaxNode OpenBrace { get; }
    public SeparatedSyntaxList<ITypeSyntax> TypeArguments { get; }
    public SyntaxNode CloseBrace { get; }
    public GenericTypeSyntax(IdentifierSyntax identifier, SyntaxNode openBrace, SeparatedSyntaxList<ITypeSyntax> typeArguments, SyntaxNode closeBrace)
    {
        Identifier = identifier;
        OpenBrace = openBrace;
        TypeArguments = typeArguments;
        CloseBrace = closeBrace;
    }
    public GenericTypeSyntax(IdentifierSyntax identifier)
        : this(
              identifier,
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenGeneric)),
              new SeparatedSyntaxList<ITypeSyntax>(),
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseGeneric))
              )
    {
    }

    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { Identifier, OpenBrace, TypeArguments, CloseBrace };
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitGenericTypeSyntax(this);
    }

    public GenericTypeSyntax WithTypeArguments(params ITypeSyntax[] arguments)
    {
        return new GenericTypeSyntax(
            Identifier,
            OpenBrace,
            new SeparatedSyntaxList<ITypeSyntax>(arguments.ToList(), SyntaxKind.CommaToken),
            CloseBrace);
    }
}
