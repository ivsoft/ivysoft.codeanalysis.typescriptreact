// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;
public partial class SyntaxNodeRewriter    
{
    private Rewriter<TResult> Rewrite<TResult>(TResult node)
    {
        return new Rewriter<TResult>(this, node);
    }

    private readonly struct Rewriter<TResult>
    {
        private readonly SyntaxNodeVisitor<ISyntaxNode> _visitor;
        private readonly TResult _node;
        public Rewriter(SyntaxNodeVisitor<ISyntaxNode> visitor, TResult node)
        {
            _visitor = visitor;
            _node = node;
        }
        public Rewriter<TResult, IEnumerable<TArg1>> Rewrite<TArg1>(Func<TResult, IEnumerable<TArg1>> property) where TArg1 : ISyntaxNode
        {
            return Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, IEnumerable<TArg1>>(source._visitor, source._node, changed, arg));
        }
        public Rewriter<TResult, TArg1>? Rewrite<TArg1>(Func<TResult, TArg1> property) where TArg1 : ISyntaxNode
        {
            return Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1>(source._visitor, source._node, changed, arg));
        }
        public Rewriter<TResult, TArg1?> RewriteNullabe<TArg1>(Func<TResult, TArg1?> property) where TArg1 : ISyntaxNode
        {
            return RewriteNullabe(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1?>(source._visitor, source._node, changed, arg));
        }
        internal static TResultRewriter Rewrite<TSourceRewriter, TArg, TResultRewriter>(
            TSourceRewriter source,
            SyntaxNodeVisitor<ISyntaxNode> visitor,
            IEnumerable<TArg> collection,
            Func<TSourceRewriter, bool, IEnumerable<TArg>, TResultRewriter> resultRewriter) where TArg : ISyntaxNode
        {
            var result = new List<TArg>();
            bool changed = false;
            foreach (var item in collection)
            {
                var newItem = (TArg?)item.Accept<ISyntaxNode>(visitor);
                if (null == newItem)
                {
                    changed = true;
                }
                else
                {
                    if (!ReferenceEquals(newItem, item))
                    {
                        changed = true;
                    }
                    result.Add(newItem);
                }
            }

            if (changed)
            {
                return resultRewriter(source, true, result);
            }
            else
            {
                return resultRewriter(source, false, collection);
            }
        }
        internal static TResultRewriter? Rewrite<TSourceRewriter, TArg, TResultRewriter>(
            TSourceRewriter source,
            SyntaxNodeVisitor<ISyntaxNode> visitor,
            TArg arg,
            Func<TSourceRewriter, bool, TArg, TResultRewriter> resultRewriter) where TArg : ISyntaxNode
        {
            var newValue = (TArg?)arg.Accept<ISyntaxNode>(visitor);
            if(null == newValue)
            {
                return default;
            }
            if (ReferenceEquals(newValue, arg))
            {
                return resultRewriter(source, false, arg);
            }
            return resultRewriter(source, true, newValue);
        }
        internal static TResultRewriter RewriteNullabe<TSourceRewriter, TArg, TResultRewriter>(
            TSourceRewriter source,
            SyntaxNodeVisitor<ISyntaxNode> visitor,
            TArg? arg,
            Func<TSourceRewriter, bool, TArg?, TResultRewriter> resultRewriter) where TArg : ISyntaxNode
        {
            if (null == arg)
            {
                return resultRewriter(source, false, arg);
            }
            var newValue = (TArg?)arg.Accept<ISyntaxNode>(visitor);
            if (null != newValue && ReferenceEquals(newValue, arg))
            {
                return resultRewriter(source, false, arg);
            }
            return resultRewriter(source, true, newValue);
        }
    }
    private readonly struct Rewriter<TResult, TArg1>
    {
        private readonly SyntaxNodeVisitor<ISyntaxNode> _visitor;
        private readonly TResult _node;
        private readonly bool _changed;
        private readonly TArg1 _arg1;       
        public Rewriter(SyntaxNodeVisitor<ISyntaxNode> visitor, TResult node, bool changed, TArg1 arg1)
        {
            _visitor = visitor;
            _node = node;
            _changed = changed;
            _arg1 = arg1;
        }
        public Rewriter<TResult, TArg1, TArg2>? Rewrite<TArg2>(Func<TResult, TArg2> property) where TArg2 : ISyntaxNode
        {
            return Rewriter<TResult>.Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1, TArg2>(source._visitor, source._node, source._changed || changed, source._arg1, arg));
        }
        public Rewriter<TResult, TArg1, IEnumerable<TArg2>> Rewrite<TArg2>(Func<TResult, IEnumerable<TArg2>> property) where TArg2 : ISyntaxNode
        {
            return Rewriter<TResult>.Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1, IEnumerable<TArg2>>(source._visitor, source._node, source._changed || changed, source._arg1, arg));
        }
        public Rewriter<TResult, TArg1, TArg2?> RewriteNullable<TArg2>(Func<TResult, TArg2?> property) where TArg2 : ISyntaxNode
        {
            return Rewriter<TResult>.RewriteNullabe(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1, TArg2?>(source._visitor, source._node, source._changed || changed, source._arg1, arg));
        }
        public TResult GetResult(Func<TArg1, TResult> generator)
        {
            if (!_changed)
            {
                return _node;
            }
            return generator(_arg1);
        }
    }
    private readonly struct Rewriter<TResult, TArg1, TArg2>
    {
        private readonly SyntaxNodeVisitor<ISyntaxNode> _visitor;
        private readonly TResult _node;
        private readonly bool _changed;
        private readonly TArg1 _arg1;
        private readonly TArg2 _arg2;
        public Rewriter(SyntaxNodeVisitor<ISyntaxNode> visitor, TResult node, bool changed, TArg1 arg1, TArg2 arg2)
        {
            _visitor = visitor;
            _node = node;
            _changed = changed;
            _arg1 = arg1;
            _arg2 = arg2;
        }
        public Rewriter<TResult, TArg1, TArg2, TArg3>? Rewrite<TArg3>(Func<TResult, TArg3> property) where TArg3 : ISyntaxNode
        {
            return Rewriter<TResult>.Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1, TArg2, TArg3>(source._visitor, source._node, source._changed || changed, source._arg1, source._arg2, arg));
        }
        public Rewriter<TResult, TArg1, TArg2, IEnumerable<TArg3>> Rewrite<TArg3>(Func<TResult, IEnumerable<TArg3>> property) where TArg3 : ISyntaxNode
        {
            return Rewriter<TResult>.Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1, TArg2, IEnumerable<TArg3>>(source._visitor, source._node, source._changed || changed, source._arg1, source._arg2, arg));
        }
        public Rewriter<TResult, TArg1, TArg2, TArg3?> RewriteNullable<TArg3>(Func<TResult, TArg3?> property) where TArg3 : ISyntaxNode
        {
            return Rewriter<TResult>.RewriteNullabe(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1, TArg2, TArg3?>(source._visitor, source._node, source._changed || changed, source._arg1, source._arg2, arg));
        }
        public TResult GetResult(Func<TArg1, TArg2, TResult> generator)
        {
            if (!_changed)
            {
                return _node;
            }
            return generator(_arg1, _arg2);
        }
    }
    private readonly struct Rewriter<TResult, TArg1, TArg2, TArg3>
    {
        private readonly SyntaxNodeVisitor<ISyntaxNode> _visitor;
        private readonly TResult _node;
        private readonly bool _changed;
        private readonly TArg1 _arg1;
        private readonly TArg2 _arg2;
        private readonly TArg3 _arg3;
        public Rewriter(SyntaxNodeVisitor<ISyntaxNode> visitor, TResult node, bool changed, TArg1 arg1, TArg2 arg2, TArg3 arg3)
        {
            _visitor = visitor;
            _node = node;
            _changed = changed;
            _arg1 = arg1;
            _arg2 = arg2;
            _arg3 = arg3;
        }
        public Rewriter<TResult, TArg1, TArg2, TArg3, TArg4>? Rewrite<TArg4>(Func<TResult, TArg4> property) where TArg4 : ISyntaxNode
        {
            return Rewriter<TResult>.Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1, TArg2, TArg3, TArg4>(source._visitor, source._node, source._changed || changed, source._arg1, source._arg2, source._arg3, arg));
        }
        public Rewriter<TResult, TArg1, TArg2, TArg3, IEnumerable<TArg4>> Rewrite<TArg4>(Func<TResult, IEnumerable<TArg4>> property) where TArg4 : ISyntaxNode
        {
            return Rewriter<TResult>.Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1, TArg2, TArg3, IEnumerable<TArg4>>(source._visitor, source._node, source._changed || changed, source._arg1, source._arg2, source._arg3, arg));
        }
        public Rewriter<TResult, TArg1, TArg2, TArg3, TArg4?> RewriteNullable<TArg4>(Func<TResult, TArg4?> property) where TArg4 : ISyntaxNode
        {
            return Rewriter<TResult>.RewriteNullabe(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1, TArg2, TArg3, TArg4?>(source._visitor, source._node, source._changed || changed, source._arg1, source._arg2, source._arg3, arg));
        }
        public TResult GetResult(Func<TArg1, TArg2, TArg3, TResult> generator)
        {
            if (!_changed)
            {
                return _node;
            }
            return generator(_arg1, _arg2, _arg3);
        }
    }
    private readonly struct Rewriter<TResult, TArg1, TArg2, TArg3, TArg4>
    {
        private readonly SyntaxNodeVisitor<ISyntaxNode> _visitor;
        private readonly TResult _node;
        private readonly bool _changed;
        private readonly TArg1 _arg1;
        private readonly TArg2 _arg2;
        private readonly TArg3 _arg3;
        private readonly TArg4 _arg4;
        public Rewriter(SyntaxNodeVisitor<ISyntaxNode> visitor, TResult node, bool changed, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4)
        {
            _visitor = visitor;
            _node = node;
            _changed = changed;
            _arg1 = arg1;
            _arg2 = arg2;
            _arg3 = arg3;
            _arg4 = arg4;
        }
        public Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, TArg5>? Rewrite<TArg5>(Func<TResult, TArg5> property) where TArg5 : ISyntaxNode
        {
            return Rewriter<TResult>.Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, TArg5>(source._visitor, source._node, source._changed || changed, source._arg1, source._arg2, source._arg3, source._arg4, arg));
        }
        public Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, IEnumerable<TArg5>> Rewrite<TArg5>(Func<TResult, IEnumerable<TArg5>> property) where TArg5 : ISyntaxNode
        {
            return Rewriter<TResult>.Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, IEnumerable<TArg5>>(source._visitor, source._node, source._changed || changed, source._arg1, source._arg2, source._arg3, source._arg4, arg));
        }
        public Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, TArg5?> RewriteNullable<TArg5>(Func<TResult, TArg5?> property) where TArg5 : ISyntaxNode
        {
            return Rewriter<TResult>.RewriteNullabe(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, TArg5?>(source._visitor, source._node, source._changed || changed, source._arg1, source._arg2, source._arg3, source._arg4, arg));
        }
        public TResult GetResult(Func<TArg1, TArg2, TArg3, TArg4, TResult> generator)
        {
            if (!_changed)
            {
                return _node;
            }
            return generator(_arg1, _arg2, _arg3, _arg4);
        }
    }
    private readonly struct Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, TArg5>
    {
        private readonly SyntaxNodeVisitor<ISyntaxNode> _visitor;
        private readonly TResult _node;
        private readonly bool _changed;
        private readonly TArg1 _arg1;
        private readonly TArg2 _arg2;
        private readonly TArg3 _arg3;
        private readonly TArg4 _arg4;
        private readonly TArg5 _arg5;
        public Rewriter(SyntaxNodeVisitor<ISyntaxNode> visitor, TResult node, bool changed, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4, TArg5 arg5)
        {
            _visitor = visitor;
            _node = node;
            _changed = changed;
            _arg1 = arg1;
            _arg2 = arg2;
            _arg3 = arg3;
            _arg4 = arg4;
            _arg5 = arg5;
        }
        public Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6>? Rewrite<TArg6>(Func<TResult, TArg6> property) where TArg6 : ISyntaxNode
        {
            return Rewriter<TResult>.Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6>(source._visitor, source._node, source._changed || changed, source._arg1, source._arg2, source._arg3, source._arg4, source._arg5, arg));
        }
        public Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, TArg5, IEnumerable<TArg6>> Rewrite<TArg6>(Func<TResult, IEnumerable<TArg6>> property) where TArg6 : ISyntaxNode
        {
            return Rewriter<TResult>.Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, TArg5, IEnumerable<TArg6>>(source._visitor, source._node, source._changed || changed, source._arg1, source._arg2, source._arg3, source._arg4, source._arg5, arg));
        }
        public Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6?> RewriteNullable<TArg6>(Func<TResult, TArg6?> property) where TArg6 : ISyntaxNode
        {
            return Rewriter<TResult>.RewriteNullabe(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6?>(source._visitor, source._node, source._changed || changed, source._arg1, source._arg2, source._arg3, source._arg4, source._arg5, arg));
        }
        public TResult GetResult(Func<TArg1, TArg2, TArg3, TArg4, TArg5, TResult> generator)
        {
            if (!_changed)
            {
                return _node;
            }
            return generator(_arg1, _arg2, _arg3, _arg4, _arg5);
        }
    }
    private readonly struct Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6>
    {
        private readonly SyntaxNodeVisitor<ISyntaxNode> _visitor;
        private readonly TResult _node;
        private readonly bool _changed;
        private readonly TArg1 _arg1;
        private readonly TArg2 _arg2;
        private readonly TArg3 _arg3;
        private readonly TArg4 _arg4;
        private readonly TArg5 _arg5;
        private readonly TArg6 _arg6;
        public Rewriter(
            SyntaxNodeVisitor<ISyntaxNode> visitor,
            TResult node,
            bool changed,
            TArg1 arg1,
            TArg2 arg2,
            TArg3 arg3,
            TArg4 arg4,
            TArg5 arg5,
            TArg6 arg6)
        {
            _visitor = visitor;
            _node = node;
            _changed = changed;
            _arg1 = arg1;
            _arg2 = arg2;
            _arg3 = arg3;
            _arg4 = arg4;
            _arg5 = arg5;
            _arg6 = arg6;
        }
        public Rewriter<
            TResult,
            TArg1,
            TArg2,
            TArg3,
            TArg4,
            TArg5,
            TArg6,
            TArg7
            >? Rewrite<TArg7>(Func<TResult, TArg7> property) where TArg7 : ISyntaxNode
        {
            return Rewriter<TResult>.Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7>(
                    source._visitor,
                    source._node,
                    source._changed|| changed,
                    source._arg1,
                    source._arg2,
                    source._arg3,
                    source._arg4,
                    source._arg5,
                    source._arg6,
                    arg
                    ));
        }
        public Rewriter<
            TResult,
            TArg1,
            TArg2,
            TArg3,
            TArg4,
            TArg5,
            TArg6,
            IEnumerable<TArg7>
            > Rewrite<TArg7>(Func<TResult, IEnumerable<TArg7>> property)
            where TArg7 : ISyntaxNode
        {
            return Rewriter<TResult>.Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<
                    TResult,
                    TArg1,
                    TArg2,
                    TArg3,
                    TArg4,
                    TArg5,
                    TArg6,
                    IEnumerable<TArg7>
                    >(
                    source._visitor,
                    source._node,
                    source._changed || changed,
                    source._arg1,
                    source._arg2,
                    source._arg3,
                    source._arg4,
                    source._arg5,
                    source._arg6,
                    arg));
        }
        public Rewriter<
            TResult,
            TArg1,
            TArg2,
            TArg3,
            TArg4,
            TArg5,
            TArg6,
            TArg7?
            > RewriteNullable<TArg7>(Func<TResult, TArg7?> property)
            where TArg7 : ISyntaxNode
        {
            return Rewriter<TResult>.RewriteNullabe(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<
                    TResult,
                    TArg1,
                    TArg2,
                    TArg3,
                    TArg4,
                    TArg5,
                    TArg6,
                    TArg7?
                    >(
                    source._visitor,
                    source._node,
                    source._changed || changed,
                    source._arg1,
                    source._arg2,
                    source._arg3,
                    source._arg4,
                    source._arg5,
                    source._arg6,
                    arg
                    ));
        }
        public TResult GetResult(Func<
            TArg1,
            TArg2,
            TArg3,
            TArg4,
            TArg5,
            TArg6,
            TResult> generator)
        {
            if (!_changed)
            {
                return _node;
            }
            return generator(
                _arg1,
                _arg2,
                _arg3,
                _arg4,
                _arg5,
                _arg6
                );
        }
    }
    private readonly struct Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7>
    {
        private readonly SyntaxNodeVisitor<ISyntaxNode> _visitor;
        private readonly TResult _node;
        private readonly bool _changed;
        private readonly TArg1 _arg1;
        private readonly TArg2 _arg2;
        private readonly TArg3 _arg3;
        private readonly TArg4 _arg4;
        private readonly TArg5 _arg5;
        private readonly TArg6 _arg6;
        private readonly TArg7 _arg7;
        public Rewriter(
            SyntaxNodeVisitor<ISyntaxNode> visitor,
            TResult node,
            bool changed,
            TArg1 arg1,
            TArg2 arg2,
            TArg3 arg3,
            TArg4 arg4,
            TArg5 arg5,
            TArg6 arg6,
            TArg7 arg7
            )
        {
            _visitor = visitor;
            _node = node;
            _changed = changed;
            _arg1 = arg1;
            _arg2 = arg2;
            _arg3 = arg3;
            _arg4 = arg4;
            _arg5 = arg5;
            _arg6 = arg6;
            _arg7 = arg7;
        }
        public Rewriter<
            TResult,
            TArg1,
            TArg2,
            TArg3,
            TArg4,
            TArg5,
            TArg6,
            TArg7,
            TArg8
            >? Rewrite<TArg8>(Func<TResult, TArg8> property) where TArg8 : ISyntaxNode
        {
            return Rewriter<TResult>.Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<
                    TResult,
                    TArg1,
                    TArg2,
                    TArg3,
                    TArg4,
                    TArg5,
                    TArg6,
                    TArg7,
                    TArg8
                    >(
                    source._visitor,
                    source._node,
                    source._changed || changed,
                    source._arg1,
                    source._arg2,
                    source._arg3,
                    source._arg4,
                    source._arg5,
                    source._arg6,
                    source._arg7,
                    arg
                    ));
        }
        public Rewriter<
            TResult,
            TArg1,
            TArg2,
            TArg3,
            TArg4,
            TArg5,
            TArg6,
            TArg7,
            IEnumerable<TArg8>
            > Rewrite<TArg8>(Func<TResult, IEnumerable<TArg8>> property)
            where TArg8 : ISyntaxNode
        {
            return Rewriter<TResult>.Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<
                    TResult,
                    TArg1,
                    TArg2,
                    TArg3,
                    TArg4,
                    TArg5,
                    TArg6,
                    TArg7,
                    IEnumerable<TArg8>
                    >(
                    source._visitor,
                    source._node,
                    source._changed || changed,
                    source._arg1,
                    source._arg2,
                    source._arg3,
                    source._arg4,
                    source._arg5,
                    source._arg6,
                    source._arg7,
                    arg));
        }
        public Rewriter<
            TResult,
            TArg1,
            TArg2,
            TArg3,
            TArg4,
            TArg5,
            TArg6,
            TArg7,
            TArg8?
            > RewriteNullable<TArg8>(Func<TResult, TArg8?> property)
            where TArg8 : ISyntaxNode
        {
            return Rewriter<TResult>.RewriteNullabe(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<
                    TResult,
                    TArg1,
                    TArg2,
                    TArg3,
                    TArg4,
                    TArg5,
                    TArg6,
                    TArg7,
                    TArg8?
                    >(
                    source._visitor,
                    source._node,
                    source._changed || changed,
                    source._arg1,
                    source._arg2,
                    source._arg3,
                    source._arg4,
                    source._arg5,
                    source._arg6,
                    source._arg7,
                    arg
                    ));
        }
        public TResult GetResult(Func<
            TArg1,
            TArg2,
            TArg3,
            TArg4,
            TArg5,
            TArg6,
            TArg7,
            TResult> generator)
        {
            if (!_changed)
            {
                return _node;
            }
            return generator(
                _arg1,
                _arg2,
                _arg3,
                _arg4,
                _arg5,
                _arg6,
                _arg7
                );
        }
    }
    private readonly struct Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8>
    {
        private readonly SyntaxNodeVisitor<ISyntaxNode> _visitor;
        private readonly TResult _node;
        private readonly bool _changed;
        private readonly TArg1 _arg1;
        private readonly TArg2 _arg2;
        private readonly TArg3 _arg3;
        private readonly TArg4 _arg4;
        private readonly TArg5 _arg5;
        private readonly TArg6 _arg6;
        private readonly TArg7 _arg7;
        private readonly TArg8 _arg8;
        public Rewriter(
            SyntaxNodeVisitor<ISyntaxNode> visitor,
            TResult node,
            bool changed,
            TArg1 arg1,
            TArg2 arg2,
            TArg3 arg3,
            TArg4 arg4,
            TArg5 arg5,
            TArg6 arg6,
            TArg7 arg7,
            TArg8 arg8
            )
        {
            _visitor = visitor;
            _node = node;
            _changed = changed;
            _arg1 = arg1;
            _arg2 = arg2;
            _arg3 = arg3;
            _arg4 = arg4;
            _arg5 = arg5;
            _arg6 = arg6;
            _arg7 = arg7;
            _arg8 = arg8;
        }
        public Rewriter<
            TResult,
            TArg1,
            TArg2,
            TArg3,
            TArg4,
            TArg5,
            TArg6,
            TArg7,
            TArg8,
            TArg9
            >? Rewrite<TArg9>(Func<TResult, TArg9> property) where TArg9 : ISyntaxNode
        {
            return Rewriter<TResult>.Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<
                    TResult,
                    TArg1,
                    TArg2,
                    TArg3,
                    TArg4,
                    TArg5,
                    TArg6,
                    TArg7,
                    TArg8,
                    TArg9
                    >(
                    source._visitor,
                    source._node,
                    source._changed || changed,
                    source._arg1,
                    source._arg2,
                    source._arg3,
                    source._arg4,
                    source._arg5,
                    source._arg6,
                    source._arg7,
                    source._arg8,
                    arg
                    ));
        }
        public Rewriter<
            TResult,
            TArg1,
            TArg2,
            TArg3,
            TArg4,
            TArg5,
            TArg6,
            TArg7,
            TArg8,
            IEnumerable<TArg9>
            > Rewrite<TArg9>(Func<TResult, IEnumerable<TArg9>> property)
            where TArg9 : ISyntaxNode
        {
            return Rewriter<TResult>.Rewrite(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<
                    TResult,
                    TArg1,
                    TArg2,
                    TArg3,
                    TArg4,
                    TArg5,
                    TArg6,
                    TArg7,
                    TArg8,
                    IEnumerable<TArg9>
                    >(
                    source._visitor,
                    source._node,
                    source._changed || changed,
                    source._arg1,
                    source._arg2,
                    source._arg3,
                    source._arg4,
                    source._arg5,
                    source._arg6,
                    source._arg7,
                    source._arg8,
                    arg));
        }
        public Rewriter<
            TResult,
            TArg1,
            TArg2,
            TArg3,
            TArg4,
            TArg5,
            TArg6,
            TArg7,
            TArg8,
            TArg9?
            > RewriteNullable<TArg9>(Func<TResult, TArg9?> property)
            where TArg9 : ISyntaxNode
        {
            return Rewriter<TResult>.RewriteNullabe(
                this,
                _visitor,
                property(_node),
                (source, changed, arg) => new Rewriter<
                    TResult,
                    TArg1,
                    TArg2,
                    TArg3,
                    TArg4,
                    TArg5,
                    TArg6,
                    TArg7,
                    TArg8,
                    TArg9?
                    >(
                    source._visitor,
                    source._node,
                    source._changed || changed,
                    source._arg1,
                    source._arg2,
                    source._arg3,
                    source._arg4,
                    source._arg5,
                    source._arg6,
                    source._arg7,
                    source._arg8,
                    arg
                    ));
        }
        public TResult GetResult(Func<
            TArg1,
            TArg2,
            TArg3,
            TArg4,
            TArg5,
            TArg6,
            TArg7,
            TArg8,
            TResult> generator)
        {
            if (!_changed)
            {
                return _node;
            }
            return generator(
                _arg1,
                _arg2,
                _arg3,
                _arg4,
                _arg5,
                _arg6,
                _arg7,
                _arg8
                );
        }
    }
    private readonly struct Rewriter<TResult, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9>
    {
        private readonly SyntaxNodeVisitor<ISyntaxNode> _visitor;
        private readonly TResult _node;
        private readonly bool _changed;
        private readonly TArg1 _arg1;
        private readonly TArg2 _arg2;
        private readonly TArg3 _arg3;
        private readonly TArg4 _arg4;
        private readonly TArg5 _arg5;
        private readonly TArg6 _arg6;
        private readonly TArg7 _arg7;
        private readonly TArg8 _arg8;
        private readonly TArg9 _arg9;
        public Rewriter(
            SyntaxNodeVisitor<ISyntaxNode> visitor,
            TResult node,
            bool changed,
            TArg1 arg1,
            TArg2 arg2,
            TArg3 arg3,
            TArg4 arg4,
            TArg5 arg5,
            TArg6 arg6,
            TArg7 arg7,
            TArg8 arg8,
            TArg9 arg9
            )
        {
            _visitor = visitor;
            _node = node;
            _changed = changed;
            _arg1 = arg1;
            _arg2 = arg2;
            _arg3 = arg3;
            _arg4 = arg4;
            _arg5 = arg5;
            _arg6 = arg6;
            _arg7 = arg7;
            _arg8 = arg8;
            _arg9 = arg9;
        }
        public TResult GetResult(Func<
            TArg1,
            TArg2,
            TArg3,
            TArg4,
            TArg5,
            TArg6,
            TArg7,
            TArg8,
            TArg9,
            TResult
            > generator)
        {
            if (!_changed)
            {
                return _node;
            }
            return generator(
                _arg1,
                _arg2,
                _arg3,
                _arg4,
                _arg5,
                _arg6,
                _arg7,
                _arg8,
                _arg9
                );
        }
    }
}
