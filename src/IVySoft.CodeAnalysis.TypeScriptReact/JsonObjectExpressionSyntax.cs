// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct JsonObjectExpressionSyntax : ExpressionSyntax
{
    public SyntaxNode OpeningBracket { get; }
    public SeparatedSyntaxList<IJsonObjectMember> Members { get; }
    public SyntaxNode ClosingBracket { get; }
    public JsonObjectExpressionSyntax(SyntaxNode openingBracket, SeparatedSyntaxList<IJsonObjectMember> members, SyntaxNode closingBracket)
    {
        OpeningBracket = openingBracket;
        Members = members;
        ClosingBracket = closingBracket;
    }
    public JsonObjectExpressionSyntax(SeparatedSyntaxList<IJsonObjectMember> members)
        : this(new SyntaxNode(new SyntaxToken(SyntaxKind.OpenBraceToken)), members, new SyntaxNode(new SyntaxToken(SyntaxKind.CloseBraceToken)))
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { OpeningBracket, Members, ClosingBracket };
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitJsonObjectExpressionSyntax(this);
    }
}
