// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Text;

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public interface ISyntaxNode
{
    IEnumerable<ISyntaxNode> Children { get; }
    T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class;
}

public static class SyntaxNodeExtensions
{
    public static async ValueTask WriteTo(this ISyntaxNode node, TextWriter stream, CancellationToken cancellationToken)
    {
        if(node is SyntaxNode syntaxNode)
        {
            await stream.WriteAsync(syntaxNode.LeadingTrivia.Text);
            await stream.WriteAsync(syntaxNode.Token.Text);
            await stream.WriteAsync(syntaxNode.TrailingTrivia.Text);
        }
        foreach (var child in node.Children)
        {
            await child.WriteTo(stream, cancellationToken);
        }
    }
    public static void WriteTo(this ISyntaxNode node, StringBuilder sb)
    {
        if (node is SyntaxNode syntaxNode)
        {
            _ = sb
                .Append(syntaxNode.LeadingTrivia.Text)
                .Append(syntaxNode.Token.Text)
                .Append(syntaxNode.TrailingTrivia.Text)
                ;
        }
        foreach (var child in node.Children)
        {
            child.WriteTo(sb);
        }
    }
    public static IEnumerable<ISyntaxNode> DescendantsOrSelf(this ISyntaxNode node)
    {
        yield return node;
        foreach (var child in node.Children)
        {
            foreach(var item in child.DescendantsOrSelf())
            {
                yield return item;                     
            }
        }
    }
}
