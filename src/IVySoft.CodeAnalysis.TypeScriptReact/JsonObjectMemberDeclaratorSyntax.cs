// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Text;

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public sealed class JsonObjectMemberDeclaratorSyntax : IJsonObjectMember
{
    public IdentifierSyntax Identifier { get; }
    public SyntaxNode? Colon { get; }
    public ExpressionSyntax? Value { get; }
    public JsonObjectMemberDeclaratorSyntax(IdentifierSyntax identifier, SyntaxNode? colon, ExpressionSyntax? value)
    {
        Identifier = identifier;
        Colon = colon;
        Value = value;
    }
    public JsonObjectMemberDeclaratorSyntax(IdentifierSyntax identifier, ExpressionSyntax value)
        : this(identifier, new SyntaxNode(new SyntaxToken(SyntaxKind.ColonToken)), value)
    {
    }
    public JsonObjectMemberDeclaratorSyntax(IdentifierSyntax identifier)
        : this(identifier, null, null)
    {
    }
    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            yield return Identifier;
            if(null != Colon)
            {
                yield return Colon;
            }
            if(null != Value)
            {
                yield return Value;
            }
        }
    }

    public override string ToString()
    {
        var sb = new StringBuilder();
        this.WriteTo(sb);
        return sb.ToString();
    }
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitJsonObjectMemberDeclaratorSyntax(this);
    }
}
