// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct TypeofExpressionSyntax : ExpressionSyntax
{
    public SyntaxNode TypeofKeyword { get; }
    public ExpressionSyntax Expression { get; }
    public TypeofExpressionSyntax(SyntaxNode typeofKeyword, ExpressionSyntax expression)
    {
        TypeofKeyword = typeofKeyword;
        Expression = expression;
    }
    public TypeofExpressionSyntax(ExpressionSyntax expression)
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.TypeOfKeyword)),
              expression
              )
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { TypeofKeyword, Expression };

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitTypeofExpressionSyntax(this);
    }
}
