// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct EnumMemberSyntax : ISyntaxNode
{
    public IdentifierSyntax Identifier { get; }
    public EqualsValueClauseSyntax? Value { get; }

    public EnumMemberSyntax(IdentifierSyntax identifier, EqualsValueClauseSyntax? value)
    {
        Identifier = identifier;
        Value = value;
    }
    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            yield return Identifier;
            if(null != Value)
            {
                yield return Value;
            }
        }
    }

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitEnumMemberSyntax(this);
    }
}
