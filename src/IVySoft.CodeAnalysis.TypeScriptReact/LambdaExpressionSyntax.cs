// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct LambdaExpressionSyntax : ExpressionSyntax
{
    public SyntaxNode[] Modifiers { get; }
    public SyntaxNode OpeningParen { get; }
    public SeparatedSyntaxList<ParameterSyntax> Parameters { get; }
    public SyntaxNode ClosingParen { get; }

    public SyntaxNode LambdaOperator { get; }
    public ExpressionSyntax Body { get; }

    public LambdaExpressionSyntax(
        SyntaxNode[] modifiers,
        SyntaxNode openingParen,
        SeparatedSyntaxList<ParameterSyntax> parameters,
        SyntaxNode closingParen,
        SyntaxNode lambdaOperator,
        ExpressionSyntax body)
    {
        Modifiers = modifiers;
        OpeningParen = openingParen;
        Parameters = parameters;
        ClosingParen = closingParen;
        LambdaOperator = lambdaOperator;
        Body = body;
    }
    public LambdaExpressionSyntax()
        : this(
              new SyntaxNode[0],
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenParenToken)),
              new SeparatedSyntaxList<ParameterSyntax>(),
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseParenToken)),
              new SyntaxNode(new SyntaxToken(SyntaxKind.LabmdaToken)),
              new ExpressionBlockSyntax()
              )
    {
    }
    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            foreach (var modifier in Modifiers)
            {
                yield return modifier;
            }
            yield return OpeningParen;
            yield return Parameters;
            yield return ClosingParen;
            yield return LambdaOperator;
            yield return Body;
        }
    }
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitLambdaExpressionSyntax(this);
    }

    public LambdaExpressionSyntax WithModifiers(params SyntaxNode[] modifiers)
    {
        return new LambdaExpressionSyntax(
            modifiers,
            OpeningParen,
            Parameters,
            ClosingParen,
            LambdaOperator,
            Body);
    }
    public LambdaExpressionSyntax WithParameters(params ParameterSyntax[] parameters)
    {
        return new LambdaExpressionSyntax(
            Modifiers,
            OpeningParen,
            new SeparatedSyntaxList<ParameterSyntax>(parameters.ToList(), SyntaxKind.CommaToken),
            ClosingParen,
            LambdaOperator,
            Body);
    }

    public LambdaExpressionSyntax WithBody(ExpressionSyntax body)
    {
        return new LambdaExpressionSyntax(
            Modifiers,
            OpeningParen,
            Parameters,
            ClosingParen,
            LambdaOperator,
            body);
    }
}
