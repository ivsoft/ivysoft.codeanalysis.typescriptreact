// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Collections;
using System.Text;

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct SeparatedSyntaxList<TNode>
    : IReadOnlyList<TNode>
    , IEnumerable<TNode>
    , IEnumerable
    , IReadOnlyCollection<TNode>
    , ISyntaxNode
    where TNode : ISyntaxNode
{
    private readonly IList<TNode> _data;
    private readonly IList<SyntaxNode> _seperators;
    public SeparatedSyntaxList()
    {
        _data = new List<TNode>();
        _seperators = new List<SyntaxNode>();
    }
    public SeparatedSyntaxList(IList<TNode> data, SyntaxKind separatorKind = SyntaxKind.CommaToken)
    {
        _data = data;
        if(1 < data.Count)
        {
            _seperators = new List<SyntaxNode>(data.Count - 1);
            for(int i = 0; i < data.Count - 1; ++i)
            {
                _seperators.Add(new SyntaxNode(new SyntaxToken(separatorKind)));
            }
        }
        else
        {
            _seperators = new List<SyntaxNode>();
        }
    }
    public SeparatedSyntaxList(TNode data)
    {
        _data = new List<TNode> { data };
        _seperators = new List<SyntaxNode>();
    }
    public SeparatedSyntaxList(IList<TNode> data, IList<SyntaxNode> separators)
    {
        _data = data;
        _seperators = separators;
    }
    public SeparatedSyntaxList<TNode> Concat(IEnumerable<TNode> data, SyntaxKind separatorKind = SyntaxKind.CommaToken)
    {
        return new SeparatedSyntaxList<TNode>(_data.Concat(data).ToList(), separatorKind);
    }

    public TNode this[int index] => _data[index];

    public int Count => _data.Count;

    public IEnumerator<TNode> GetEnumerator() => _data.GetEnumerator();
    IEnumerable<ISyntaxNode> ISyntaxNode.Children
    {
        get
        {
            for (int i = 0; i < _data.Count; ++i)
            {
                yield return _data[i];
                if (i < _seperators.Count)
                {
                    yield return _seperators[i];
                }
            }
        }
    }
    IEnumerator IEnumerable.GetEnumerator() => _data.GetEnumerator();
    public override string ToString()
    {
        var sb = new StringBuilder();
        this.WriteTo(sb);
        return sb.ToString();
    }
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitSeparatedSyntaxList<TNode>(this);
    }
}
