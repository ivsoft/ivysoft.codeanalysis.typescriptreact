// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Text;

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class JsxEndElementSyntax : ISyntaxNode
{
    public SyntaxNode OpeningBracket { get; }
    public IdentifierSyntax Identifier { get; }
    public SyntaxNode ClosingBracket { get; }

    public JsxEndElementSyntax(SyntaxNode openingBracket, IdentifierSyntax identifier, SyntaxNode closingBracket)
    {
        OpeningBracket = openingBracket;
        Identifier = identifier;
        ClosingBracket = closingBracket;
    }
    public JsxEndElementSyntax(IdentifierSyntax identifier)
        : this(new SyntaxNode(new SyntaxToken(SyntaxKind.JsxClosingElementOpeningBracket)), identifier, new SyntaxNode(new SyntaxToken(SyntaxKind.JsxElementClosingBracket)))
    {
    }

    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { OpeningBracket, Identifier, ClosingBracket };
    public override string ToString()
    {
        var sb = new StringBuilder();
        this.WriteTo(sb);
        return sb.ToString();
    }
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitJsxEndElementSyntax(this);
    }
}
