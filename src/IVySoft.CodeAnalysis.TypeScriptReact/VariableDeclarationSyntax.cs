// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct VariableDeclarationSyntax : StatementSyntax, ICompilationUnitMemberSyntax, INamespaceMemberSyntax
{
    public SyntaxNode[] Modifiers { get; }
    public SyntaxNode DeclarationKeyword { get; }
    public SyntaxNode? OpenBracket { get; }
    public SeparatedSyntaxList<ParameterSyntax> Identifiers { get; }
    public SyntaxNode? CloseBracket { get; }
    public EqualsValueClauseSyntax? Initializer { get; }
    //public SyntaxNode SemiColon { get; }
    public VariableDeclarationSyntax(
        SyntaxNode[] modifiers,
        SyntaxNode declarationKeyword,
        SyntaxNode? openBracket,
        SeparatedSyntaxList<ParameterSyntax> identifiers,
        SyntaxNode? closeBracket,
        EqualsValueClauseSyntax? initializer
        //, SyntaxNode semiColon
        )
    {
        Modifiers = modifiers;
        DeclarationKeyword = declarationKeyword;
        OpenBracket = openBracket;
        Identifiers = identifiers;
        CloseBracket = closeBracket;
        Initializer = initializer;
        //SemiColon = semiColon;
    }
    public VariableDeclarationSyntax(ParameterSyntax identifier)
        : this(
              new SyntaxNode[0],
              new SyntaxNode(new SyntaxToken(SyntaxKind.LetKeyword)),
              null,
              new SeparatedSyntaxList<ParameterSyntax>(identifier),
              null,
              null
              //, new SyntaxNode(new SyntaxToken(SyntaxKind.SemicolonToken))
              )
    {
    }
    public VariableDeclarationSyntax(IdentifierSyntax identifier)
        : this(new ParameterSyntax(identifier))
    {
    }
    public VariableDeclarationSyntax(IList<ParameterSyntax> identifiers)
        : this(
              new SyntaxNode[0],
              new SyntaxNode(new SyntaxToken(SyntaxKind.LetKeyword)),
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenBracketToken)),
              new SeparatedSyntaxList<ParameterSyntax>(identifiers),
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseBracketToken)),
              null
              //, new SyntaxNode(new SyntaxToken(SyntaxKind.SemicolonToken))
              )
    {
    }
    public VariableDeclarationSyntax(IList<IdentifierSyntax> identifiers)
        : this(identifiers.Select(x => new ParameterSyntax(x)).ToList())
    {
    }
    public VariableDeclarationSyntax WithModifiers(params SyntaxNode[] modifiers)
    {
        return new VariableDeclarationSyntax(
            modifiers,
            DeclarationKeyword,
            OpenBracket,
            Identifiers,
            CloseBracket,
            Initializer
            //, SemiColon
            );
    }
    public VariableDeclarationSyntax WithConstKeyword()
    {
        return new VariableDeclarationSyntax(
            Modifiers,
            new SyntaxNode(new SyntaxToken(SyntaxKind.ConstKeyword)),
            OpenBracket,
            Identifiers,
            CloseBracket,
            Initializer
            //, SemiColon
            );
    }
    public VariableDeclarationSyntax WithInitializer(EqualsValueClauseSyntax initializer)
    {
        return new VariableDeclarationSyntax(
            Modifiers,
            DeclarationKeyword,
            OpenBracket,
            Identifiers,
            CloseBracket,
            initializer
            //, SemiColon
            );
    }
    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            foreach (var modifier in Modifiers)
            {
                yield return modifier;
            }
            yield return DeclarationKeyword;
            if (null != OpenBracket)
            {
                yield return OpenBracket;
            }
            yield return Identifiers;
            if (null != CloseBracket)
            {
                yield return CloseBracket;
            }
            if (null != Initializer)
            {
                yield return Initializer;
            }
            //yield return SemiColon;
        }
    }
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitVariableDeclarationSyntax(this);
    }
}
