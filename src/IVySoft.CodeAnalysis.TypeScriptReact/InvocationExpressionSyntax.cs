// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct InvocationExpressionSyntax : ExpressionSyntax
{
    public ExpressionSyntax Target { get; }
    public SyntaxNode OpeningBracket { get; }
    public SeparatedSyntaxList<ArgumentSyntax> Arguments { get; }
    public SyntaxNode ClosingBracket { get; }
    public InvocationExpressionSyntax(
        ExpressionSyntax target,
        SyntaxNode openingBracket,
        SeparatedSyntaxList<ArgumentSyntax> arguments,
        SyntaxNode closingBracket)
    {
        Target = target;
        OpeningBracket = openingBracket;
        Arguments = arguments;
        ClosingBracket = closingBracket;
    }
    public InvocationExpressionSyntax(ExpressionSyntax target)
        : this(
              target,
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenParenToken)),
              new SeparatedSyntaxList<ArgumentSyntax>(),
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseParenToken))
              )
    {
    }

    public InvocationExpressionSyntax WithArgumentList(SeparatedSyntaxList<ArgumentSyntax> arguments)
    {
        return new InvocationExpressionSyntax(Target, OpeningBracket, arguments, ClosingBracket);
    }
    public InvocationExpressionSyntax WithArgumentList(params ArgumentSyntax[] arguments)
    {
        return WithArgumentList(new SeparatedSyntaxList<ArgumentSyntax>(arguments));
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { Target, OpeningBracket, Arguments, ClosingBracket };
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitInvocationExpressionSyntax(this);
    }
}
