// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Text;

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public sealed class JsxAttributeSyntax : ISyntaxNode
{
    public IdentifierSyntax Name { get; }
    public JsxEqualValueClauseSyntax Value { get; }

    public JsxAttributeSyntax(IdentifierSyntax name, JsxEqualValueClauseSyntax value)
    {
        Name = name;
        Value = value;
    }

    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { Name, Value };
    public override string ToString()
    {
        var sb = new StringBuilder();
        this.WriteTo(sb);
        return sb.ToString();
    }

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitJsxAttributeSyntax(this);
    }
}
