// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct MemberAccessExpressionSyntax : ExpressionSyntax
{
    public SyntaxNode Operator { get; }
    public ExpressionSyntax Target { get; }
    public IdentifierSyntax Member { get; }
    public MemberAccessExpressionSyntax(SyntaxNode @operator, ExpressionSyntax target, IdentifierSyntax member)
    {
        Operator = @operator;
        Target = target;
        Member = member;
    }
    public MemberAccessExpressionSyntax(SyntaxKind kind, ExpressionSyntax target, IdentifierSyntax member)
        : this(new SyntaxNode(new SyntaxToken(kind)), target, member)
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { Target, Operator, Member };
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitMemberAccessExpressionSyntax(this);
    }
}
