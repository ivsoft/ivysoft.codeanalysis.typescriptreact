// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public sealed class LiteralTypeSyntax : ITypeSyntax
{
    public SyntaxNode Value { get; }

    public LiteralTypeSyntax(SyntaxNode value)
    {
        Value = value;
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { Value };

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitLiteralTypeSyntax(this);
    }
}
