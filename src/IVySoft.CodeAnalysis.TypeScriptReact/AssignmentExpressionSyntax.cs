// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct AssignmentExpressionSyntax : ExpressionSyntax
{
    public ExpressionSyntax Target { get; }
    public SyntaxNode Operator { get; }
    public ExpressionSyntax Source { get; }
    public AssignmentExpressionSyntax(ExpressionSyntax target, SyntaxNode @operator, ExpressionSyntax source)
    {
        Target = target;
        Operator = @operator;
        Source = source;
    }
    public AssignmentExpressionSyntax(SyntaxKind kind, ExpressionSyntax target, ExpressionSyntax source)
        : this(target, new SyntaxNode(new SyntaxToken(kind)), source)
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[]
    {
        Target, Operator, Source
    };

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitAssignmentExpressionSyntax(this);
    }
}
