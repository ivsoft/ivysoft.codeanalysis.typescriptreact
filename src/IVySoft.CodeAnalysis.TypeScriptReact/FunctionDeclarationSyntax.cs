// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly struct FunctionDeclarationSyntax : StatementSyntax
{
    public SyntaxNode FunctionKeyword { get; }
    public IdentifierNameSyntax Identifier { get; }
    public SyntaxNode OpenParen { get; }
    public SeparatedSyntaxList<ParameterSyntax> Parameters { get; }
    public SyntaxNode CloseParen { get; }
    public SyntaxNode OpenBrace { get; }
    public StatementSyntax[] Statements { get; }
    public SyntaxNode CloseBrace { get; }

    public FunctionDeclarationSyntax(SyntaxNode functionKeyword, IdentifierNameSyntax identifier, SyntaxNode openParen, SeparatedSyntaxList<ParameterSyntax> parameters, SyntaxNode closeParen, SyntaxNode openBrace, StatementSyntax[] statements, SyntaxNode closeBrace)
    {
        FunctionKeyword = functionKeyword;
        Identifier = identifier;
        OpenParen = openParen;
        Parameters = parameters;
        CloseParen = closeParen;
        OpenBrace = openBrace;
        Statements = statements;
        CloseBrace = closeBrace;
    }
    public FunctionDeclarationSyntax(IdentifierNameSyntax identifier)
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.FunctionKeyword)),
              identifier,
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenParenToken)),
              new SeparatedSyntaxList<ParameterSyntax>(),
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseParenToken)),
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenBraceToken)),
              new StatementSyntax[0],
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseBraceToken))
              )
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[]
    {
        FunctionKeyword,
        Identifier,
        OpenParen,
        Parameters,
        CloseParen,
        OpenBrace
    }
    .Concat(Statements)
    .Concat(new ISyntaxNode[]
    {
        CloseBrace
    });

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitFunctionDeclarationSyntax(this);
    }

    public FunctionDeclarationSyntax AddStatements(params StatementSyntax[] statements)
    {
        return new FunctionDeclarationSyntax(
            FunctionKeyword,
            Identifier,
            OpenParen,
            Parameters,
            CloseParen,
            OpenBrace,
            Statements.Concat(statements).ToArray(),
            CloseBrace);
    }
}
