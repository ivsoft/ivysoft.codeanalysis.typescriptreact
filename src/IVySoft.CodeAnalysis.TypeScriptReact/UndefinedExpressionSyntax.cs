// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct UndefinedExpressionSyntax : ExpressionSyntax
{
    public SyntaxNode UndefinedKeyword { get; }

    public UndefinedExpressionSyntax(SyntaxNode undefinedKeyword)
    {
        UndefinedKeyword = undefinedKeyword;
    }
    public UndefinedExpressionSyntax()
        : this(new SyntaxNode(new SyntaxToken(SyntaxKind.UndefinedKeyword)))
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { UndefinedKeyword };

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitUndefinedExpressionSyntax(this);
    }
}
