// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class JsxAttributeExpressionValueSyntax : JsxAttributeValueSyntax
{
    public SyntaxNode OpenBrace { get; }
    public ExpressionSyntax Expression { get; }
    public SyntaxNode CloseBrace { get; }

    public JsxAttributeExpressionValueSyntax(SyntaxNode openBrace, ExpressionSyntax expression, SyntaxNode closeBrace)
    {
        OpenBrace = openBrace;
        Expression = expression;
        CloseBrace = closeBrace;
    }
    public JsxAttributeExpressionValueSyntax(ExpressionSyntax expression)
        : this(new SyntaxNode(new SyntaxToken(SyntaxKind.OpenBraceToken)), expression, new SyntaxNode(new SyntaxToken(SyntaxKind.CloseBraceToken)))
    {
    }
    public override IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { OpenBrace, Expression, CloseBrace };
}
