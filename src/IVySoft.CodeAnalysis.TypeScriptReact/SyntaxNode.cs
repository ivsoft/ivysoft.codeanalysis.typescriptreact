// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class SyntaxNode : ISyntaxNode
{
    public SyntaxToken Token { get; }
    public SyntaxToken LeadingTrivia { get; }
    public SyntaxToken TrailingTrivia { get; }

    public SyntaxNode(SyntaxToken token, SyntaxToken leadingTrivia, SyntaxToken trailingTrivia)
    {
        Token = token;
        LeadingTrivia = leadingTrivia;
        TrailingTrivia = trailingTrivia;
    }
    public SyntaxNode(SyntaxToken token)
    {
        Token = token;
        LeadingTrivia = new SyntaxToken(SyntaxKind.Unknown, string.Empty);
        TrailingTrivia = new SyntaxToken(SyntaxKind.Unknown, string.Empty);
    }

    public override string ToString()
    {
        return $"{LeadingTrivia}{Token}{TrailingTrivia}";
    }

    public IEnumerable<ISyntaxNode> Children { get; } = new ISyntaxNode[0];
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitSyntaxNode(this);
    }
    public SyntaxNode? Accept(SyntaxNodeRewriter visitor)
    {
        return (SyntaxNode?)visitor.VisitSyntaxNode(this);
    }
}
