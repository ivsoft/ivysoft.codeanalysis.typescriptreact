// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.
using System.Text;

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class JsxEqualValueClauseSyntax : ISyntaxNode
{
    public SyntaxNode EqualToken { get; }
    public JsxAttributeValueSyntax Value { get; }

    public JsxEqualValueClauseSyntax(SyntaxNode equalToken, JsxAttributeValueSyntax value)
    {
        EqualToken = equalToken;
        Value = value;
    }
    public JsxEqualValueClauseSyntax(JsxAttributeValueSyntax value)
        : this(new SyntaxNode(new SyntaxToken(SyntaxKind.EqualsToken)), value)
    {
    }

    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { EqualToken, Value };
    public override string ToString()
    {
        var sb = new StringBuilder();
        this.WriteTo(sb);
        return sb.ToString();
    }
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitJsxEqualValueClauseSyntax(this);
    }
}
