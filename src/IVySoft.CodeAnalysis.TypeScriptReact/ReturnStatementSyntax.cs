// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly struct ReturnStatementSyntax : StatementSyntax
{
    public SyntaxNode ReturnKeyword { get; }
    public ExpressionSyntax Expression { get; }
    public SyntaxNode Semicolon { get; }

    public ReturnStatementSyntax(SyntaxNode returnKeyword, ExpressionSyntax expression, SyntaxNode semicolon)
    {
        ReturnKeyword = returnKeyword;
        Expression = expression;
        Semicolon = semicolon;
    }
    public ReturnStatementSyntax(ExpressionSyntax expression)
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.ReturnKeyword)),
              expression,
              new SyntaxNode(new SyntaxToken(SyntaxKind.SemicolonToken))
              )
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[]
    { ReturnKeyword, Expression, Semicolon };

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitReturnStatementSyntax(this);
    }
}
