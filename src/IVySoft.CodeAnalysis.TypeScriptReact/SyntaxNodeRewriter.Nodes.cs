// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;
public partial class SyntaxNodeRewriter : SyntaxNodeVisitor<ISyntaxNode>
{
    public override CompilationUnitSyntax? VisitCompilationUnitSyntax(CompilationUnitSyntax node)
    {
        var result = (CompilationUnitSyntax?)VisitDefault(node);
        if (null == result || result != node)
        {
            return result;
        }
        return Rewrite(node)
            .Rewrite(x => x.Imports)?
            .Rewrite(x => x.Members)
            .GetResult((imports, members) => new CompilationUnitSyntax(
                new SeparatedSyntaxList<ImportDirectiveSyntax>(imports.ToList()),
                members.ToArray()));
    }

    public override ISyntaxNode? VisitVariableDeclarationSyntax(VariableDeclarationSyntax node)
    {
        var result = (VariableDeclarationSyntax?)VisitDefault(node);
        if (null == result || result != node)
        {
            return result;
        }
        return Rewrite(node)
            .Rewrite(x => x.Modifiers)
            .Rewrite(x => x.DeclarationKeyword)?
            .RewriteNullable(x => x.OpenBracket)
            .Rewrite(x => (IEnumerable<ParameterSyntax>)x.Identifiers)
            .RewriteNullable(x => x.CloseBracket)
            .RewriteNullable(x => x.Initializer)
            //.Rewrite(x => x.SemiColon)?
            .GetResult((modifiers,
                        declarationKeyword,
                        openBrace,
                        identifiers,
                        closeBrace,
                        initializer
                        //, semiColon
                        ) => new VariableDeclarationSyntax(
                            modifiers.ToArray(),
                            declarationKeyword,
                            openBrace,
                            new SeparatedSyntaxList<ParameterSyntax>(identifiers.ToList()),
                            closeBrace,
                            initializer
                            //, semiColon
                            ));
    }
    public override ImportDirectiveSyntax? VisitImportDirectiveSyntax(ImportDirectiveSyntax node)
    {
        var result = (ImportDirectiveSyntax?)VisitDefault(node);
        if (null == result || result != node)
        {
            return result;
        }
        return Rewrite(node)
            .Rewrite(x => x.ImportKeyword)?
            .Rewrite(x => (IEnumerable<ImportDeclarationSyntax>)x.References)
            .RewriteNullable(x => x.Comma)
            .RewriteNullable(x => x.OpeningBrace)
            .Rewrite(x => (IEnumerable<ImportDeclarationSyntax>)x.NamedReferences)
            .RewriteNullable(x => x.ClosingBrace)
            .RewriteNullable(x => x.FromKeyword)
            .Rewrite(x => x.Source)?
            .Rewrite(x => x.Semicolon)?
            .GetResult((importKeyword,
                        references,
                        comma,
                        openingCurlyBrace,
                        namedReferences,
                        closingCurlyBrace,
                        fromKeyword,
                        source,
                        semicolon) =>
            new ImportDirectiveSyntax(
                importKeyword,
                new SeparatedSyntaxList<ImportDeclarationSyntax>(references.ToList()),
                comma,
                openingCurlyBrace,
                new SeparatedSyntaxList<ImportDeclarationSyntax>(namedReferences.ToList()),
                closingCurlyBrace,
                fromKeyword,
                source,
                semicolon));
    }
    public override ArgumentSyntax? VisitArgumentSyntax(ArgumentSyntax node)
    {
        var result = (ArgumentSyntax?)VisitDefault(node);
        if (null == result || result != node)
        {
            return result;
        }
        return Rewrite(node)
            .Rewrite(x => x.Expression)?
            .GetResult((expression) =>
            new ArgumentSyntax(expression));
    }
    public override ISyntaxNode? VisitAssignmentExpressionSyntax(AssignmentExpressionSyntax node)
    {
        var result = (AssignmentExpressionSyntax?)VisitDefault(node);
        if (null == result || result != node)
        {
            return result;
        }
        return Rewrite(node)
            .Rewrite(x => x.Target)?
            .Rewrite(x => x.Operator)?
            .Rewrite(x => x.Source)?
            .GetResult((target, @operator, source) =>
            new AssignmentExpressionSyntax(target, @operator, source));
    }
    public override ISyntaxNode? VisitExpressionStatementSyntax(ExpressionStatementSyntax node)
    {
        var result = (ExpressionStatementSyntax?)VisitDefault(node);
        if (null == result || result != node)
        {
            return result;
        }
        return Rewrite(node)
            .Rewrite(x => x.Expression)?
            .Rewrite(x => x.Semicolon)?
            .GetResult((expression, semicolon) =>
            new ExpressionStatementSyntax(expression, semicolon));
    }
    public override GlobalStatementSyntax? VisitGlobalStatementSyntax(GlobalStatementSyntax node)
    {
        var result = (GlobalStatementSyntax?)VisitDefault(node);
        if (null == result || result != node)
        {
            return result;
        }
        return Rewrite(node)
            .Rewrite(x => x.Statement)?
            .GetResult((statement) =>
            new GlobalStatementSyntax(statement));
    }
    public override ISyntaxNode? VisitIdentifierNameSyntax(IdentifierNameSyntax node)
    {
        var result = (IdentifierNameSyntax?)VisitDefault(node);
        if (null == result || result != node)
        {
            return result;
        }
        return Rewrite(node)
            .Rewrite(x => x.Identifier)?
            .GetResult((identifier) =>
            new IdentifierNameSyntax(identifier));
    }
    public override IdentifierSyntax? VisitIdentifierSyntax(IdentifierSyntax node)
    {
        var result = (IdentifierSyntax?)VisitDefault(node);
        if (null == result || result != node)
        {
            return result;
        }
        return Rewrite(node)
            .Rewrite(x => x.Identifier)?
            .GetResult((identifier) =>
            new IdentifierSyntax(identifier));
    }
    public override ImportDeclarationSyntax? VisitImportDeclarationSyntax(ImportDeclarationSyntax node)
    {
        var result = (ImportDeclarationSyntax?)VisitDefault(node);
        if (null == result || result != node)
        {
            return result;
        }
        return Rewrite(node)
            .Rewrite(x => x.Name)?
            .RewriteNullable(x => x.AsKeyword)
            .RewriteNullable(x => x.Alias)
            .GetResult((name, asKeyword, alias) =>
            new ImportDeclarationSyntax(name, asKeyword, alias));
    }
    public override ISyntaxNode? VisitInvocationExpressionSyntax(InvocationExpressionSyntax node)
    {
        var result = (InvocationExpressionSyntax?)VisitDefault(node);
        if (null == result || result != node)
        {
            return result;
        }
        return Rewrite(node)
            .Rewrite(x => x.Target)?
            .Rewrite(x => x.OpeningBracket)?
            .Rewrite(x => (IEnumerable<ArgumentSyntax>)x.Arguments)
            .Rewrite(x => x.ClosingBracket)?
            .GetResult((target, openingBracket, arguments, closingBracket) =>
            new InvocationExpressionSyntax(target, openingBracket, new SeparatedSyntaxList<ArgumentSyntax>(arguments.ToList()), closingBracket));
    }
    public override ISyntaxNode? VisitSeparatedSyntaxList<TNode>(SeparatedSyntaxList<TNode> collection)
    {
        var resultCollection = (SeparatedSyntaxList<TNode>?)VisitDefault(collection);
        if (null == resultCollection || resultCollection != collection)
        {
            return resultCollection;
        }
        var result = new List<TNode>();
        bool changed = false;
        foreach (var item in collection)
        {
            var newItem = (TNode?)item.Accept<ISyntaxNode>(this);
            if (null == newItem)
            {
                changed = true;
            }
            else
            {
                if (!ReferenceEquals(newItem, item))
                {
                    changed = true;
                }
                result.Add(newItem);
            }
        }

        if (changed)
        {
            return new SeparatedSyntaxList<TNode>(result);
        }
        else
        {
            return collection;
        }
    }
}

