// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class UnionTypeSyntax : ITypeSyntax
{
    public SeparatedSyntaxList<ITypeSyntax> Types { get; }

    public UnionTypeSyntax(SeparatedSyntaxList<ITypeSyntax> types)
    {
        Types = types;
    }

    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { Types };
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitUnionTypeSyntax(this);
    }
}
