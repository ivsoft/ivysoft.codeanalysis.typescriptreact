// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct LiteralExpressionSyntax : ExpressionSyntax
{
    public SyntaxNode Literal { get; }
    public LiteralExpressionSyntax(SyntaxNode literal)
    {
        Literal = literal;
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { Literal };
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitLiteralExpressionSyntax(this);
    }
}
