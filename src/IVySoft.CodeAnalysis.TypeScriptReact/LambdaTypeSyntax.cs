// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct LambdaTypeSyntax : ITypeSyntax
{
    public SyntaxNode OpeningParen { get; }
    public SeparatedSyntaxList<ParameterSyntax> Parameters { get; }
    public SyntaxNode ClosingParen { get; }
    public SyntaxNode LambdaOperator { get; }
    public ITypeSyntax ResultType { get; }

    public LambdaTypeSyntax(
        SyntaxNode openingParen,
        SeparatedSyntaxList<ParameterSyntax> parameters,
        SyntaxNode closingParen,
        SyntaxNode lambdaOperator,
        ITypeSyntax resultType)
    {
        OpeningParen = openingParen;
        Parameters = parameters;
        ClosingParen = closingParen;
        LambdaOperator = lambdaOperator;
        ResultType = resultType;
    }
    public LambdaTypeSyntax()
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenParenToken)),
              new SeparatedSyntaxList<ParameterSyntax>(),
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseParenToken)),
              new SyntaxNode(new SyntaxToken(SyntaxKind.LabmdaToken)),
              new TypeNameSyntax(new IdentifierSyntax(new SyntaxNode(new SyntaxToken(SyntaxKind.VoidKeyword))))
              )
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[]
    {
        OpeningParen, Parameters, ClosingParen, LambdaOperator, ResultType
    };
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitLambdaTypeSyntax(this);
    }
    public LambdaTypeSyntax WithParameters(params ParameterSyntax[] parameters)
    {
        return new LambdaTypeSyntax(
            OpeningParen,
            new SeparatedSyntaxList<ParameterSyntax>(parameters.ToList(), SyntaxKind.CommaToken),
            ClosingParen,
            LambdaOperator,
            ResultType);
    }

    public LambdaTypeSyntax WithResultType(ITypeSyntax resultType)
    {
        return new LambdaTypeSyntax(
            OpeningParen,
            Parameters,
            ClosingParen,
            LambdaOperator,
            resultType);
    }
}
