// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct ObjectCreationExpressionSyntax : ExpressionSyntax
{
    public SyntaxNode NewKeyword { get; }
    public ITypeSyntax Type { get; }
    public SyntaxNode OpenParen { get; }
    public SeparatedSyntaxList<ArgumentSyntax> Arguments { get; }
    public SyntaxNode CloseParen { get; }

    public ObjectCreationExpressionSyntax(
        SyntaxNode newKeyword,
        ITypeSyntax type,
        SyntaxNode openParen,
        SeparatedSyntaxList<ArgumentSyntax> arguments,
        SyntaxNode closeParen)
    {
        NewKeyword = newKeyword;
        Type = type;
        OpenParen = openParen;
        Arguments = arguments;
        CloseParen = closeParen;
    }
    public ObjectCreationExpressionSyntax(ITypeSyntax type)
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.NewKeyword)),
              type,
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenParenToken)),
              new SeparatedSyntaxList<ArgumentSyntax>(),
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseParenToken))
              )
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[]
    {
        NewKeyword, Type, OpenParen, Arguments, CloseParen
    };
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitObjectCreationExpressionSyntax(this);
    }
    public ObjectCreationExpressionSyntax WithArgumentsList(SeparatedSyntaxList<ArgumentSyntax> arguments)
    {
        return new ObjectCreationExpressionSyntax(
            NewKeyword,
            Type,
            OpenParen,
            arguments,
            CloseParen);
    }
    public ObjectCreationExpressionSyntax WithArgumentList(params ArgumentSyntax[] arguments)
    {
        return new ObjectCreationExpressionSyntax(
            NewKeyword,
            Type,
            OpenParen,
            new SeparatedSyntaxList<ArgumentSyntax>(arguments.ToList()),
            CloseParen);
    }
}
