// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct JsxExpressionSyntax : JsxElementBody
{
    public SyntaxNode OpenBrace { get; }
    public ExpressionSyntax Expression { get; }
    public SyntaxNode CloseBrace { get; }

    public JsxExpressionSyntax(SyntaxNode openBrace, ExpressionSyntax expression, SyntaxNode closeBrace)
    {
        OpenBrace = openBrace;
        Expression = expression;
        CloseBrace = closeBrace;
    }
    public JsxExpressionSyntax(ExpressionSyntax expression)
        : this(new SyntaxNode(new SyntaxToken(SyntaxKind.OpenBraceToken)), expression, new SyntaxNode(new SyntaxToken(SyntaxKind.CloseBraceToken)))
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { OpenBrace, Expression, CloseBrace };

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitJsxExpressionSyntax(this);
    }
}
