// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Text;

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class JsxStartElementSyntax : ISyntaxNode
{
    public SyntaxNode OpeningBracket { get; }
    public IdentifierSyntax Identifier { get; }
    public JsxAttributeSyntax[] Attributes { get; set; }
    public SyntaxNode ClosingBracket { get; }

    public JsxStartElementSyntax(SyntaxNode openingBracket, IdentifierSyntax identifier, JsxAttributeSyntax[] attributes, SyntaxNode closingBracket)
    {
        OpeningBracket = openingBracket;
        Identifier = identifier;
        Attributes = attributes;
        ClosingBracket = closingBracket;
    }
    public JsxStartElementSyntax(IdentifierSyntax identifier)
        : this(new SyntaxNode(new SyntaxToken(SyntaxKind.JsxElementOpeningBracket)), identifier, new JsxAttributeSyntax[0], new SyntaxNode(new SyntaxToken(SyntaxKind.JsxSelfClosingElementClosingBracket)))
    {
    }
    public JsxStartElementSyntax()
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.JsxElementOpeningBracket)),
              new IdentifierSyntax(new SyntaxNode(new SyntaxToken(SyntaxKind.Identifier, string.Empty))),
              new JsxAttributeSyntax[0],
              new SyntaxNode(new SyntaxToken(SyntaxKind.JsxSelfClosingElementClosingBracket))
              )
    {
    }

    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            yield return OpeningBracket;
            yield return Identifier;
            foreach (var attrubute in Attributes)
            {
                yield return attrubute;
            }
            yield return ClosingBracket;
        }
    }

    public JsxStartElementSyntax WithAttributes(JsxAttributeSyntax[] attributes)
    {
        return new JsxStartElementSyntax(
            OpeningBracket,
            Identifier,
            attributes,
            ClosingBracket);
    }
    public override string ToString()
    {
        var sb = new StringBuilder();
        this.WriteTo(sb);
        return sb.ToString();
    }
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitJsxStartElementSyntax(this);
    }
}
