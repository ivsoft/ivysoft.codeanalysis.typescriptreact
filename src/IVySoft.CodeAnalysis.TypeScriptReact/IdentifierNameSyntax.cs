// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct IdentifierNameSyntax : ExpressionSyntax
{
    public IdentifierSyntax Identifier { get; }
    public IdentifierNameSyntax(IdentifierSyntax identifier)
    {
        Identifier = identifier;
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { Identifier };

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitIdentifierNameSyntax(this);
    }
}
