// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public sealed class JsonCopyObjectSyntax : IJsonObjectMember
{
    public SyntaxNode DotDotDot { get; }
    public ExpressionSyntax Expression { get; }
    public JsonCopyObjectSyntax(SyntaxNode dotDotDot, ExpressionSyntax expression)
    {
        DotDotDot = dotDotDot;
        Expression = expression;
    }
    public JsonCopyObjectSyntax(ExpressionSyntax expression)
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.DotDotDotToken)),
              expression
              )
    {
    }

    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { DotDotDot, Expression };
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitJsonCopyObjectSyntax(this);
    }
}
