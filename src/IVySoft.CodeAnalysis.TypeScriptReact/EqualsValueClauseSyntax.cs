// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Text;

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public sealed class EqualsValueClauseSyntax : ISyntaxNode
{
    public SyntaxNode EqualToken { get; }
    public ExpressionSyntax Expression { get; }

    public EqualsValueClauseSyntax(SyntaxNode equals, ExpressionSyntax expression)
    {
        EqualToken = equals;
        Expression = expression;
    }
    public EqualsValueClauseSyntax(ExpressionSyntax expression)
        : this(new SyntaxNode(new SyntaxToken(SyntaxKind.EqualsToken)), expression)
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { EqualToken, Expression };
    public override string ToString()
    {
        var sb = new StringBuilder();
        this.WriteTo(sb);
        return sb.ToString();
    }

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitEqualsValueClauseSyntax(this);
    }
}
