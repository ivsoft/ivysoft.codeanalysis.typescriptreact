// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly struct IfStatementSyntax : StatementSyntax
{
    public SyntaxNode IfKeyword { get; }
    public SyntaxNode OpenParen { get; }
    public ExpressionSyntax Condition { get; }
    public SyntaxNode CloseParen { get; }
    public StatementSyntax TrueStatement { get; }
    public SyntaxNode? ElseKeyword { get; }
    public StatementSyntax? ElseStatement { get; }
    public IfStatementSyntax(
        SyntaxNode ifKeyword,
        SyntaxNode openPaten,
        ExpressionSyntax condition,
        SyntaxNode closeParen,
        StatementSyntax trueStatement,
        SyntaxNode? elseKeyword,
        StatementSyntax? elseStatement)
    {
        IfKeyword = ifKeyword;
        OpenParen = openPaten;
        Condition = condition;
        CloseParen = closeParen;
        TrueStatement = trueStatement;
        ElseKeyword = elseKeyword;
        ElseStatement = elseStatement;
    }
    public IfStatementSyntax(
        ExpressionSyntax condition,
        StatementSyntax trueStatement)
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.IfKeyword)),
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenParenToken)),
              condition,
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseParenToken)),
              trueStatement,
              null,
              null
              )
    {
    }
    public IfStatementSyntax(
        ExpressionSyntax condition,
        StatementSyntax trueStatement,
        StatementSyntax elseStatement)
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.IfKeyword)),
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenParenToken)),
              condition,
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseParenToken)),
              trueStatement,
              new SyntaxNode(new SyntaxToken(SyntaxKind.ElseKeyword)),
              elseStatement
              )
    {
    }

    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            yield return IfKeyword;
            yield return OpenParen;
            yield return Condition;
            yield return CloseParen;
            yield return TrueStatement;
            if(null != ElseKeyword)
            {
                yield return ElseKeyword;
            }
            if(null != ElseStatement)
            {
                yield return ElseStatement;
            }
        }
    }
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitIfStatementSyntax(this);
    }
}
