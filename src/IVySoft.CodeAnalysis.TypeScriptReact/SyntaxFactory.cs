// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;
public static class SyntaxFactory
{
    public static ImportDirectiveSyntax ImportDirective(SyntaxNode source)
    {
        return new ImportDirectiveSyntax(source);
    }
    public static ImportDirectiveSyntax ImportDirective(SyntaxNode source, ImportDeclarationSyntax importReference, bool isNamed)
    {
        return new ImportDirectiveSyntax(
            source,
            new SeparatedSyntaxList<ImportDeclarationSyntax>(
                importReference),
            isNamed);
    }
    public static ImportDirectiveSyntax ImportDirective(SyntaxNode source, IdentifierSyntax identifier, bool isNamed)
    {
        return new ImportDirectiveSyntax(
            source,
            new SeparatedSyntaxList<ImportDeclarationSyntax>(
                new ImportDeclarationSyntax(identifier)
            ),
            isNamed);
    }
    public static ImportDeclarationSyntax ImportReference(IdentifierSyntax reference)
    {
        return new ImportDeclarationSyntax(reference);
    }
    public static ImportDeclarationSyntax ImportReference(IdentifierSyntax reference, IdentifierSyntax alias)
    {
        return new ImportDeclarationSyntax(reference, alias);
    }
    public static SyntaxNode Token(TriviaListSyntax leading, SyntaxKind kind, TriviaListSyntax trailing)
    {
        return new SyntaxNode(
            new SyntaxToken(kind),
            leading.Node,
            trailing.Node);
    }
    public static SyntaxNode Token(SyntaxKind kind)
    {
        return new SyntaxNode(new SyntaxToken(kind));
    }
    public static TriviaListSyntax TriviaList()
    {
        return new TriviaListSyntax();
    }
    public static TriviaListSyntax TriviaList(SyntaxToken token)
    {
        return new TriviaListSyntax(token);
    }

    public static CompilationUnitSyntax CompilationUnit()
    {
        return new CompilationUnitSyntax();
    }

    public static GlobalStatementSyntax GlobalStatement(StatementSyntax statement)
    {
        return new GlobalStatementSyntax(statement);
    }
    public static ExportDeclarationSyntax ExportDeclaration(IdentifierSyntax identifier)
    {
        return new ExportDeclarationSyntax(identifier);
    }

    public static VariableDeclarationSyntax VariableDeclaration(IdentifierSyntax identifier)
    {
        return new VariableDeclarationSyntax(identifier);
    }
    public static ExpressionStatementSyntax ExpressionStatement(ExpressionSyntax expression)
    {
        return new ExpressionStatementSyntax(expression);
    }

    public static AssignmentExpressionSyntax AssignmentExpression(SyntaxKind kind, ExpressionSyntax target, ExpressionSyntax source)
    {
        return new AssignmentExpressionSyntax(kind, target, source);
    }

    public static MemberAccessExpressionSyntax MemberAccessExpression(SyntaxKind kind, ExpressionSyntax target, IdentifierSyntax member)
    {
        return new MemberAccessExpressionSyntax(kind, target, member);
    }

    public static SeparatedSyntaxList<TNode> SingletonList<TNode>(TNode node, SyntaxKind separatorKind = SyntaxKind.CommaToken)
       where TNode : ISyntaxNode
    {
        return new SeparatedSyntaxList<TNode>(new List<TNode>{ node }, separatorKind);
    }
    public static SeparatedSyntaxList<TNode> SeparatedList<TNode>(params TNode[] nodes)
       where TNode : ISyntaxNode
    {
        return new SeparatedSyntaxList<TNode>(nodes.ToList(), SyntaxKind.CommaToken);
    }

    public static SyntaxNode Literal(string text)
    {
        return new SyntaxNode(new SyntaxToken(SyntaxKind.StringLiteral, text));
    }
    public static SyntaxNode Literal(long value)
    {
        return new SyntaxNode(new SyntaxToken(SyntaxKind.NumericLiteral, value.ToString()));
    }
    public static LiteralExpressionSyntax LiteralExpression(SyntaxNode literal)
    {
        return new LiteralExpressionSyntax(literal);
    }
    public static LiteralExpressionSyntax LiteralExpression(SyntaxKind kind)
    {
        return new LiteralExpressionSyntax(new SyntaxNode(new SyntaxToken(kind)));
    }
    public static InvocationExpressionSyntax InvocationExpression(ExpressionSyntax target)
    {
        return new InvocationExpressionSyntax(target);
    }
    public static ArgumentSyntax Argument(ExpressionSyntax expression)
    {
        return new ArgumentSyntax(expression);
    }
    public static JsxElementSyntax JsxElement(IdentifierSyntax identifier)
    {
        return new JsxElementSyntax(identifier);
    }
    public static JsxElementSyntax JsxElement()
    {
        return new JsxElementSyntax();
    }
    public static JsxAttributeSyntax JsxAttribute(IdentifierSyntax identifier, string value)
    {
        return new JsxAttributeSyntax(
            identifier,
            new JsxEqualValueClauseSyntax(
                new JsxAttributeStringValueSyntax(
                    new SyntaxNode(
                        new SyntaxToken(SyntaxKind.StringLiteral, value)))));
    }
    public static JsxAttributeSyntax JsxAttribute(IdentifierSyntax identifier, ExpressionSyntax expression)
    {
        return new JsxAttributeSyntax(
            identifier,
            new JsxEqualValueClauseSyntax(
                new JsxAttributeExpressionValueSyntax(expression)));
    }
    public static JsxExpressionSyntax JsxExpression(ExpressionSyntax expression)
    {
        return new JsxExpressionSyntax(expression);
    }
    public static JsxElementTextSyntax JsxElementText(string text)
    {
        return new JsxElementTextSyntax(text);
    }
    public static JsonObjectExpressionSyntax JsonObjectExpression(SeparatedSyntaxList<IJsonObjectMember> members)
    {
        return new JsonObjectExpressionSyntax(members);
    }
    public static JsonObjectExpressionSyntax JsonObjectExpression(params IJsonObjectMember[] members)
    {
        return JsonObjectExpression(
            new SeparatedSyntaxList<IJsonObjectMember>(
                members.ToList(),
                SyntaxKind.CommaToken));
    }
    public static JsonArrayExpressionSyntax JsonArrayExpression(SeparatedSyntaxList<ExpressionSyntax> members)
    {
        return new JsonArrayExpressionSyntax(members);
    }
    public static JsonArrayExpressionSyntax JsonArrayExpression(IEnumerable<ExpressionSyntax> members)
    {
        return new JsonArrayExpressionSyntax(new SeparatedSyntaxList<ExpressionSyntax>(members.ToList()));
    }
    public static JsonArrayExpressionSyntax JsonArrayExpression(params ExpressionSyntax[] members)
    {
        return new JsonArrayExpressionSyntax(new SeparatedSyntaxList<ExpressionSyntax>(members.ToList()));
    }
    public static JsonObjectMemberDeclaratorSyntax JsonObjectMemberDeclarator(IdentifierSyntax identifier, ExpressionSyntax value)
    {
        return new JsonObjectMemberDeclaratorSyntax(identifier, value);
    }
    public static JsonObjectMemberDeclaratorSyntax JsonObjectMemberDeclarator(IdentifierSyntax identifier)
    {
        return new JsonObjectMemberDeclaratorSyntax(identifier);
    }
    public static EqualsValueClauseSyntax EqualsValueClause(ExpressionSyntax expression)
    {
        return new EqualsValueClauseSyntax(expression);
    }
    public static PostfixUnaryExpressionSyntax PostfixUnaryExpression(SyntaxKind kind, ExpressionSyntax expression)
    {
        return new PostfixUnaryExpressionSyntax(new SyntaxNode(new SyntaxToken(kind)), expression);
    }
    public static IdentifierSyntax Identifier(string identifier)
    {
        return new IdentifierSyntax(new SyntaxNode(new SyntaxToken(SyntaxKind.Identifier, identifier)));
    }
    public static IdentifierSyntax Identifier(TriviaListSyntax leading, string identifier, TriviaListSyntax trailing)
    {
        return new IdentifierSyntax(new SyntaxNode(new SyntaxToken(SyntaxKind.Identifier, identifier), leading.Node, trailing.Node));
    }
    public static IdentifierNameSyntax IdentifierName(IdentifierSyntax identifier)
    {
        return new IdentifierNameSyntax(identifier);
    }
    public static IdentifierNameSyntax IdentifierName(string identifier)
    {
        return IdentifierName(Identifier(identifier));
    }
    public static SyntaxToken SingleLineComment(string text)
    {
        return new SyntaxToken(SyntaxKind.SingleLineCommentTrivia, text);
    }
    public static FunctionDeclarationSyntax FunctionDeclaration(IdentifierNameSyntax identifier)
    {
        return new FunctionDeclarationSyntax(identifier);
    }
    public static ReturnStatementSyntax ReturnStatement(ExpressionSyntax expression)
    {
        return new ReturnStatementSyntax(expression);
    }
    public static ParenthesizedExpressionSyntax ParenthesizedExpression(ExpressionSyntax expression)
    {
        return new ParenthesizedExpressionSyntax(expression);
    }
    public static NamespaceDeclarationSyntax NamespaceDeclaration(IdentifierSyntax identifier)
    {
        return new NamespaceDeclarationSyntax(identifier);
    }
    public static DeclareInterfaceSyntax DeclareInterface(IdentifierSyntax identifier)
    {
        return new DeclareInterfaceSyntax(identifier);
    }
    public static InterfaceDefinitionSyntax InterfaceDefinition(IdentifierSyntax identifier)
    {
        return new InterfaceDefinitionSyntax(identifier);
    }
    public static InterfacePropertySyntax InterfaceProperty(IdentifierSyntax identifier, ITypeSyntax type)
    {
        return new InterfacePropertySyntax(identifier, type);
    }
    public static EnumDefinitionSyntax EnumDefinition(IdentifierSyntax identifier)
    {
        return new EnumDefinitionSyntax(identifier);
    }
    public static EnumMemberSyntax EnumMember(IdentifierSyntax identifier)
    {
        return new EnumMemberSyntax(identifier, null);
    }
    public static EnumMemberSyntax EnumMember(IdentifierSyntax identifier, EqualsValueClauseSyntax? value)
    {
        return new EnumMemberSyntax(identifier, value);
    }
    public static ExtendsSyntax Extends(params IdentifierSyntax[] identifiers)
    {
        return new ExtendsSyntax(identifiers);
    }
    public static TypeAliasDefinitionSyntax TypeAliasDefinition(IdentifierSyntax identifier, ITypeSyntax type)
    {
        return new TypeAliasDefinitionSyntax(identifier, type);
    }
    public static TypeAliasDefinitionSyntax TypeAliasDefinition(IdentifierSyntax identifier, IdentifierSyntax type)
    {
        return new TypeAliasDefinitionSyntax(identifier, new TypeNameSyntax(type));
    }
    public static UnionTypeSyntax UnionType(params ITypeSyntax[] types)
    {
        return new UnionTypeSyntax(new SeparatedSyntaxList<ITypeSyntax>(types.ToList(), SyntaxKind.BarToken));
    }
    public static TypeNameSyntax TypeName(IdentifierSyntax identifier)
    {
        return new TypeNameSyntax(identifier);
    }
    public static TypeNameSyntax TypeName(string identifier)
    {
        return new TypeNameSyntax(Identifier(identifier));
    }
    public static UndefinedTypeSyntax UndefinedType()
    {
        return new UndefinedTypeSyntax();
    }
    public static NullTypeSyntax NullType()
    {
        return new NullTypeSyntax();
    }
    public static LambdaTypeSyntax LambdaType()
    {
        return new LambdaTypeSyntax();
    }
    public static GenericNameSyntax GenericName(string identifier)
    {
        return new GenericNameSyntax(Identifier(identifier));
    }
    public static ArrayTypeSyntax ArrayType(ITypeSyntax elementType)
    {
        return new ArrayTypeSyntax(elementType);
    }
    public static LiteralTypeSyntax LiteralType(string value)
    {
        return new LiteralTypeSyntax(new SyntaxNode(new SyntaxToken(SyntaxKind.StringLiteral, value)));
    }
    public static GenericTypeSyntax GenericType(IdentifierSyntax identifier)
    {
        return new GenericTypeSyntax(identifier);
    }
    public static TypeOfSyntax TypeOf(ExpressionSyntax expression)
    {
        return new TypeOfSyntax(expression);
    }
    public static VariableDeclarationSyntax ConstDefinition(IdentifierSyntax identifier)
    {
        return new VariableDeclarationSyntax(identifier).WithConstKeyword();
    }
    public static VariableDeclarationSyntax ArrayCaptureDefinition(params IdentifierSyntax[] identifiers)
    {
        return new VariableDeclarationSyntax(identifiers);
    }
    public static VariableDeclarationSyntax ConstDefinition(ParameterSyntax identifier)
    {
        return new VariableDeclarationSyntax(identifier).WithConstKeyword();
    }
    public static ObjectCreationExpressionSyntax ObjectCreationExpression(ITypeSyntax type)
    {
        return new ObjectCreationExpressionSyntax(type);
    }
    public static LambdaExpressionSyntax LambdaExpression()
    {
        return new LambdaExpressionSyntax();
    }
    public static ParameterSyntax Parameter(IdentifierSyntax identifier)
    {
        return new ParameterSyntax(identifier);
    }
    public static ExpressionBlockSyntax ExpressionBlock(params StatementSyntax[] statements)
    {
        return new ExpressionBlockSyntax(statements);
    }

    public static IfStatementSyntax IfStatement(ExpressionSyntax expression, StatementSyntax trueCase)
    {
        return new IfStatementSyntax(expression, trueCase);
    }
    public static IfStatementSyntax IfStatement(ExpressionSyntax expression, StatementSyntax trueCase, StatementSyntax? elseCase = null)
    {
        if (null == elseCase)
        {
            return IfStatement(expression, trueCase);
        }
        return new IfStatementSyntax(expression, trueCase, elseCase);
    }
    public static BinaryExpressionSyntax BinaryExpression(SyntaxKind kind, ExpressionSyntax left, ExpressionSyntax rigth)
    {
        return new BinaryExpressionSyntax(left, new SyntaxNode(new SyntaxToken(kind)),  rigth);
    }
    public static BlockSyntax Block(params StatementSyntax[] statements)
    {
        return new BlockSyntax(statements);
    }
    public static AwaitExpressionSyntax AwaitExpression(ExpressionSyntax expression)
    {
        return new AwaitExpressionSyntax(expression);
    }
    public static JsonCopyObjectSyntax JsonCopyObject(ExpressionSyntax expression)
    {
        return new JsonCopyObjectSyntax(expression);
    }
    public static UndefinedExpressionSyntax UndefinedExpression()
    {
        return new UndefinedExpressionSyntax();
    }
    public static NullExpressionSyntax NullExpression()
    {
        return new NullExpressionSyntax();
    }
    public static TypeofExpressionSyntax TypeofExpression(ExpressionSyntax expression)
    {
        return new TypeofExpressionSyntax(expression);
    }
    public static InstanceOfExpressionSyntax InstanceOfExpression(ExpressionSyntax expression, ITypeSyntax type)
    {
        return new InstanceOfExpressionSyntax(expression, type);
    }
    public static ConditionalAccessExpressionSyntax ConditionalAccessExpression(ExpressionSyntax target, IdentifierSyntax member)
    {
        return new ConditionalAccessExpressionSyntax(target, member);
    }
}
