// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public sealed class TypeAliasDefinitionSyntax : INamespaceMemberSyntax, ICompilationUnitMemberSyntax
{
    public SyntaxNode[] Modifiers { get; }
    public SyntaxNode TypeKeyword { get; }
    public IdentifierSyntax Identifier { get; }
    public SyntaxNode Equal { get; }
    public ITypeSyntax Type { get; }
    public SyntaxNode SemiColon { get; }


    public TypeAliasDefinitionSyntax(
        SyntaxNode[] modifiers,
        SyntaxNode typeKeyword,
        IdentifierSyntax identifier,
        SyntaxNode equal,
        ITypeSyntax type,
        SyntaxNode semiColon)
    {
        Modifiers = modifiers;
        TypeKeyword = typeKeyword;
        Identifier = identifier;
        Equal = equal;
        Type = type;
        SemiColon = semiColon;
    }
    public TypeAliasDefinitionSyntax(
        IdentifierSyntax identifier,
        ITypeSyntax type)
        : this(
              new SyntaxNode[0],
              new SyntaxNode(new SyntaxToken(SyntaxKind.TypeKeyword)),
              identifier,
              new SyntaxNode(new SyntaxToken(SyntaxKind.EqualsToken)),
              type,
              new SyntaxNode(new SyntaxToken(SyntaxKind.SemicolonToken))
              )
    {
    }

    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            foreach(var modifier in Modifiers)
            {
                yield return modifier;
            }
            yield return TypeKeyword;
            yield return Identifier;
            yield return Equal;
            yield return Type;
            yield return SemiColon;
        }
    }

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitTypeAliasDefinitionSyntax(this);
    }

    public TypeAliasDefinitionSyntax WithModifiers(params SyntaxNode[] modifiers)
    {
        return new TypeAliasDefinitionSyntax(
            modifiers,
            TypeKeyword,
            Identifier,
            Equal,
            Type,
            SemiColon);
    }
}
