// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly struct TriviaListSyntax
{
    public SyntaxToken Node { get; }

    public TriviaListSyntax()
    {
        Node = new SyntaxToken(SyntaxKind.Unknown, "");
    }
    public TriviaListSyntax(SyntaxToken node)
    {
        Node = node;
    }
}
