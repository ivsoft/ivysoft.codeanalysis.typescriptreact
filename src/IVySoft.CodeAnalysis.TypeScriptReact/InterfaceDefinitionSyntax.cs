// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class InterfaceDefinitionSyntax : INamespaceMemberSyntax, ICompilationUnitMemberSyntax
{
    public SyntaxNode[] Modifiers { get; }
    public SyntaxNode InterfaceKeyword { get; }
    public IdentifierSyntax Identifier { get; }
    public ExtendsSyntax? Extends { get; }
    public SyntaxNode OpenBrace { get; }
    public IInterfaceDefinitionMemberSyntax[] Members { get; }
    public SyntaxNode CloseBrace { get; }

    public InterfaceDefinitionSyntax(
        SyntaxNode[] modifiers,
        SyntaxNode interfaceKeyword,
        IdentifierSyntax identifier,
        ExtendsSyntax? extends,
        SyntaxNode openBrace,
        IInterfaceDefinitionMemberSyntax[] members,
        SyntaxNode closeBrace)
    {
        Modifiers = modifiers;
        InterfaceKeyword = interfaceKeyword;
        Identifier = identifier;
        Extends = extends;
        OpenBrace = openBrace;
        Members = members;
        CloseBrace = closeBrace;
    }
    public InterfaceDefinitionSyntax(
        IdentifierSyntax identifier)
        : this(
              new SyntaxNode[0],
              new SyntaxNode(new SyntaxToken(SyntaxKind.InterfaceKeyword)),
              identifier,
              null,
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenBraceToken)),
              new IInterfaceDefinitionMemberSyntax[0],
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseBraceToken))
              )
    {
    }
    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            foreach (var modifier in Modifiers)
            {
                yield return modifier;
            }
            yield return InterfaceKeyword;
            yield return Identifier;
            if (null != Extends)
            {
                yield return Extends;
            }
            yield return OpenBrace;
            foreach (var member in Members)
            {
                yield return member;
            }
            yield return CloseBrace;
        }
    }

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitInterfaceDefinitionSyntax(this);
    }

    public InterfaceDefinitionSyntax WithMembers(params IInterfaceDefinitionMemberSyntax[] members)
    {
        return new InterfaceDefinitionSyntax(
            Modifiers,
            InterfaceKeyword,
            Identifier,
            Extends,
            OpenBrace,
            members,
            CloseBrace);
    }

    public InterfaceDefinitionSyntax WithExtends(ExtendsSyntax extends)
    {
        return new InterfaceDefinitionSyntax(
            Modifiers,
            InterfaceKeyword,
            Identifier,
            extends,
            OpenBrace,
            Members,
            CloseBrace);
    }
    public InterfaceDefinitionSyntax WithModifiers(params SyntaxNode[] modifiers)
    {
        return new InterfaceDefinitionSyntax(
            modifiers,
            InterfaceKeyword,
            Identifier,
            Extends,
            OpenBrace,
            Members,
            CloseBrace);
    }
}
