// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public sealed class TypeOfSyntax : ITypeSyntax
{
    public SyntaxNode TypeOfKeyword { get; }
    public ExpressionSyntax Expression { get; }

    public TypeOfSyntax(SyntaxNode typeOfKeyword, ExpressionSyntax expression)
    {
        TypeOfKeyword = typeOfKeyword;
        Expression = expression;
    }
    public TypeOfSyntax(ExpressionSyntax expression)
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.TypeOfKeyword)),
              expression
              )
    {
    }

    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { TypeOfKeyword, Expression };

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitTypeOfSyntax(this);
    }
}
