// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Text;

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class ImportDirectiveSyntax : ISyntaxNode
{
    public SyntaxNode ImportKeyword { get; }
    public SeparatedSyntaxList<ImportDeclarationSyntax> References { get; }
    public SyntaxNode? Comma { get; }
    public SyntaxNode? OpeningBrace { get; }
    public SeparatedSyntaxList<ImportDeclarationSyntax> NamedReferences { get; }
    public SyntaxNode? ClosingBrace { get; }
    public SyntaxNode? FromKeyword { get; }
    public SyntaxNode Source { get; }
    public SyntaxNode Semicolon { get; }

    public ImportDirectiveSyntax(
        SyntaxNode importKeyword,
        SeparatedSyntaxList<ImportDeclarationSyntax> references,
        SyntaxNode? comma,
        SyntaxNode? openingCurlyBrace,
        SeparatedSyntaxList<ImportDeclarationSyntax> namedReferences,
        SyntaxNode? closingCurlyBrace,
        SyntaxNode? fromKeyword,
        SyntaxNode source,
        SyntaxNode semicolon
        )
    {
        ImportKeyword = importKeyword;
        References = references;
        Comma = comma;
        OpeningBrace = openingCurlyBrace;
        NamedReferences = namedReferences;
        ClosingBrace = closingCurlyBrace;
        FromKeyword = fromKeyword;
        Source = source;
        Semicolon = semicolon;
    }
    public ImportDirectiveSyntax(
        SyntaxNode source
        )
        : this(
            new SyntaxNode(new SyntaxToken(SyntaxKind.ImportKeyword)),
            new SeparatedSyntaxList<ImportDeclarationSyntax>(),
            null,
            null,
            new SeparatedSyntaxList<ImportDeclarationSyntax>(),
            null,
            null,
            source,
            new SyntaxNode(new SyntaxToken(SyntaxKind.SemicolonToken)))
    {
    }
    public ImportDirectiveSyntax(
        SyntaxNode source,
        SeparatedSyntaxList<ImportDeclarationSyntax> references,
        bool isNamed
        )
        : this(
            new SyntaxNode(new SyntaxToken(SyntaxKind.ImportKeyword)),
            isNamed ? new SeparatedSyntaxList<ImportDeclarationSyntax>() : references,
            null,
            null,
            isNamed ? references : new SeparatedSyntaxList<ImportDeclarationSyntax>(),
            null,
            ((0 != references.Count)
            ? new SyntaxNode(new SyntaxToken(SyntaxKind.FromKeyword))
            : null),
            source,
            new SyntaxNode(new SyntaxToken(SyntaxKind.SemicolonToken)))
    {
    }
    public ImportDirectiveSyntax WithImportKeyword(SyntaxNode token)
    {
        return new ImportDirectiveSyntax(
            token,
            References,
            Comma,
            OpeningBrace,
            NamedReferences,
            ClosingBrace,
            FromKeyword,
            Source,
            Semicolon);
    }
    public ImportDirectiveSyntax WithReferences(params ImportDeclarationSyntax[] references)
    {
        return WithReferences(new SeparatedSyntaxList<ImportDeclarationSyntax>(references));
    }
    public ImportDirectiveSyntax WithReferences(SeparatedSyntaxList<ImportDeclarationSyntax> references)
    {
        return new ImportDirectiveSyntax(
            ImportKeyword,
            references,
            Comma
            ?? ((0 != references.Count && 0 != NamedReferences.Count)
            ? new SyntaxNode(new SyntaxToken(SyntaxKind.CommaToken))
            : null),
            OpeningBrace,
            NamedReferences,
            ClosingBrace,
            FromKeyword ?? ((0 != references.Count || 0 != NamedReferences.Count)
            ? new SyntaxNode(new SyntaxToken(SyntaxKind.FromKeyword))
            : null),
            Source,
            Semicolon);
    }
    public ImportDirectiveSyntax WithNamedReferences(params ImportDeclarationSyntax[] references)
    {
        return WithNamedReferences(new SeparatedSyntaxList<ImportDeclarationSyntax>(references));
    }
    public ImportDirectiveSyntax WithNamedReferences(SeparatedSyntaxList<ImportDeclarationSyntax> references)
    {
        return new ImportDirectiveSyntax(
            ImportKeyword,
            References,
            Comma
            ?? ((0 != References.Count && 0 != references.Count)
            ? new SyntaxNode(new SyntaxToken(SyntaxKind.CommaToken))
            : null),
            OpeningBrace ?? ((0 != references.Count)
            ? new SyntaxNode(new SyntaxToken(SyntaxKind.OpenBraceToken))
            : null),
            references,
            ClosingBrace ?? ((0 != references.Count)
            ? new SyntaxNode(new SyntaxToken(SyntaxKind.CloseBraceToken))
            : null),
            FromKeyword ?? ((0 != References.Count || 0 != references.Count)
            ? new SyntaxNode(new SyntaxToken(SyntaxKind.FromKeyword))
            : null),
            Source,
            Semicolon);
    }
    public ImportDirectiveSyntax AddReferences(params ImportDeclarationSyntax[] references)
    {
        return AddReferences(new SeparatedSyntaxList<ImportDeclarationSyntax>(references));
    }
    public ImportDirectiveSyntax AddReferences(SeparatedSyntaxList<ImportDeclarationSyntax> references)
    {
        return new ImportDirectiveSyntax(
            ImportKeyword,
            References.Concat(references),
            Comma
            ?? ((0 != NamedReferences.Count)
            ? new SyntaxNode(new SyntaxToken(SyntaxKind.CommaToken))
            : null),
            OpeningBrace,
            NamedReferences,
            ClosingBrace,
            FromKeyword ?? ((0 != references.Count || 0 != References.Count || 0 != NamedReferences.Count)
            ? new SyntaxNode(new SyntaxToken(SyntaxKind.FromKeyword))
            : null),
            Source,
            Semicolon);
    }
    public ImportDirectiveSyntax AddNamedReferences(params ImportDeclarationSyntax[] references)
    {
        return AddNamedReferences(new SeparatedSyntaxList<ImportDeclarationSyntax>(references));
    }
    public ImportDirectiveSyntax AddNamedReferences(SeparatedSyntaxList<ImportDeclarationSyntax> references)
    {
        return new ImportDirectiveSyntax(
            ImportKeyword,
            References,
            Comma
            ?? ((0 != References.Count)
            ? new SyntaxNode(new SyntaxToken(SyntaxKind.CommaToken))
            : null),
            OpeningBrace ?? ((0 != NamedReferences.Count || 0 != references.Count)
            ? new SyntaxNode(new SyntaxToken(SyntaxKind.OpenBraceToken))
            : null),
            NamedReferences.Concat(references),
            ClosingBrace ?? ((0 != NamedReferences.Count || 0 != references.Count)
            ? new SyntaxNode(new SyntaxToken(SyntaxKind.CloseBraceToken))
            : null),
            FromKeyword ?? ((0 != References.Count || 0 != references.Count || 0 != NamedReferences.Count)
            ? new SyntaxNode(new SyntaxToken(SyntaxKind.FromKeyword))
            : null),
            Source,
            Semicolon);
    }
    public ImportDirectiveSyntax WithSemicolonToken(SyntaxNode semicolon)
    {
        return new ImportDirectiveSyntax(
            ImportKeyword,
            References,
            Comma,
            OpeningBrace,
            NamedReferences,
            ClosingBrace,
            FromKeyword,
            Source,
            semicolon);
    }

    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            yield return ImportKeyword;
            yield return References;
            if (null != Comma)
            {
                yield return Comma;
            }
            if (null != OpeningBrace)
            {
                yield return OpeningBrace;
            }
            yield return NamedReferences;
            if (null != ClosingBrace)
            {
                yield return ClosingBrace;
            }
            if (null != FromKeyword)
            {
                yield return FromKeyword;
            }
            yield return Source;
            yield return Semicolon;
        }
    }
    public override string ToString()
    {
        var sb = new StringBuilder();
        this.WriteTo(sb);
        return sb.ToString();
    }
    public virtual T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitImportDirectiveSyntax(this);
    }
}
