// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Text;

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public abstract class JsxAttributeValueSyntax : ISyntaxNode
{
    public abstract IEnumerable<ISyntaxNode> Children { get; }

    public virtual T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitJsxAttributeValueSyntax(this);
    }

    public override string ToString()
    {
        var sb = new StringBuilder();
        this.WriteTo(sb);
        return sb.ToString();
    }
}
