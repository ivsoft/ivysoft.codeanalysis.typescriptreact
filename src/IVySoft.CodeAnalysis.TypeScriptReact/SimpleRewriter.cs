// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class SimpleRewriter : SyntaxNodeRewriter
{
    private readonly ISyntaxNode _source;
    private readonly ISyntaxNode? _target;

    public SimpleRewriter(ISyntaxNode source, ISyntaxNode? target)
    {
        _source = source;
        _target = target;
    }

    public override ISyntaxNode? VisitDefault(ISyntaxNode? node)
    {
        if(ReferenceEquals(node, _source))
        {
            return _target;
        }
        return base.VisitDefault(node);
    }
}
