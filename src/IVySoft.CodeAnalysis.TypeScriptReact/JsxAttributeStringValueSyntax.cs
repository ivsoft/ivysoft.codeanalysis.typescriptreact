// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class JsxAttributeStringValueSyntax : JsxAttributeValueSyntax
{
    public SyntaxNode Value { get; }

    public JsxAttributeStringValueSyntax(SyntaxNode value)
    {
        Value = value;
    }
    public override IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { Value };
}
