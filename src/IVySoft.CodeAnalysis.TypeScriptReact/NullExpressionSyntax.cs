﻿// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct NullExpressionSyntax : ExpressionSyntax
{
    public SyntaxNode NullKeyword { get; }

    public NullExpressionSyntax(SyntaxNode nullKeyword)
    {
        NullKeyword = nullKeyword;
    }
    public NullExpressionSyntax()
        : this(new SyntaxNode(new SyntaxToken(SyntaxKind.NullKeyword)))
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { NullKeyword };

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitNullExpressionSyntax(this);
    }
}
