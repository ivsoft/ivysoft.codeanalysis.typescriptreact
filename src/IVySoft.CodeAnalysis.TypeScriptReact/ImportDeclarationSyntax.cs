// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Text;

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class ImportDeclarationSyntax : ISyntaxNode
{
    public IdentifierSyntax Name { get; }
    public SyntaxNode? AsKeyword { get; }
    public IdentifierSyntax? Alias { get; }

    public ImportDeclarationSyntax(IdentifierSyntax name)
    {
        Name = name;
    }
    public ImportDeclarationSyntax(IdentifierSyntax name, IdentifierSyntax alias)
    {
        Name = name;
        AsKeyword = new SyntaxNode(new SyntaxToken(SyntaxKind.AsKeyword));
        Alias = alias;
    }
    public ImportDeclarationSyntax(IdentifierSyntax name, SyntaxNode? asKeyword, IdentifierSyntax? alias)
    {
        Name = name;
        AsKeyword = asKeyword;
        Alias = alias;
    }

    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            yield return Name;
            if (null != AsKeyword)
            {
                yield return AsKeyword;
            }
            if (null != Alias)
            {
                yield return Alias;
            }
        }
    }
    public override string ToString()
    {
        var sb = new StringBuilder();
        this.WriteTo(sb);
        return sb.ToString();
    }
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitImportDeclarationSyntax(this);
    }
}
