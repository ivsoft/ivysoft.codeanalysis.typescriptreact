// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly struct BlockSyntax : StatementSyntax
{
    public SyntaxNode OpenBrace { get; }
    public StatementSyntax[] Statements { get; }
    public SyntaxNode CloseBrace { get; }

    public BlockSyntax(
        SyntaxNode openBrace,
        StatementSyntax[] statements,
        SyntaxNode closeBrace)
    {
        OpenBrace = openBrace;
        Statements = statements;
        CloseBrace = closeBrace;
    }
    public BlockSyntax(
        StatementSyntax[] statements)
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenBraceToken)),
              statements,
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseBraceToken))
              )
    {
    }
    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            yield return OpenBrace;
            foreach(var statement in Statements)
            {
                yield return statement;
            }
            yield return CloseBrace;
        }
    }
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitBlockSyntax(this);
    }
}
