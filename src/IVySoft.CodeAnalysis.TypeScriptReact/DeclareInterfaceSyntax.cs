// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct DeclareInterfaceSyntax : INamespaceMemberSyntax
{
    public SyntaxNode[] Modifiers { get; }
    public SyntaxNode DeclareKeyword { get; }
    public SyntaxNode InterfaceKeyword { get; }
    public IdentifierSyntax Identifier { get; }
    public SyntaxNode OpenBrace { get; }
    public SyntaxNode CloseBrace { get; }

    public DeclareInterfaceSyntax(
        SyntaxNode[] modifiers,
        SyntaxNode declareKeyword,
        SyntaxNode interfaceKeyword,
        IdentifierSyntax identifier,
        SyntaxNode openBrace,
        SyntaxNode closeBrace)
    {
        Modifiers = modifiers;
        DeclareKeyword = declareKeyword;
        InterfaceKeyword = interfaceKeyword;
        Identifier = identifier;
        OpenBrace = openBrace;
        CloseBrace = closeBrace;
    }
    public DeclareInterfaceSyntax(
        IdentifierSyntax identifier)
        : this(
              new SyntaxNode[0],
              new SyntaxNode(new SyntaxToken(SyntaxKind.DeclareKeyword)),
              new SyntaxNode(new SyntaxToken(SyntaxKind.InterfaceKeyword)),
              identifier,
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenBraceToken)),
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseBraceToken))
              )
    {
    }
    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            foreach (var modifier in Modifiers)
            {
                yield return modifier;
            }
            yield return DeclareKeyword;
            yield return InterfaceKeyword;
            yield return Identifier;
            yield return OpenBrace;
            yield return CloseBrace;
        }
    }

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitDeclareInterfaceSyntax(this);
    }

    public DeclareInterfaceSyntax WithModifiers(params SyntaxNode[] modifiers)
    {
        return new DeclareInterfaceSyntax(
            modifiers,
            DeclareKeyword,
            InterfaceKeyword,
            Identifier,
            OpenBrace,
            CloseBrace);
    }
}
