// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct UndefinedTypeSyntax : ITypeSyntax
{
    public readonly SyntaxNode UndefinedKeyword { get; }

    public UndefinedTypeSyntax()
    {
        UndefinedKeyword = new SyntaxNode(new SyntaxToken(SyntaxKind.UndefinedKeyword));
    }

    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { UndefinedKeyword };

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitUndefinedTypeSyntax(this);
    }
}
