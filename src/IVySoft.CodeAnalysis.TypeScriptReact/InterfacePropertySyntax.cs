// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class InterfacePropertySyntax : IInterfaceDefinitionMemberSyntax
{
    public IdentifierSyntax Identifier { get; }
    public SyntaxNode Colon { get; }
    public ITypeSyntax Type { get; }
    public SyntaxNode SemiColon { get; }

    public InterfacePropertySyntax(IdentifierSyntax identifier, SyntaxNode colon, ITypeSyntax type, SyntaxNode semiColon)
    {
        Identifier = identifier;
        Colon = colon;
        Type = type;
        SemiColon = semiColon;
    }
    public InterfacePropertySyntax(IdentifierSyntax identifier, ITypeSyntax type)
        : this(
              identifier,
              new SyntaxNode(new SyntaxToken(SyntaxKind.ColonToken)),
              type,
              new SyntaxNode(new SyntaxToken(SyntaxKind.SemicolonToken))
              )
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[]
    {
        Identifier,
        Colon,
        Type,
        SemiColon
    };

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitInterfacePropertySyntax(this);
    }
}
