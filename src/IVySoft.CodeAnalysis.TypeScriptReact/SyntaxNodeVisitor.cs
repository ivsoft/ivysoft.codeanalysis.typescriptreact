// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;


public class SyntaxNodeVisitor<T> where T : class
{
    public virtual T? VisitDefault(ISyntaxNode? node)
    {
        return node as T;
    }
    public virtual T? VisitCompilationUnitSyntax(CompilationUnitSyntax node) => VisitDefault(node);
    public virtual T? VisitImportDirectiveSyntax(ImportDirectiveSyntax node) => VisitDefault(node);
    public virtual T? VisitSyntaxNode(SyntaxNode node) => VisitDefault(node);
    public virtual T? VisitImportDeclarationSyntax(ImportDeclarationSyntax node) => VisitDefault(node);
    public virtual T? VisitIdentifierSyntax(IdentifierSyntax node) => VisitDefault(node);
    public virtual T? VisitGlobalStatementSyntax(GlobalStatementSyntax node) => VisitDefault(node);
    public virtual T? VisitArgumentSyntax(ArgumentSyntax node) => VisitDefault(node);
    public virtual T? VisitExpressionSyntax(ExpressionSyntax node) => VisitDefault(node);
    public virtual T? VisitIdentifierNameSyntax(IdentifierNameSyntax node) => VisitExpressionSyntax(node);
    public virtual T? VisitInvocationExpressionSyntax(InvocationExpressionSyntax node) => VisitExpressionSyntax(node);
    public virtual T? VisitAssignmentExpressionSyntax(AssignmentExpressionSyntax node) => VisitExpressionSyntax(node);
    public virtual T? VisitJsonObjectExpressionSyntax(JsonObjectExpressionSyntax node) => VisitExpressionSyntax(node);
    public virtual T? VisitJsonArrayExpressionSyntax(JsonArrayExpressionSyntax node) => VisitExpressionSyntax(node);
    public virtual T? VisitLiteralExpressionSyntax(LiteralExpressionSyntax node) => VisitExpressionSyntax(node);
    public virtual T? VisitMemberAccessExpressionSyntax(MemberAccessExpressionSyntax node) => VisitExpressionSyntax(node);
    public virtual T? VisitPostfixUnaryExpressionSyntax(PostfixUnaryExpressionSyntax node) => VisitExpressionSyntax(node);

    public virtual T? VisitJsonObjectMemberDeclaratorSyntax(JsonObjectMemberDeclaratorSyntax node) => VisitDefault(node);
    public virtual T? VisitJsxElementBody(JsxElementBody node) => VisitDefault(node);
    public virtual T? VisitJsxElementSyntax(JsxElementSyntax node) => VisitJsxElementBody(node);

    public virtual T? VisitStatementSyntax(StatementSyntax node) => VisitDefault(node);
    public virtual T? VisitExpressionStatementSyntax(ExpressionStatementSyntax node) => VisitStatementSyntax(node);
    public virtual T? VisitVariableDeclarationSyntax(VariableDeclarationSyntax node) => VisitDefault(node);

    public virtual T? VisitSeparatedSyntaxList<TNode>(SeparatedSyntaxList<TNode> node) where TNode : ISyntaxNode => VisitDefault(node);
    public virtual T? VisitEqualsValueClauseSyntax(EqualsValueClauseSyntax node) => VisitDefault(node);
    public virtual T? VisitJsxAttributeSyntax(JsxAttributeSyntax node) => VisitDefault(node);
    public virtual T? VisitJsxAttributeValueSyntax(JsxAttributeValueSyntax node) => VisitDefault(node);
    public virtual T? VisitJsxEndElementSyntax(JsxEndElementSyntax node) => VisitDefault(node);
    public virtual T? VisitJsxEqualValueClauseSyntax(JsxEqualValueClauseSyntax node) => VisitDefault(node);
    public virtual T? VisitJsxStartElementSyntax(JsxStartElementSyntax node) => VisitDefault(node);
    public virtual T? VisitFunctionDeclarationSyntax(FunctionDeclarationSyntax node) => VisitStatementSyntax(node);
    public virtual T? VisitFunctionParameterSyntax(ParameterSyntax node) => VisitDefault(node);
    public virtual T? VisitReturnStatementSyntax(ReturnStatementSyntax node) => VisitStatementSyntax(node);
    public virtual T? VisitExportDeclarationSyntax(ExportDeclarationSyntax node) => VisitDefault(node);
    public virtual T? VisitParenthesizedExpressionSyntax(ParenthesizedExpressionSyntax node) => VisitExpressionSyntax(node);
    public virtual T? VisitNamespaceDeclarationSyntax(NamespaceDeclarationSyntax node) => VisitDefault(node);
    public virtual T? VisitNamespaceMemberSyntax(INamespaceMemberSyntax node) => VisitDefault(node);
    public virtual T? VisitDeclareInterfaceSyntax(DeclareInterfaceSyntax node) => VisitNamespaceMemberSyntax(node);
    public virtual T? VisitInterfaceDefinitionSyntax(InterfaceDefinitionSyntax node) => VisitNamespaceMemberSyntax(node);

    public virtual T? VisitInterfaceDefinitionMemberSyntax(IInterfaceDefinitionMemberSyntax node) => VisitDefault(node);    
    public virtual T? VisitInterfacePropertySyntax(InterfacePropertySyntax node) => VisitInterfaceDefinitionMemberSyntax(node);
    public virtual T? VisitTypeSyntax(TypeNameSyntax node) => VisitDefault(node);
    public virtual T? VisitExtendsSyntax(ExtendsSyntax node) => VisitDefault(node);
    public virtual T? VisitTypeAliasDefinitionSyntax(TypeAliasDefinitionSyntax node) => VisitDefault(node);
    public virtual T? VisitUnionTypeSyntax(UnionTypeSyntax node) => VisitDefault(node);
    public virtual T? VisitLiteralTypeSyntax(LiteralTypeSyntax node) => VisitDefault(node);
    public virtual T? VisitGenericTypeSyntax(GenericTypeSyntax node) => VisitDefault(node);
    public virtual T? VisitObjectCreationExpressionSyntax(ObjectCreationExpressionSyntax node) => VisitDefault(node);
    public virtual T? VisitLambdaExpressionSyntax(LambdaExpressionSyntax node) => VisitDefault(node);
    public virtual T? VisitExpressionBlockSyntax(ExpressionBlockSyntax node) => VisitDefault(node);
    public virtual T? VisitTypeOfSyntax(TypeOfSyntax node) => VisitDefault(node);
    public virtual T? VisitJsxExpressionSyntax(JsxExpressionSyntax node) => VisitDefault(node);
    public virtual T? VisitIfStatementSyntax(IfStatementSyntax node) => VisitStatementSyntax(node);
    public virtual T? VisitBinaryExpressionSyntax(BinaryExpressionSyntax node) => VisitExpressionSyntax(node);
    public virtual T? VisitBlockSyntax(BlockSyntax node) => VisitStatementSyntax(node);
    public virtual T? VisitAwaitExpressionSyntax(AwaitExpressionSyntax node) => VisitExpressionSyntax(node);
    public virtual T? VisitJsonCopyObjectSyntax(JsonCopyObjectSyntax node) => VisitDefault(node);
    public virtual T? VisitArrayTypeSyntax(ArrayTypeSyntax node) => VisitDefault(node);
    public virtual T? VisitUndefinedExpressionSyntax(UndefinedExpressionSyntax node) => VisitExpressionSyntax(node);
    public virtual T? VisitTypeofExpressionSyntax(TypeofExpressionSyntax node) => VisitDefault(node);
    public virtual T? VisitInstanceOfExpressionSyntax(InstanceOfExpressionSyntax node) => VisitExpressionSyntax(node);
    public virtual T? VisitGenericNameSyntax(GenericNameSyntax node) => VisitDefault(node);
    public virtual T? VisitConditionalAccessExpressionSyntax(ConditionalAccessExpressionSyntax node) => VisitExpressionSyntax(node);
    public virtual T? VisitLambdaTypeSyntax(LambdaTypeSyntax node) => VisitDefault(node);
    public virtual T? VisitNullExpressionSyntax(NullExpressionSyntax node) => VisitExpressionSyntax(node);
    public virtual T? VisitJsxElementText(JsxElementTextSyntax node) => VisitDefault(node);
    public virtual T? VisitUndefinedTypeSyntax(UndefinedTypeSyntax node) => VisitDefault(node);
    public virtual T? VisitNullTypeSyntax(NullTypeSyntax node) => VisitDefault(node);
    public virtual T? VisitEnumDefinitionSyntax(EnumDefinitionSyntax node) => VisitDefault(node);
    public virtual T? VisitEnumMemberSyntax(EnumMemberSyntax node) => VisitDefault(node);
}
