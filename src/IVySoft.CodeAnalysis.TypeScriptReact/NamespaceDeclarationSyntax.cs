// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct NamespaceDeclarationSyntax : ICompilationUnitMemberSyntax
{
    public SyntaxNode[] Modifiers { get; }
    public SyntaxNode NamespaceKeyword { get; }
    public IdentifierSyntax Identifier { get; }
    public SyntaxNode OpenBrace { get; }
    public INamespaceMemberSyntax[] Members { get; }
    public SyntaxNode CloseBrace { get; }

    public NamespaceDeclarationSyntax(
        SyntaxNode[] modifiers,
        SyntaxNode namespaceKeyword,
        IdentifierSyntax identifier,
        SyntaxNode openBrace,
        INamespaceMemberSyntax[] members,
        SyntaxNode closeBrace)
    {
        Modifiers = modifiers;
        NamespaceKeyword = namespaceKeyword;
        Identifier = identifier;
        OpenBrace = openBrace;
        Members = members;
        CloseBrace = closeBrace;
    }
    public NamespaceDeclarationSyntax(IdentifierSyntax identifier)
        : this(
              new SyntaxNode[0],
              new SyntaxNode(new SyntaxToken(SyntaxKind.NamespaceKeyword)),
              identifier,
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenBraceToken)),
              new INamespaceMemberSyntax[0],
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseBraceToken))
              )
    {
    }
    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            foreach (var modifier in Modifiers)
            {
                yield return modifier;
            }
            yield return NamespaceKeyword;
            yield return Identifier;
            yield return OpenBrace;
            foreach (var member in Members)
            {
                yield return member;
            }
            yield return CloseBrace;
        }
    }

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitNamespaceDeclarationSyntax(this);
    }
    public NamespaceDeclarationSyntax WithMembers(params INamespaceMemberSyntax[] members)
    {
        return new NamespaceDeclarationSyntax(
            Modifiers,
            NamespaceKeyword,
            Identifier,
            OpenBrace,
            members,
            CloseBrace);
    }
    public NamespaceDeclarationSyntax WithModifiers(params SyntaxNode[] modifiers)
    {
        return new NamespaceDeclarationSyntax(
            modifiers,
            NamespaceKeyword,
            Identifier,
            OpenBrace,
            Members,
            CloseBrace);
    }
}
