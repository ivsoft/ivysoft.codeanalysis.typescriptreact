// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Text;

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class IdentifierSyntax : ISyntaxNode
{
    public SyntaxNode Identifier { get; }
    public string ValueText => Identifier.Token.ValueText;
    public IdentifierSyntax(SyntaxNode identifier)
    {
        Identifier = identifier;
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { Identifier };
    public override string ToString()
    {
        var sb = new StringBuilder();
        this.WriteTo(sb);
        return sb.ToString();
    }
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitIdentifierSyntax(this);
    }
}
