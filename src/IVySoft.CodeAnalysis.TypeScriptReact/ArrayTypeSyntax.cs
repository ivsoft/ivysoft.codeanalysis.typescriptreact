// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public sealed class ArrayTypeSyntax : ITypeSyntax
{
    public ITypeSyntax ElementType { get; }
    public SyntaxNode OpenBracket { get; }
    public SyntaxNode CloseBracket { get; }
    public ArrayTypeSyntax(ITypeSyntax elementType, SyntaxNode openBracket, SyntaxNode closeBracket)
    {
        ElementType = elementType;
        OpenBracket = openBracket;
        CloseBracket = closeBracket;
    }
    public ArrayTypeSyntax(ITypeSyntax elementType)
        : this(
              elementType,
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenBracketToken)),
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseBracketToken))
              )
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { ElementType, OpenBracket, CloseBracket };

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitArrayTypeSyntax(this);
    }
}
