// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public sealed class GlobalStatementSyntax : ICompilationUnitMemberSyntax
{
    public StatementSyntax Statement { get; }

    public GlobalStatementSyntax(StatementSyntax statement)
    {
        Statement = statement;
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { Statement };

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitGlobalStatementSyntax(this);
    }
}
