// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct ExportDeclarationSyntax : ICompilationUnitMemberSyntax
{
    public SyntaxNode ExportKeyword { get; }
    public SyntaxNode[] Modifiers { get; } = new SyntaxNode[0];
    public IdentifierSyntax Identifier { get; }
    public SyntaxNode Semicolon { get; }

    public ExportDeclarationSyntax(SyntaxNode exportKeyword, SyntaxNode[] modifiers, IdentifierSyntax identifier, SyntaxNode semicolon)
    {
        ExportKeyword = exportKeyword;
        Modifiers = modifiers;
        Identifier = identifier;
        Semicolon = semicolon;
    }
    public ExportDeclarationSyntax(IdentifierSyntax identifier)
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.ExportKeyword)),
              new SyntaxNode[0],
              identifier,
              new SyntaxNode(new SyntaxToken(SyntaxKind.SemicolonToken))
              )
    {
    }
    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            yield return ExportKeyword;
            foreach (var modifier in Modifiers)
            {
                yield return modifier;
            }
            yield return Identifier;
            yield return Semicolon;
        }
    }

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitExportDeclarationSyntax(this);
    }

    public ExportDeclarationSyntax WithModifiers(params SyntaxNode[] modifiers)
    {
        return new ExportDeclarationSyntax(
            ExportKeyword,
            modifiers,
            Identifier,
            Semicolon);
    }
}
