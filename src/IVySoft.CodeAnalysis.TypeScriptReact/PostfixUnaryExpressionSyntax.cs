// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct PostfixUnaryExpressionSyntax : ExpressionSyntax
{
    public SyntaxNode Operator { get; }
    public ExpressionSyntax Expression { get; }

    public PostfixUnaryExpressionSyntax(SyntaxNode @operator, ExpressionSyntax expression)
    {
        Operator = @operator;
        Expression = expression;
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { Expression, Operator };
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitPostfixUnaryExpressionSyntax(this);
    }
}
