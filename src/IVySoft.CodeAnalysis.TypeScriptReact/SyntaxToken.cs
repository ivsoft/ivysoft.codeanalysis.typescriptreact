// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Diagnostics;

namespace IVySoft.CodeAnalysis.TypeScriptReact;

[DebuggerDisplay("{GetDebuggerDisplay(), nq}")]
public sealed class SyntaxToken : IEquatable<SyntaxToken>
{
    public int Line { get; }
    public int Column { get; }
    public string Text { get; set; }
    public string ValueText { get; }
    public SyntaxKind Kind { get; }

    public SyntaxToken(SyntaxKind kind, string text, int line = -1, int column = -1)
    {
        Kind = kind;
        Text = text;
        ValueText = text;
        Line = line;
        Column = column;
    }
    public SyntaxToken(SyntaxKind kind)
    {
        Kind = kind;
#pragma warning disable IDE0010 // Add missing cases
        switch (kind)
        {
        case SyntaxKind.NewLineTrivia:
            Text = Environment.NewLine;
            break;
        default:
            Text = TokenToText[kind];
            break;
        }
#pragma warning restore IDE0010 // Add missing cases
        Line = -1;
        Column = -1;
        ValueText = Text;
    }

    public bool Equals(SyntaxToken? other)
    {
        return null != other && ValueText == other.ValueText && Kind == other.Kind;
    }

    public override string ToString() => Text;
    private string GetDebuggerDisplay()
    {
        return $"[{Kind},{Line},{Column}]{Text}";
    }

    internal static Dictionary<string, SyntaxKind> TextToToken = new()
    {
        { "abstract", SyntaxKind.AbstractKeyword },
        { "any", SyntaxKind.AnyKeyword },
        { "as", SyntaxKind.AsKeyword },
        { "boolean", SyntaxKind.BooleanKeyword },
        { "break", SyntaxKind.BreakKeyword },
        { "case", SyntaxKind.CaseKeyword },
        { "catch", SyntaxKind.CatchKeyword },
        { "class", SyntaxKind.ClassKeyword },
        { "continue", SyntaxKind.ContinueKeyword },
        { "const", SyntaxKind.ConstKeyword },
        { "constructor", SyntaxKind.ConstructorKeyword },
        { "debugger", SyntaxKind.DebuggerKeyword },
        { "declare", SyntaxKind.DeclareKeyword },
        { "default", SyntaxKind.DefaultKeyword },
        { "delete", SyntaxKind.DeleteKeyword },
        { "do", SyntaxKind.DoKeyword },
        { "else", SyntaxKind.ElseKeyword },
        { "enum", SyntaxKind.EnumKeyword },
        { "export", SyntaxKind.ExportKeyword },
        { "extends", SyntaxKind.ExtendsKeyword },
        { "false", SyntaxKind.FalseKeyword },
        { "finally", SyntaxKind.FinallyKeyword },
        { "for", SyntaxKind.ForKeyword },
        { "from", SyntaxKind.FromKeyword },
        { "function", SyntaxKind.FunctionKeyword },
        { "get", SyntaxKind.GetKeyword },
        { "if", SyntaxKind.IfKeyword },
        { "implements", SyntaxKind.ImplementsKeyword },
        { "import", SyntaxKind.ImportKeyword },
        { "in", SyntaxKind.InKeyword },
        { "instanceof", SyntaxKind.InstanceOfKeyword },
        { "interface", SyntaxKind.InterfaceKeyword },
        { "is", SyntaxKind.IsKeyword },
        { "keyof", SyntaxKind.KeyOfKeyword },
        { "let", SyntaxKind.LetKeyword },
        { "module", SyntaxKind.ModuleKeyword },
        { "namespace", SyntaxKind.NamespaceKeyword },
        { "never", SyntaxKind.NeverKeyword },
        { "new", SyntaxKind.NewKeyword },
        { "null", SyntaxKind.NullKeyword },
        { "number", SyntaxKind.NumberKeyword },
        { "object", SyntaxKind.ObjectKeyword },
        { "package", SyntaxKind.PackageKeyword },
        { "private", SyntaxKind.PrivateKeyword },
        { "protected", SyntaxKind.ProtectedKeyword },
        { "public", SyntaxKind.PublicKeyword },
        { "readonly", SyntaxKind.ReadonlyKeyword },
        { "require", SyntaxKind.RequireKeyword },
        { "global", SyntaxKind.GlobalKeyword },
        { "return", SyntaxKind.ReturnKeyword },
        { "set", SyntaxKind.SetKeyword },
        { "static", SyntaxKind.StaticKeyword },
        { "string", SyntaxKind.StringKeyword },
        { "super", SyntaxKind.SuperKeyword },
        { "switch", SyntaxKind.SwitchKeyword },
        { "symbol", SyntaxKind.SymbolKeyword },
        { "this", SyntaxKind.ThisKeyword },
        { "throw", SyntaxKind.ThrowKeyword },
        { "true", SyntaxKind.TrueKeyword },
        { "try", SyntaxKind.TryKeyword },
        { "type", SyntaxKind.TypeKeyword },
        { "typeof", SyntaxKind.TypeOfKeyword },
        { "undefined", SyntaxKind.UndefinedKeyword },
        { "var", SyntaxKind.VarKeyword },
        { "void", SyntaxKind.VoidKeyword },
        { "while", SyntaxKind.WhileKeyword },
        { "with", SyntaxKind.WithKeyword },
        { "yield", SyntaxKind.YieldKeyword },
        { "async", SyntaxKind.AsyncKeyword },
        { "await", SyntaxKind.AwaitKeyword },
        { "of", SyntaxKind.OfKeyword },
        { "{", SyntaxKind.OpenBraceToken },
        { "}", SyntaxKind.CloseBraceToken },
        { "(", SyntaxKind.OpenParenToken },
        { ")", SyntaxKind.CloseParenToken },
        { "[", SyntaxKind.OpenBracketToken },
        { "]", SyntaxKind.CloseBracketToken },
        { ".", SyntaxKind.SimpleMemberAccessExpression },
        { "...", SyntaxKind.DotDotDotToken },
        { ";", SyntaxKind.SemicolonToken },
        { ",", SyntaxKind.CommaToken },
        { "<", SyntaxKind.LessThanToken },
        { ">", SyntaxKind.GreaterThanToken },
        { "<=", SyntaxKind.LessThanEqualsToken },
        { ">=", SyntaxKind.GreaterThanEqualsToken },
        { "==", SyntaxKind.EqualsEqualsToken },
        { "!=", SyntaxKind.ExclamationEqualsToken },
        { "===", SyntaxKind.EqualsEqualsEqualsToken },
        { "!==", SyntaxKind.ExclamationEqualsEqualsToken },
        { "=>", SyntaxKind.EqualsGreaterThanToken },
        { "+", SyntaxKind.PlusToken },
        { "-", SyntaxKind.MinusToken },
        { "**", SyntaxKind.AsteriskAsteriskToken },
        { "*", SyntaxKind.AsteriskToken },
        { "/", SyntaxKind.SlashToken },
        { "%", SyntaxKind.PercentToken },
        { "++", SyntaxKind.PlusPlusToken },
        { "--", SyntaxKind.MinusMinusToken },
        { "<<", SyntaxKind.LessThanLessThanToken },
        { "</", SyntaxKind.LessThanSlashToken },
        { ">>", SyntaxKind.GreaterThanGreaterThanToken },
        { ">>>", SyntaxKind.GreaterThanGreaterThanGreaterThanToken },
        { "&", SyntaxKind.AmpersandToken },
        { "|", SyntaxKind.BarToken },
        { "^", SyntaxKind.CaretToken },
        { "!", SyntaxKind.ExclamationToken },
        { "~", SyntaxKind.TildeToken },
        { "&&", SyntaxKind.AmpersandAmpersandToken },
        { "||", SyntaxKind.BarBarToken },
        { "?", SyntaxKind.QuestionToken },
        { ":", SyntaxKind.ColonToken },
        { "=", SyntaxKind.EqualsToken },
        { "+=", SyntaxKind.PlusEqualsToken },
        { "-=", SyntaxKind.MinusEqualsToken },
        { "*=", SyntaxKind.AsteriskEqualsToken },
        { "**=", SyntaxKind.AsteriskAsteriskEqualsToken },
        { "/=", SyntaxKind.SlashEqualsToken },
        { "%=", SyntaxKind.PercentEqualsToken },
        { "<<=", SyntaxKind.LessThanLessThanEqualsToken },
        { ">>=", SyntaxKind.GreaterThanGreaterThanEqualsToken },
        { ">>>=", SyntaxKind.GreaterThanGreaterThanGreaterThanEqualsToken },
        { "&=", SyntaxKind.AmpersandEqualsToken },
        { "|=", SyntaxKind.BarEqualsToken },
        { "^=", SyntaxKind.CaretEqualsToken },
        { "@", SyntaxKind.AtToken },
    };
    internal static Dictionary<SyntaxKind, string> TokenToText = new()
    {
        {SyntaxKind.AbstractKeyword ,"abstract"},
        {SyntaxKind.AnyKeyword ,"any"},
        {SyntaxKind.AsKeyword ,"as"},
        {SyntaxKind.AsExpression, "as"},
        {SyntaxKind.BooleanKeyword ,"boolean"},
        {SyntaxKind.BreakKeyword ,"break"},
        {SyntaxKind.CaseKeyword ,"case"},
        {SyntaxKind.CatchKeyword ,"catch"},
        {SyntaxKind.ClassKeyword ,"class"},
        {SyntaxKind.ContinueKeyword ,"continue"},
        {SyntaxKind.ConstKeyword ,"const"},
        {SyntaxKind.ConstructorKeyword ,"constructor"},
        {SyntaxKind.DebuggerKeyword ,"debugger"},
        {SyntaxKind.DeclareKeyword ,"declare"},
        {SyntaxKind.DefaultKeyword ,"default"},
        {SyntaxKind.DeleteKeyword ,"delete"},
        {SyntaxKind.DoKeyword ,"do"},
        {SyntaxKind.ElseKeyword ,"else"},
        {SyntaxKind.EnumKeyword ,"enum"},
        {SyntaxKind.ExportKeyword ,"export"},
        {SyntaxKind.ExtendsKeyword ,"extends"},
        {SyntaxKind.FalseKeyword ,"false"},
        {SyntaxKind.FinallyKeyword ,"finally"},
        {SyntaxKind.ForKeyword ,"for"},
        {SyntaxKind.FromKeyword ,"from"},
        {SyntaxKind.FunctionKeyword ,"function"},
        {SyntaxKind.GetKeyword ,"get"},
        {SyntaxKind.IfKeyword ,"if"},
        {SyntaxKind.ImplementsKeyword ,"implements"},
        {SyntaxKind.ImportKeyword ,"import"},
        {SyntaxKind.InKeyword ,"in"},
        {SyntaxKind.InstanceOfKeyword ,"instanceof"},
        {SyntaxKind.InterfaceKeyword ,"interface"},
        {SyntaxKind.IsKeyword ,"is"},
        {SyntaxKind.KeyOfKeyword ,"keyof"},
        {SyntaxKind.LetKeyword ,"let"},
        {SyntaxKind.ModuleKeyword ,"module"},
        {SyntaxKind.NamespaceKeyword ,"namespace"},
        {SyntaxKind.NeverKeyword ,"never"},
        {SyntaxKind.NewKeyword ,"new"},
        {SyntaxKind.NullKeyword ,"null"},
        {SyntaxKind.NumberKeyword ,"number"},
        {SyntaxKind.ObjectKeyword ,"object"},
        {SyntaxKind.PackageKeyword ,"package"},
        {SyntaxKind.PrivateKeyword ,"private"},
        {SyntaxKind.ProtectedKeyword ,"protected"},
        {SyntaxKind.PublicKeyword ,"public"},
        {SyntaxKind.ReadonlyKeyword ,"readonly"},
        {SyntaxKind.RequireKeyword ,"require"},
        {SyntaxKind.GlobalKeyword ,"global"},
        {SyntaxKind.ReturnKeyword ,"return"},
        {SyntaxKind.SetKeyword ,"set"},
        {SyntaxKind.StaticKeyword ,"static"},
        {SyntaxKind.StringKeyword ,"string"},
        {SyntaxKind.SuperKeyword ,"super"},
        {SyntaxKind.SwitchKeyword ,"switch"},
        {SyntaxKind.SymbolKeyword ,"symbol"},
        {SyntaxKind.ThisKeyword ,"this"},
        {SyntaxKind.ThrowKeyword ,"throw"},
        {SyntaxKind.TrueKeyword ,"true"},
        {SyntaxKind.TryKeyword ,"try"},
        {SyntaxKind.TypeKeyword ,"type"},
        {SyntaxKind.TypeOfKeyword ,"typeof"},
        {SyntaxKind.UndefinedKeyword ,"undefined"},
        {SyntaxKind.VarKeyword ,"var"},
        {SyntaxKind.VoidKeyword ,"void"},
        {SyntaxKind.WhileKeyword ,"while"},
        {SyntaxKind.WithKeyword ,"with"},
        {SyntaxKind.YieldKeyword ,"yield"},
        {SyntaxKind.AsyncKeyword ,"async"},
        {SyntaxKind.AwaitKeyword ,"await"},
        {SyntaxKind.OfKeyword ,"of"},
        {SyntaxKind.OpenBraceToken,"{"},
        {SyntaxKind.CloseBraceToken ,"}"},
        {SyntaxKind.OpenParenToken ,"("},
        {SyntaxKind.CloseParenToken ,")"},
        {SyntaxKind.OpenBracketToken ,"["},
        {SyntaxKind.CloseBracketToken ,"]"},
        {SyntaxKind.SimpleMemberAccessExpression ,"."},
        {SyntaxKind.DotDotDotToken ,"..."},
        {SyntaxKind.SemicolonToken ,";"},
        {SyntaxKind.CommaToken ,","},
        {SyntaxKind.LessThanToken ,"<"},
        {SyntaxKind.GreaterThanToken ,">"},
        {SyntaxKind.LessThanEqualsToken ,"<="},
        {SyntaxKind.GreaterThanEqualsToken ,">="},
        {SyntaxKind.EqualsEqualsToken ,"=="},
        {SyntaxKind.ExclamationEqualsToken ,"!="},
        {SyntaxKind.EqualsEqualsEqualsToken ,"==="},
        {SyntaxKind.ExclamationEqualsEqualsToken ,"!=="},
        {SyntaxKind.EqualsGreaterThanToken ,"=>"},
        {SyntaxKind.PlusToken ,"+"},
        {SyntaxKind.MinusToken ,"-"},
        {SyntaxKind.AsteriskAsteriskToken ,"**"},
        {SyntaxKind.AsteriskToken ,"*"},
        {SyntaxKind.SlashToken ,"/"},
        {SyntaxKind.PercentToken ,"%"},
        {SyntaxKind.PlusPlusToken ,"++"},
        {SyntaxKind.MinusMinusToken ,"--"},
        {SyntaxKind.LessThanLessThanToken ,"<<"},
        {SyntaxKind.LessThanSlashToken ,"</"},
        {SyntaxKind.GreaterThanGreaterThanToken ,">>"},
        {SyntaxKind.GreaterThanGreaterThanGreaterThanToken ,">>>"},
        {SyntaxKind.AmpersandToken ,"&"},
        {SyntaxKind.BarToken ,"|"},
        {SyntaxKind.CaretToken ,"^"},
        {SyntaxKind.ExclamationToken ,"!"},
        {SyntaxKind.TildeToken ,"~"},
        {SyntaxKind.AmpersandAmpersandToken ,"&&"},
        {SyntaxKind.BarBarToken ,"||"},
        {SyntaxKind.QuestionToken ,"?"},
        {SyntaxKind.ColonToken ,":"},
        {SyntaxKind.EqualsToken ,"="},
        {SyntaxKind.PlusEqualsToken ,"+="},
        {SyntaxKind.MinusEqualsToken ,"-="},
        {SyntaxKind.AsteriskEqualsToken ,"*="},
        {SyntaxKind.AsteriskAsteriskEqualsToken ,"**="},
        {SyntaxKind.SlashEqualsToken ,"/="},
        {SyntaxKind.PercentEqualsToken ,"%="},
        {SyntaxKind.LessThanLessThanEqualsToken ,"<<="},
        {SyntaxKind.GreaterThanGreaterThanEqualsToken ,">>="},
        {SyntaxKind.GreaterThanGreaterThanGreaterThanEqualsToken ,">>>="},
        {SyntaxKind.AmpersandEqualsToken ,"&="},
        {SyntaxKind.BarEqualsToken ,"|="},
        {SyntaxKind.CaretEqualsToken ,"^="},
        {SyntaxKind.AtToken ,"@"},

        {SyntaxKind.SimpleAssignmentExpression, "=" },
        {SyntaxKind.SuppressNullableWarningExpression, "!" },

        {SyntaxKind.JsxElementOpeningBracket, "<" },
        {SyntaxKind.JsxClosingElementOpeningBracket, "</" },
        {SyntaxKind.JsxSelfClosingElementClosingBracket, "/>" },
        {SyntaxKind.JsxElementClosingBracket, ">" },

        {SyntaxKind.OpenGeneric, "<" },
        {SyntaxKind.CloseGeneric, ">" },

        {SyntaxKind.LabmdaToken, "=>" },

    };

}
