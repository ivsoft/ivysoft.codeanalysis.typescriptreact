// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct JsxElementSyntax : JsxElementBody
{
    public JsxStartElementSyntax StartElement { get; }
    public JsxElementBody[] Body { get; }
    public JsxEndElementSyntax? EndElement { get; }

    public JsxElementSyntax(JsxStartElementSyntax startElement, JsxElementBody[] body, JsxEndElementSyntax? endElement)
    {
        StartElement = startElement;
        Body = body;
        EndElement = endElement;
    }

    public JsxElementSyntax(IdentifierSyntax identifier)
        : this(new JsxStartElementSyntax(identifier), new JsxElementBody[0], null)
    {
    }
    public JsxElementSyntax()
        : this(new JsxStartElementSyntax(), new JsxElementBody[0], null)
    {
    }

    public JsxElementSyntax WithBody(params JsxElementBody[] body)
    {
        return new JsxElementSyntax(
            new JsxStartElementSyntax(
                StartElement.OpeningBracket,
                StartElement.Identifier,
                StartElement.Attributes,
                new SyntaxNode(
                    new SyntaxToken(SyntaxKind.JsxElementClosingBracket),
                    StartElement.ClosingBracket.LeadingTrivia,
                    StartElement.ClosingBracket.TrailingTrivia)),
            body,
            EndElement ?? new JsxEndElementSyntax(SyntaxFactory.Identifier(StartElement.Identifier.ValueText)));
    }
    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            yield return StartElement;
            foreach (var body in Body)
            {
                yield return body;
            }
            if (null != EndElement)
            {
                yield return EndElement;
            }
        }
    }

    public JsxElementSyntax WithAttributes(params JsxAttributeSyntax[] attributes)
    {
        return new JsxElementSyntax(
            StartElement.WithAttributes(attributes),
            Body,
            EndElement);

    }
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitJsxElementSyntax(this);
    }
}
