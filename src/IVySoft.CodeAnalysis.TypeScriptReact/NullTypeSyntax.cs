// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct NullTypeSyntax : ITypeSyntax
{
    public readonly SyntaxNode NullKeyword { get; }

    public NullTypeSyntax()
    {
        NullKeyword = new SyntaxNode(new SyntaxToken(SyntaxKind.NullKeyword));
    }

    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { NullKeyword };

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitNullTypeSyntax(this);
    }
}
