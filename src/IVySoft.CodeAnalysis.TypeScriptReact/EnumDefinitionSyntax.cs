// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class EnumDefinitionSyntax : INamespaceMemberSyntax, ICompilationUnitMemberSyntax
{
    public SyntaxNode[] Modifiers { get; }
    public SyntaxNode EnumKeyword { get; }
    public IdentifierSyntax Identifier { get; }
    public ExtendsSyntax? Extends { get; }
    public SyntaxNode OpenBrace { get; }
    public SeparatedSyntaxList<EnumMemberSyntax> Members { get; }
    public SyntaxNode CloseBrace { get; }

    public EnumDefinitionSyntax(
        SyntaxNode[] modifiers,
        SyntaxNode enumKeyword,
        IdentifierSyntax identifier,
        ExtendsSyntax? extends,
        SyntaxNode openBrace,
        SeparatedSyntaxList<EnumMemberSyntax> members,
        SyntaxNode closeBrace)
    {
        Modifiers = modifiers;
        EnumKeyword = enumKeyword;
        Identifier = identifier;
        Extends = extends;
        OpenBrace = openBrace;
        Members = members;
        CloseBrace = closeBrace;
    }
    public EnumDefinitionSyntax(
        IdentifierSyntax identifier)
        : this(
              new SyntaxNode[0],
              new SyntaxNode(new SyntaxToken(SyntaxKind.EnumKeyword)),
              identifier,
              null,
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenBraceToken)),
              new SeparatedSyntaxList<EnumMemberSyntax>(),
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseBraceToken))
              )
    {
    }
    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            foreach (var modifier in Modifiers)
            {
                yield return modifier;
            }
            yield return EnumKeyword;
            yield return Identifier;
            if (null != Extends)
            {
                yield return Extends;
            }
            yield return OpenBrace;
            yield return Members;
            yield return CloseBrace;
        }
    }

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitEnumDefinitionSyntax(this);
    }

    public EnumDefinitionSyntax WithMembers(params EnumMemberSyntax[] members)
    {
        return new EnumDefinitionSyntax(
            Modifiers,
            EnumKeyword,
            Identifier,
            Extends,
            OpenBrace,
            new SeparatedSyntaxList<EnumMemberSyntax>(members),
            CloseBrace);
    }

    public EnumDefinitionSyntax WithExtends(ExtendsSyntax extends)
    {
        return new EnumDefinitionSyntax(
            Modifiers,
            EnumKeyword,
            Identifier,
            extends,
            OpenBrace,
            Members,
            CloseBrace);
    }
    public EnumDefinitionSyntax WithModifiers(params SyntaxNode[] modifiers)
    {
        return new EnumDefinitionSyntax(
            modifiers,
            EnumKeyword,
            Identifier,
            Extends,
            OpenBrace,
            Members,
            CloseBrace);
    }
}
