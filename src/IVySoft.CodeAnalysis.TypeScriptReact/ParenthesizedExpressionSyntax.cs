// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.


namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct ParenthesizedExpressionSyntax : ExpressionSyntax
{
    public SyntaxNode OpenParenthesis { get; }
    public ExpressionSyntax Expression { get; }
    public SyntaxNode CloseParenthesis { get; }

    public ParenthesizedExpressionSyntax(SyntaxNode openParenthesis, ExpressionSyntax expression, SyntaxNode closeParenthesis)
    {
        OpenParenthesis = openParenthesis;
        Expression = expression;
        CloseParenthesis = closeParenthesis;
    }
    public ParenthesizedExpressionSyntax(ExpressionSyntax expression)
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenParenToken)),
              expression,
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseParenToken))
              )
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[]
        { OpenParenthesis, Expression, CloseParenthesis };

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitParenthesizedExpressionSyntax(this);
    }
}
