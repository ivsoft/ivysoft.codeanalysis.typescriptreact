// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct ExpressionBlockSyntax : ExpressionSyntax
{
    public SyntaxNode OpeningBrace { get; }
    public StatementSyntax[] Statements { get; }
    public SyntaxNode ClosingBrace { get; }

    public ExpressionBlockSyntax(SyntaxNode openingBrace, StatementSyntax[] statements, SyntaxNode closingBrace)
    {
        OpeningBrace = openingBrace;
        Statements = statements;
        ClosingBrace = closingBrace;
    }
    public ExpressionBlockSyntax(StatementSyntax[] statements)
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenBraceToken)),
              statements,
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseBraceToken))
              )
    {
    }
    public ExpressionBlockSyntax()
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenBraceToken)),
              new StatementSyntax[0],
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseBraceToken))
              )
    {
    }

    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            yield return OpeningBrace;
            foreach(var statement in Statements)
            {
                yield return statement;
            }
            yield return ClosingBrace;
        }
    }

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitExpressionBlockSyntax(this);
    }
}
