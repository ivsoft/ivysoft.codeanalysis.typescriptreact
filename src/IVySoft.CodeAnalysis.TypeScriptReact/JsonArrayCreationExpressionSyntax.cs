// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct JsonArrayExpressionSyntax : ExpressionSyntax
{
    public SyntaxNode OpeningBracket { get; }
    public SeparatedSyntaxList<ExpressionSyntax> Members { get; }
    public SyntaxNode ClosingBracket { get; }
    public JsonArrayExpressionSyntax(SyntaxNode openingBracket, SeparatedSyntaxList<ExpressionSyntax> members, SyntaxNode closingBracket)
    {
        OpeningBracket = openingBracket;
        Members = members;
        ClosingBracket = closingBracket;
    }
    public JsonArrayExpressionSyntax(SeparatedSyntaxList<ExpressionSyntax> members)
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.OpenBracketToken)),
              members,
              new SyntaxNode(new SyntaxToken(SyntaxKind.CloseBracketToken)))
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { OpeningBracket, Members, ClosingBracket };
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitJsonArrayExpressionSyntax(this);
    }
}
