// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct ExpressionStatementSyntax : StatementSyntax
{
    public ExpressionSyntax Expression { get; }
    public SyntaxNode Semicolon { get; }
    public ExpressionStatementSyntax(ExpressionSyntax expression, SyntaxNode semicolon)
    {
        Expression = expression;
        Semicolon = semicolon;
    }
    public ExpressionStatementSyntax(ExpressionSyntax expression)
        : this(expression, new SyntaxNode(new SyntaxToken(SyntaxKind.SemicolonToken)))
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { Expression, Semicolon };

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitExpressionStatementSyntax(this);
    }
}
