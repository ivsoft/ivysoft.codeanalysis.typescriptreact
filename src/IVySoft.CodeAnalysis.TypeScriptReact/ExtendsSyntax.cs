// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class ExtendsSyntax : ISyntaxNode
{
    public SyntaxNode ExtendsKeyword { get; }
    public SeparatedSyntaxList<IdentifierSyntax> BaseInterfaces { get; }

    public ExtendsSyntax(
        SyntaxNode extendsKeyword,
        SeparatedSyntaxList<IdentifierSyntax> baseInterfaces)
    {
        ExtendsKeyword = extendsKeyword;
        BaseInterfaces = baseInterfaces;
    }
    public ExtendsSyntax(
        IdentifierSyntax[] baseInterfaces)
        : this(
              new SyntaxNode(new SyntaxToken(SyntaxKind.ExtendsKeyword)),
              new SeparatedSyntaxList<IdentifierSyntax>(baseInterfaces)
              )
    {
    }

    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { ExtendsKeyword, BaseInterfaces };

    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitExtendsSyntax(this);
    }
}
