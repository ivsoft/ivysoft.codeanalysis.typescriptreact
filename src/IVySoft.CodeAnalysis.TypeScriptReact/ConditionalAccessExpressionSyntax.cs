// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public readonly record struct ConditionalAccessExpressionSyntax : ExpressionSyntax
{
    public ExpressionSyntax Target { get; }
    public SyntaxNode ConditionalOperator { get; }
    public SyntaxNode Operator { get; }
    public IdentifierSyntax Member { get; }
    public ConditionalAccessExpressionSyntax(
        ExpressionSyntax target,
        SyntaxNode conditionalOperator,
        SyntaxNode @operator,
        IdentifierSyntax member)
    {
        Target = target;
        ConditionalOperator = conditionalOperator;
        Operator = @operator;
        Member = member;
    }
    public ConditionalAccessExpressionSyntax(
        ExpressionSyntax target,
        IdentifierSyntax member)
        : this(
              target,
              new SyntaxNode(new SyntaxToken(SyntaxKind.QuestionToken)),
              new SyntaxNode(new SyntaxToken(SyntaxKind.SimpleMemberAccessExpression)),
              member)
    {
    }
    public IEnumerable<ISyntaxNode> Children => new ISyntaxNode[] { Target, ConditionalOperator, Operator, Member };
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitConditionalAccessExpressionSyntax(this);
    }
}
