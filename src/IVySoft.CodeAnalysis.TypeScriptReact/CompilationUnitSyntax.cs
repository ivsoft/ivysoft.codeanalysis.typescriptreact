// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Text;

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public class CompilationUnitSyntax : ISyntaxNode
{
    public SeparatedSyntaxList<ImportDirectiveSyntax> Imports { get; }
    public ICompilationUnitMemberSyntax[] Members { get; }

    public CompilationUnitSyntax()
    {
        Imports = new SeparatedSyntaxList<ImportDirectiveSyntax>();
        Members = new ICompilationUnitMemberSyntax[0];
    }
    public CompilationUnitSyntax(SeparatedSyntaxList<ImportDirectiveSyntax> imports)
    {
        Imports = imports;
        Members = new ICompilationUnitMemberSyntax[0];
    }
    public CompilationUnitSyntax(SeparatedSyntaxList<ImportDirectiveSyntax> imports, ICompilationUnitMemberSyntax[] members)
    {
        Imports = imports;
        Members = members;
    }

    public CompilationUnitSyntax WithImpors(SeparatedSyntaxList<ImportDirectiveSyntax> imports)
    {
        return new CompilationUnitSyntax(imports, Members);
    }
    public CompilationUnitSyntax WithMembers(params ICompilationUnitMemberSyntax[] members)
    {
        return new CompilationUnitSyntax(Imports, members);
    }
    public IEnumerable<ISyntaxNode> Children => Imports.OfType<ISyntaxNode>().Concat(Members);

    public CompilationUnitSyntax NormalizeWhitespace(TextWriter? log = null)
    {
        var context = new NormalizeWhitespaceContext(log);
        context.Visit(this, this);
        return this;
    }
    public override string ToString()
    {
        var sb = new StringBuilder();
        this.WriteTo(sb);
        return sb.ToString();
    }
    public virtual T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitCompilationUnitSyntax(this);
    }
    public CompilationUnitSyntax AddImpors(params ImportDirectiveSyntax[] importDirectiveSyntaxs)
    {
        return new CompilationUnitSyntax(
            new SeparatedSyntaxList<ImportDirectiveSyntax>(
                Imports.Concat(importDirectiveSyntaxs).ToList()),
            Members);
    }

    public CompilationUnitSyntax AddMembers(params ICompilationUnitMemberSyntax[] members)
    {
        return new CompilationUnitSyntax(
            Imports,
            Members.Concat(members).ToArray());
    }
}
