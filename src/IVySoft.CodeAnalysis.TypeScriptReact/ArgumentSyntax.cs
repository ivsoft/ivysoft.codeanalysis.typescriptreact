// Copyright (c) Vadim Malyshev (lboss75@gmail.com, vadim@iv-soft.ru). All rights reserved.

using System.Text;

namespace IVySoft.CodeAnalysis.TypeScriptReact;

public sealed class ArgumentSyntax : ISyntaxNode
{
    public ExpressionSyntax Expression { get; }

    public IEnumerable<ISyntaxNode> Children
    {
        get
        {
            yield return Expression;
        }
    }

    public ArgumentSyntax(ExpressionSyntax expression)
    {
        Expression = expression;
    }
    public override string ToString()
    {
        var sb = new StringBuilder();
        this.WriteTo(sb);
        return sb.ToString();
    }
    public T? Accept<T>(SyntaxNodeVisitor<T> visitor) where T : class
    {
        return visitor.VisitArgumentSyntax(this);
    }
}
